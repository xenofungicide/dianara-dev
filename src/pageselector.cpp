/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "pageselector.h"


PageSelector::PageSelector(QWidget *parent) : QWidget(parent)
{
    this->setWindowTitle(tr("Jump to page") + " - Dianara");
    this->setWindowIcon(QIcon::fromTheme("go-next-view-page"));

    this->setWindowFlags(Qt::Dialog);
    this->setWindowModality(Qt::ApplicationModal);


    this->messageLabel = new QLabel(tr("Page number:"));

    this->pageNumberSpinbox = new QSpinBox();
    pageNumberSpinbox->setRange(1, 1); // 1-totalPages, but initially 1-1
    pageNumberSpinbox->setValue(1);
    connect(pageNumberSpinbox, SIGNAL(editingFinished()),
            this, SLOT(onPageNumberEntered()));

    this->rangeLabel = new QLabel();

    this->goButton = new QPushButton(QIcon::fromTheme("go-next-view-page",
                                                      QIcon(":/images/button-share.png")),
                                     tr("&Go"));
    connect(goButton, SIGNAL(clicked()),
            this, SLOT(goToPage()));

    this->goToLastButton = new QPushButton(QIcon::fromTheme("go-last-view-page",
                                                            QIcon(":/images/button-share.png")),
                                           tr("Go to &last page"));
    connect(goToLastButton, SIGNAL(clicked()),
            this, SLOT(goToLastPage()));


    this->closeButton = new QPushButton(QIcon::fromTheme("window-close",
                                                         QIcon(":/images/button-cancel.png")),
                                        tr("&Cancel"));
    connect(closeButton, SIGNAL(clicked()),
            this, SLOT(hide()));


    closeAction = new QAction(this);
    closeAction->setShortcut(QKeySequence(Qt::Key_Escape));
    connect(closeAction, SIGNAL(triggered()),
            this, SLOT(hide()));
    this->addAction(closeAction);


    // Layout
    this->topLayout = new QHBoxLayout();
    topLayout->addWidget(messageLabel);
    topLayout->addWidget(pageNumberSpinbox);
    topLayout->addSpacing(8);
    topLayout->addWidget(rangeLabel);
    topLayout->addSpacing(24);
    topLayout->addWidget(goButton);

    this->bottomLayout = new QHBoxLayout();
    bottomLayout->addWidget(goToLastButton);
    bottomLayout->addSpacing(8);
    bottomLayout->addWidget(closeButton);


    this->mainLayout = new QVBoxLayout();
    mainLayout->addLayout(topLayout);
    mainLayout->addSpacing(8);
    mainLayout->addStretch(1);
    mainLayout->addSpacing(8);
    mainLayout->addLayout(bottomLayout);
    this->setLayout(mainLayout);


    qDebug() << "PageSelector created";
}

PageSelector::~PageSelector()
{
    qDebug() << "PageSelector destroyed";
}

/*
 * To be called from TimeLine()
 *
 * Set current page based on current Timeline page, and total page count
 *
 */
void PageSelector::showForPage(int currentPage, int totalPageCount)
{
    this->pageNumberSpinbox->setRange(1, totalPageCount);
    this->pageNumberSpinbox->setValue(currentPage);
    this->rangeLabel->setText(QString("[ 1-%1 ]").arg(totalPageCount));

    this->show();
    this->pageNumberSpinbox->setFocus();
}


////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SLOTS /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void PageSelector::goToPage()
{
    emit pageJumpRequested(this->pageNumberSpinbox->value());

    this->hide();
}


void PageSelector::goToLastPage()
{
    emit pageJumpRequested(this->pageNumberSpinbox->maximum()); // FIXME

    this->hide();
}

void PageSelector::onPageNumberEntered()
{
    // If spinbox still has focus, it means ENTER was pressed
    if (this->pageNumberSpinbox->hasFocus())
    {
        this->goToPage();
    }

    // If not, it means focus when somewhere else,
    // which also causes editingFinished() to be emitted
}
