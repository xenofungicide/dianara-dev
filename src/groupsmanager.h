/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef GROUPSMANAGER_H
#define GROUPSMANAGER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QAction>
#include <QMessageBox>

#include <QDebug>

#include "pumpcontroller.h"


class GroupsManager : public QWidget
{
    Q_OBJECT

public:
    explicit GroupsManager(PumpController *pumpController,
                           QWidget *parent = 0);
    ~GroupsManager();

signals:

public slots:
    void createGroup();
    void deleteGroup();
    void joinGroup();
    void leaveGroup();

private:
    QVBoxLayout *mainLayout;

    QLineEdit *newGroupNameLineEdit;
    QLineEdit *newGroupSummaryLineEdit;
    QLineEdit *newGroupDescLineEdit;
    QPushButton *createGroupButton;

    QLineEdit *joinLeaveGroupLineEdit;
    QPushButton *joinGroupButton;
    QPushButton *leaveGroupButton;

    QAction *closeAction;

    PumpController *pController;
};

#endif // GROUPSMANAGER_H
