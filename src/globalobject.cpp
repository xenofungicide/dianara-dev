/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "globalobject.h"


GlobalObject::GlobalObject(QObject *parent) : QObject(parent)
{
    QSettings settings;
    settings.beginGroup("Configuration");

    /////////////////////////////////////////////////////////////////// GENERAL


    ///////////////////////////////////////////////////////////////////// FONTS
    QFont defaultTitleFont;     // 1 point larger
    defaultTitleFont.setPointSize(defaultTitleFont.pointSize() + 1);
    defaultTitleFont.setBold(true);

    QFont defaultContentFont;   // Just the default text size

    QFont defaultCommentsFont;  //  1 point smaller
    defaultCommentsFont.setPointSize(defaultCommentsFont.pointSize() - 1);

    QFont defaultMinorFeedFont; // 2 points smaller
    defaultMinorFeedFont.setPointSize(defaultMinorFeedFont.pointSize() - 2);

    this->syncFontSettings(settings.value("font1",
                                          defaultTitleFont).toString(),
                           settings.value("font2",
                                          defaultContentFont).toString(),
                           settings.value("font3",
                                          defaultCommentsFont).toString(),
                           settings.value("font4",
                                          defaultMinorFeedFont).toString());


    //////////////////////////////////////////////////////////////////// COLORS
    this->colorsList.clear();
    colorsList << settings.value("color1", "#401020").toString()
               << settings.value("color2", "#154060").toString()
               << settings.value("color3", "#403510").toString()
               << settings.value("color4", "#103510").toString()
               << settings.value("color5").toString()
               << settings.value("color6").toString();
    // no need to call this->syncColorSettings() now...


    ///////////////////////////////////////////////////////////////// TIMELINES
    int pppMain = qBound(5, settings.value("postsPerPageMain", 20).toInt(), 50);
    int pppOther = qBound(1, settings.value("postsPerPageOther", 5).toInt(), 30);
    this->syncTimelinesSettings(pppMain,
                                pppOther,
                                settings.value("minorFeedSnippets", 0).toInt(),
                                settings.value("snippetsCharLimit", 200).toInt(),
                                settings.value("showDeletedPosts", false).toBool(),
                                settings.value("hideDuplicatedPosts", false).toBool());


    ///////////////////////////////////////////////////////////////////// POSTS
    this->syncPostSettings(settings.value("postAvatarSizeIndex", 2).toInt(), // 3rd (64x64) by default
                           settings.value("postExtendedShares",    true).toBool(),
                           settings.value("postShowExtraInfo",     false).toBool(),
                           settings.value("postHLAuthorComments",  true).toBool(),
                           settings.value("postHLOwnComments",     true).toBool(),
                           settings.value("postIgnoreSslInImages", false).toBool());


    ////////////////////////////////////////////////////////////////// COMPOSER
    this->syncComposerSettings(settings.value("publicPosts", false).toBool(),
                               settings.value("useFilenameAsTitle", false).toBool(),
                               settings.value("showCharacterCounter", false).toBool());


    ///////////////////////////////////////////////////////////// NOTIFICATIONS
    this->syncNotificationSettings(); // Dummy, TODO


    ////////////////////////////////////////////////////////////////////// TRAY
    this->syncTrayOptions(); // Dummy, TODO


    settings.endGroup();


    // Model for nick completion used by Composer
    this->nickCompletionModel = new QStandardItemModel(this);

    // Timeline height used to calculate maximum optimal height for post contents
    this->timelineHeight = 400; // Some initial value

    qDebug() << "GlobalObject created";
}

GlobalObject::~GlobalObject()
{
    qDebug() << "GlobalObject destroyed";
}




// General options
void GlobalObject::syncGeneralSettings()
{
    // TODO
}


// Font options
void GlobalObject::syncFontSettings(QString postTitleFont,
                                    QString postContentsFont,
                                    QString commentsFont,
                                    QString minorFeedFont)
{
    this->postTitleFontInfo = postTitleFont;
    this->postContentsFontInfo = postContentsFont;
    this->commentsFontInfo = commentsFont;
    this->minorFeedFontInfo = minorFeedFont;

    qDebug() << "GlobalObject::syncFontSettings() - font info sync'd";
}

QString GlobalObject::getPostTitleFont()
{
    return this->postTitleFontInfo;
}

QString GlobalObject::getPostContentsFont()
{
    return this->postContentsFontInfo;
}

QString GlobalObject::getCommentsFont()
{
    return this->commentsFontInfo;
}

QString GlobalObject::getMinorFeedFont()
{
    return this->minorFeedFontInfo;
}


// Color options
void GlobalObject::syncColorSettings(QStringList newColorList)
{
    this->colorsList = newColorList;

    qDebug() << "GlobalObject::syncColorSettings() - color list sync'd";
}

QStringList GlobalObject::getColorsList()
{
    return this->colorsList;
}

QString GlobalObject::getColor(int colorIndex)
{
    QString color;

    if (colorIndex >= 0 && colorIndex < this->colorsList.length())
    {
        color = this->colorsList.at(colorIndex);
        if (!QColor::isValidColor(color)) // If invalid, clear it
        {
            color.clear();
        }
    }

    return color;
}


// Timeline options
void GlobalObject::syncTimelinesSettings(int pppMain, int pppOther,
                                         int minorFeedSnippets, int snippetsChars,
                                         bool showDeleted, bool hideDuplicates)
{
    this->postsPerPageMain = pppMain;
    this->postsPerPageOther = pppOther;

    this->minorFeedSnippetsType = minorFeedSnippets;
    this->snippetsCharLimit = snippetsChars;

    this->showDeletedPosts = showDeleted;
    this->hideDuplicatedPosts = hideDuplicates;

    // TODO: more...
}

int GlobalObject::getPostsPerPageMain()
{
    return this->postsPerPageMain;
}

int GlobalObject::getPostsPerPageOther()
{
    return this->postsPerPageOther;
}

int GlobalObject::getMinorFeedSnippetsType()
{
    return this->minorFeedSnippetsType;
}

int GlobalObject::getSnippetsCharLimit()
{
    return this->snippetsCharLimit;
}

bool GlobalObject::getShowDeleted()
{
    return this->showDeletedPosts;
}

bool GlobalObject::getHideDuplicates()
{
    return this->hideDuplicatedPosts;
}



// Post options
void GlobalObject::syncPostSettings(int postAvatarSizeIndex,
                                    bool extendedShares, bool showExtraInfo,
                                    bool hlAuthorComments, bool hlOwnComments,
                                    bool ignoreSslInImages)
{
    this->postAvatarSizeIndex = postAvatarSizeIndex;
    int pixelSize;
    switch (postAvatarSizeIndex)
    {
    case 0:
        pixelSize = 32;
        break;
    // case 1 = default
    case 2:
        pixelSize = 64;
        break;
    case 3:
        pixelSize = 96;
        break;
    case 4:
        pixelSize = 128;
        break;
    case 5:
        pixelSize = 256;
        break;

    default: // index = 1 or invalid option
        pixelSize = 48;
    }

    this->postAvatarSize = QSize(pixelSize, pixelSize);
    this->postExtendedShares = extendedShares;
    this->postShowExtraInfo = showExtraInfo;
    this->postHLAuthorComments = hlAuthorComments;
    this->postHLOwnComments = hlOwnComments;
    this->postIgnoreSslInImages = ignoreSslInImages;
}

int GlobalObject::getPostAvatarSizeIndex()
{
    return this->postAvatarSizeIndex;
}

QSize GlobalObject::getPostAvatarSize()
{
    return this->postAvatarSize;
}

bool GlobalObject::getPostExtendedShares()
{
    return this->postExtendedShares;
}

bool GlobalObject::getPostShowExtraInfo()
{
    return this->postShowExtraInfo;
}

bool GlobalObject::getPostHLAuthorComments()
{
    return this->postHLAuthorComments;
}

bool GlobalObject::getPostHLOwnComments()
{
    return this->postHLOwnComments;
}

bool GlobalObject::getPostIgnoreSslInImages()
{
    return this->postIgnoreSslInImages;
}


// Composer options
void GlobalObject::syncComposerSettings(bool publicPosts,
                                        bool filenameAsTitle,
                                        bool showCharCounter)
{
    this->publicPostsByDefault = publicPosts;
    this->useFilenameAsTitle = filenameAsTitle;
    this->showCharacterCounter = showCharCounter;
}

bool GlobalObject::getPublicPostsByDefault()
{
    return this->publicPostsByDefault;
}

bool GlobalObject::getUseFilenameAsTitle()
{
    return this->useFilenameAsTitle;
}

bool GlobalObject::getShowCharacterCounter()
{
    return this->showCharacterCounter;
}


// Notification options
void GlobalObject::syncNotificationSettings()
{
    // TODO, still handled elsewhere
}


// Tray options
void GlobalObject::syncTrayOptions()
{
    // TODO, still handled elsewhere
}



///////////////////////////////////////////////////////////////////////////////



void GlobalObject::createMessageForContact(QString id, QString name,
                                           QString url)
{
    // Send signal to be caught by Publisher()
    emit messagingModeRequested(id, name, url);

    qDebug() << "GlobalObject; asking for Messaging mode for "
             << name << id << url;
}



void GlobalObject::editPost(QString originalPostId,
                            QString type,
                            QString title,
                            QString contents)
{
    // Signal to be caught by Publisher
    emit postEditRequested(originalPostId,
                           type,
                           title,
                           contents);

    qDebug() << "GlobalObject; asking to edit post: "
             << originalPostId << title << type;
}



QStandardItemModel *GlobalObject::getNickCompletionModel()
{
    return this->nickCompletionModel;
}


void GlobalObject::addToNickCompletionModel(QString id, QString name,
                                            QString url)
{
    QStandardItem *item = new QStandardItem(name);
    item->setData(id,  Qt::UserRole + 1);
    item->setData(url, Qt::UserRole + 2);

    this->nickCompletionModel->appendRow(item);
}

void GlobalObject::removeFromNickCompletionModel(QString id)
{
    QList<QStandardItem *> allNicks;
    allNicks = nickCompletionModel->findItems("", Qt::MatchContains);

    foreach (QStandardItem *item, allNicks)
    {
        qDebug() << item->data(Qt::UserRole + 1).toString();

        if (item->data(Qt::UserRole + 1).toString() == id)
        {
            this->nickCompletionModel->removeRow(item->row());
        }
    }
}

void GlobalObject::clearNickCompletionModel()
{
    // clear() seems to delete the items properly
    this->nickCompletionModel->clear();
}

void GlobalObject::sortNickCompletionModel()
{
    this->nickCompletionModel->sort(0);
}


// Change status bar message in main window
void GlobalObject::setStatusMessage(QString message)
{
    emit messageForStatusBar(message);
}

// Add a log message to the log viewer
void GlobalObject::logMessage(QString message, QString url)
{
    emit messageForLog(message, url);
}

void GlobalObject::storeTimelineHeight(int height)
{
    // Substract some pixels to account for the row of buttons, etc.
    this->timelineHeight = qMax(height - 80,
                                50); // Never less than 50, but window should never be that small
}

int GlobalObject::getTimelineHeight()
{
    return this->timelineHeight;
}
