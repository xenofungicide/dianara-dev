/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "timeline.h"

TimeLine::TimeLine(PumpController::requestTypes timelineType,
                   PumpController *pumpController,
                   GlobalObject *globalObject,
                   FilterChecker *filterChecker,
                   QWidget *parent) :  QWidget(parent)
{
    this->timelineType = timelineType;
    this->pController = pumpController;
    this->globalObj = globalObject;
    this->fChecker = filterChecker;

    this->setMinimumSize(180, 180); // Ensure something's always visible

    this->favoritesTimeline = false; // Initialize

    // Simulated data for demo posts
    QVariantMap demoLocationData;
    demoLocationData.insert("displayName",  "Demoville");

    QVariantMap demoAuthorData;
    demoAuthorData.insert("displayName",    "Demo User");
    demoAuthorData.insert("id",             "demo@somepump.example");
    demoAuthorData.insert("url",            "http://jancoding.wordpress.com/dianara");
    demoAuthorData.insert("location",       demoLocationData);
    demoAuthorData.insert("summary",        "I am not a real user");

    QVariantMap demoGeneratorData;
    demoGeneratorData.insert("displayName", "Dianara");

    QVariantMap demoObjectData;
    demoObjectData.insert("objectType",     "note");
    demoObjectData.insert("id",             "demo-post-id");

    // Show date/time when Dianara v1.2.5 was released
    demoObjectData.insert("published",      "2014-12-16T18:00:00Z");


    QSettings settings; // FIXME: kinda tmp, until posts have "unread" status, etc.
    settings.beginGroup("TimelineStates");

    // Demo post content depends on timeline type; also, restore some feed values
    switch (this->timelineType)
    {
    case PumpController::MainTimelineRequest:
        demoObjectData.insert("displayName", tr("Welcome to Dianara"));
        demoObjectData.insert("content",
                              tr("Dianara is a <b>pump.io</b> client.")
                              + "<br>"

                              + tr("If you don't have a Pump account yet, you can get one "
                                   "at the following address, for instance:")
                              + "<br>"
                                "<a href=\"http://pump.io/tryit.html\">http://pump.io/tryit.html</a>"
                                "<br><br>"

                              + tr("Press <b>F1</b> if you want to open the Help window.")
                              + "<br><br>"

                              + tr("First, configure your account from the "
                                   "<b>Settings - Account</b> menu.")
                              + " "
                              + tr("After the process is done, your profile "
                                   "and timelines should update automatically.")
                              + "<br><br>"

                              + tr("Take a moment to look around the menus and "
                                   "the Configuration window.")
                              + "<br><br>"

                              + tr("You can also set your profile data and picture from "
                                   "the <b>Settings - Edit Profile</b> menu.")
                              + "<br><br>"

                              + tr("There are tooltips everywhere, so if you "
                                   "hover over a button or a text field with "
                                   "your mouse, you'll probably see some "
                                   "extra information.")
                              + "<br><br>"

                              + "<a href=\"http://jancoding.wordpress.com/dianara\">"
                              + tr("Dianara's blog") + "</a><br><br>"
                                "<a href=\"https://github.com/e14n/pump.io/wiki/User-Guide\">"
                              + tr("Pump.io User Guide")
                              + "</a>"
                              + "<br><br>");

        this->previousNewestPostId = settings.value("previousNewestPostIdMain").toString();
        this->fullTimelinePostCount = settings.value("totalPostsMain").toInt();
        break;


    case PumpController::DirectTimelineRequest:
        demoObjectData.insert("displayName",  tr("Direct Messages Timeline"));
        demoObjectData.insert("content",      tr("Here, you'll see posts "
                                                 "specifically directed to you.")
                                              + "<br><br><br>");
        this->previousNewestPostId = settings.value("previousNewestPostIdDirect").toString();
        this->fullTimelinePostCount = settings.value("totalPostsDirect").toInt();
        break;


    case PumpController::ActivityTimelineRequest:
        demoObjectData.insert("displayName", tr("Activity Timeline"));
        demoObjectData.insert("content",     tr("You'll see your own posts here.")
                                             + "<br><br><br>");
        this->previousNewestPostId = settings.value("previousNewestPostIdActivity").toString();
        this->fullTimelinePostCount = settings.value("totalPostsActivity").toInt();
        break;


    case PumpController::FavoritesTimelineRequest:
        demoObjectData.insert("displayName", tr("Favorites Timeline"));
        demoObjectData.insert("content",     tr("Posts and comments you've liked.")
                                             + "<br><br><br>");
        this->previousNewestPostId = settings.value("previousNewestPostIdFavorites").toString();
        this->fullTimelinePostCount = settings.value("totalPostsFavorites").toInt();

        this->favoritesTimeline = true;
        break;



    default:
        demoObjectData.insert("content", "<h2>Empty timeline</h2>");

    }
    settings.endGroup();


    QVariantMap demoPostData;
    demoPostData.insert("actor",          demoAuthorData);
    demoPostData.insert("generator",      demoGeneratorData);
    demoPostData.insert("object",         demoObjectData);
    demoPostData.insert("id",             "demo-activity-id");


    this->firstLoad = true;
    this->gettingNew = true; // First time should be true

    this->unreadPostsCount = 0;
    this->timelineOffset = 0;
    this->wasOnFirstPage = true;
    this->pendingToReceiveNextTime = 0;

    this->postsPerPage = 1;    // 1 at first, so initial page number calculation makes sense



    getNewPendingButton = new QPushButton(QIcon::fromTheme("view-refresh",
                                                           QIcon(":/images/menu-refresh.png")),
                                          "*get more pending messages*",
                                          this);
    getNewPendingButton->setFlat(true);
    connect(getNewPendingButton, SIGNAL(clicked()),
            this, SLOT(goToFirstPage()));
    getNewPendingButton->hide();


    firstPageButton = new QPushButton(QIcon::fromTheme("go-first"),
                                      tr("Newest"));
    connect(firstPageButton, SIGNAL(clicked()),
            this, SLOT(goToFirstPage()));


    this->pageSelector = new PageSelector(this);
    connect(pageSelector, SIGNAL(pageJumpRequested(int)),
            this, SLOT(goToSpecificPage(int)));


    currentPageButton = new QPushButton("1 / 1", this); // Correct value will be set on real update
    currentPageButton->setFlat(true);
    currentPageButton->setSizePolicy(QSizePolicy::MinimumExpanding,
                                     QSizePolicy::Preferred);
    connect(currentPageButton, SIGNAL(clicked()),
            this, SLOT(showPageSelector()));



    previousPageButton = new QPushButton(QIcon::fromTheme("go-previous"),
                                         tr("Newer"));
    connect(previousPageButton, SIGNAL(clicked()),
            this, SLOT(goToPreviousPage()));
    nextPageButton = new QPushButton(QIcon::fromTheme("go-next"),
                                     tr("Older"));
    connect(nextPageButton, SIGNAL(clicked()),
            this, SLOT(goToNextPage()));


    ///// Layout
    postsLayout = new QVBoxLayout();
    postsLayout->setContentsMargins(0, 0, 0, 0);
    // Setting alignment of this layout to AlignTop caused all posts to be
    // compressed at the top when there were a lot of them; removed

    bottomLayout = new QHBoxLayout();
    bottomLayout->addSpacing(2);
    bottomLayout->addWidget(firstPageButton,    2);
    bottomLayout->addSpacing(4);
    bottomLayout->addWidget(currentPageButton,  0);
    bottomLayout->addSpacing(4);
    bottomLayout->addWidget(previousPageButton, 2);
    bottomLayout->addWidget(nextPageButton,     2);
    bottomLayout->addSpacing(2);


    mainLayout = new QVBoxLayout();
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->addWidget(getNewPendingButton);
    mainLayout->addLayout(postsLayout, 1);
    mainLayout->addStretch(0); // Ensure buttons are always at the bottom
    mainLayout->addSpacing(2);              // 2 pixel separation
    mainLayout->addLayout(bottomLayout, 0);

    this->setLayout(mainLayout);


    ////////////////////////////////////// QActions for better keyboard control

    // Single step
    scrollUpAction = new QAction(this);
    scrollUpAction->setShortcut(QKeySequence("Ctrl+Up"));
    connect(scrollUpAction, SIGNAL(triggered()),
            this, SLOT(scrollUp()));
    this->addAction(scrollUpAction);

    scrollDownAction = new QAction(this);
    scrollDownAction->setShortcut(QKeySequence("Ctrl+Down"));
    connect(scrollDownAction, SIGNAL(triggered()),
            this, SLOT(scrollDown()));
    this->addAction(scrollDownAction);

    // Pages
    scrollPageUpAction = new QAction(this);
    scrollPageUpAction->setShortcut(QKeySequence("Ctrl+PgUp"));
    connect(scrollPageUpAction, SIGNAL(triggered()),
            this, SLOT(scrollPageUp()));
    this->addAction(scrollPageUpAction);

    scrollPageDownAction = new QAction(this);
    scrollPageDownAction->setShortcut(QKeySequence("Ctrl+PgDown"));
    connect(scrollPageDownAction, SIGNAL(triggered()),
            this, SLOT(scrollPageDown()));
    this->addAction(scrollPageDownAction);

    // Top / Bottom
    scrollTopAction = new QAction(this);
    scrollTopAction->setShortcut(QKeySequence("Ctrl+Home"));
    connect(scrollTopAction, SIGNAL(triggered()),
            this, SLOT(scrollToTop()));
    this->addAction(scrollTopAction);

    scrollBottomAction = new QAction(this);
    scrollBottomAction->setShortcut(QKeySequence("Ctrl+End"));
    connect(scrollBottomAction, SIGNAL(triggered()),
            this, SLOT(scrollToBottom()));
    this->addAction(scrollBottomAction);


    // Previous/Next page in timeline
    previousPageAction = new QAction(this);
    previousPageAction->setShortcut(QKeySequence("Ctrl+Left"));
    connect(previousPageAction, SIGNAL(triggered()),
            previousPageButton, SLOT(click()));
    this->addAction(previousPageAction);

    nextPageAction = new QAction(this);
    nextPageAction->setShortcut(QKeySequence("Ctrl+Right"));
    connect(nextPageAction, SIGNAL(triggered()),
            nextPageButton, SLOT(click()));
    this->addAction(nextPageAction);


    // Add the default "demo" post
    ASActivity *demoActivity = new ASActivity(demoPostData, this);
    Post *demoPost = new Post(demoActivity,
                              false, // Not highlighted
                              false, // Not standalone
                              pController,
                              globalObj,
                              this);
    postsInTimeline.append(demoPost);
    postsLayout->addWidget(demoPost);



    // Sync avatar's follow state for every post when there are changes in the Following list
    connect(pController, SIGNAL(followingListChanged()),
            this, SLOT(updateAvatarFollowStates()));

    qDebug() << "TimeLine created";
}


/*
 * Destructor stores timeline states in the settings
 *
 */
TimeLine::~TimeLine()
{
    QSettings settings;
    settings.beginGroup("TimelineStates");

    switch (timelineType)
    {
    case PumpController::MainTimelineRequest:
        settings.setValue("previousNewestPostIdMain",
                          this->previousNewestPostId);
        settings.setValue("totalPostsMain",
                          this->fullTimelinePostCount);
        break;

    case PumpController::DirectTimelineRequest:
        settings.setValue("previousNewestPostIdDirect",
                          this->previousNewestPostId);
        settings.setValue("totalPostsDirect",
                          this->fullTimelinePostCount);
        break;

    case PumpController::ActivityTimelineRequest:
        settings.setValue("previousNewestPostIdActivity",
                          this->previousNewestPostId);
        settings.setValue("totalPostsActivity",
                          this->fullTimelinePostCount);
        break;

    case PumpController::FavoritesTimelineRequest:
        settings.setValue("previousNewestPostIdFavorites",
                          this->previousNewestPostId);
        settings.setValue("totalPostsFavorites",
                          this->fullTimelinePostCount);
        break;

    default:
        qDebug() << "Timeline destructor: timelineType is invalid!";
    }
    settings.endGroup();


    qDebug() << "TimeLine destroyed; Type:" << this->timelineType;
}



/*
 * Remove all widgets (Post *) from the timeline
 *
 */
void TimeLine::clearTimeLineContents()
{
    foreach (Post *oldPost, postsInTimeline)
    {
        this->mainLayout->removeWidget(oldPost);
        delete oldPost;
    }
    this->postsInTimeline.clear();
    this->objectsIdList.clear();

    this->pendingToReceiveNextTime = 0;

    qApp->processEvents(); // So GUI gets updated
}


/*
 * Remove oldest posts from current page, to avoid ever-increasing memory usage
 * Called after updating the timeline, only when getting newer posts on the
 * first page
 *
 * At the very least, keep as many posts as were received in last update
 *
 */
void TimeLine::removeOldPosts(int minimumToKeep)
{
    int maxPosts = qMax(this->postsPerPage * 2, // TMP FIXME
                        minimumToKeep);

    if (postsInTimeline.count() <= maxPosts)
    {
        // Not too many posts yet, so do nothing
        return;
    }

    int postCounter = 0;
    int deletedCounter = 0; // tmp, TESTS (EXTRALOGGING)
    foreach (Post *post, postsInTimeline)
    {
        if (postCounter >= maxPosts)
        {
            if (!post->isNew()             // Don't remove if it's unread
             && !post->isBeingCommented()) // or currently being commented on
            {
                this->postsLayout->removeWidget(post);
                this->postsInTimeline.removeOne(post);
                delete post;
                ++deletedCounter; // tmp, TESTS (EXTRALOGGING)
            }
        }

        ++postCounter;
    }

    // Update "next" link manually, based on the last post present in the page
    QByteArray lastPostId = postsInTimeline.last()->getActivityId().toLocal8Bit();
    lastPostId = lastPostId.toPercentEncoding(); // Needs to be percent-encoded

    this->nextPageLink = this->pController->getFeedApiUrl(this->timelineType)
                       + "?before=" + lastPostId;

#ifdef EXTRALOGGING
    // tmp, TESTS - debugging via log
    this->globalObj->logMessage(QString("##### %1 POSTS IN '%2' AFTER REMOVAL;  "
                                        "%3 CAME IN, %4 WERE DELETED - "
                                        "NEXT PAGE: %5")
                                .arg(postsInTimeline.count())
                                .arg(PumpController::getFeedNameAndPath(this->timelineType).first())
                                .arg(minimumToKeep)
                                .arg(deletedCounter)
                                .arg(this->nextPageLink));
#endif
}



int TimeLine::getCurrentPage()
{
    if (this->postsPerPage == 0)
    {
        this->postsPerPage = 1;
    }

    return (this->timelineOffset / this->postsPerPage) + 1;
}


int TimeLine::getTotalPages()
{
    if (this->postsPerPage == 0)
    {
        this->postsPerPage = 1;
    }

    return qCeil(this->fullTimelinePostCount / (float)this->postsPerPage);
}


/*
 *  Update the button at the bottom of the page, indicating current "page"
 *
 */
void TimeLine::updateCurrentPageNumber()
{
    int currentPage = this->getCurrentPage();
    int totalPages = this->getTotalPages();

    this->currentPageButton->setText(QString("%1 / %2")
                                     .arg(currentPage)
                                     .arg(totalPages));
    // The shortcut needs to be set each time the text is changed
    currentPageButton->setShortcut(QKeySequence("Ctrl+G"));

    this->currentPageButton->setToolTip(tr("Page %1 of %2.")
                                        .arg(currentPage)
                                        .arg(totalPages)
                                        + "<br>"
                                        + tr("Showing %1 posts per page.")
                                          .arg(this->postsPerPage)
                                        + "<br>"
                                        + tr("%1 posts in total.")
                                          .arg(this->fullTimelinePostCount)
                                        + "<hr>"
                                          "<b><i>"
                                        + tr("Click here or press Control+G to "
                                             "jump to a specific page")
                                        + "</i></b>");

    if (currentPage == totalPages)
    {
        this->nextPageButton->setDisabled(true);
    }
    else
    {
        this->nextPageButton->setEnabled(true);
    }
}




/*
 * Resize all posts in timeline
 *
 */
void TimeLine::resizePosts(QList<Post *> postsToResize, bool resizeAll)
{
    if (resizeAll)
    {
        postsToResize = this->postsInTimeline;
    }

    foreach (Post *post, postsToResize)
    {
        post->resetResizesCount();

        // Force a Post() resizeEvent, which will call
        // setPostContents() and setPostHeight()
        post->resize(post->width() - 1,
                     post->height() - 1);
    }
}

void TimeLine::markPostsAsRead()
{
    foreach (Post *post, postsInTimeline)
    {
        // Mark post as read without informing the timeline
        post->setPostAsRead(false);
    }

    unreadPostsCount = 0;
    highlightedPostsCount = 0;

    emit unreadPostsCountChanged(this->timelineType,
                                 this->unreadPostsCount,
                                 this->highlightedPostsCount,
                                 this->fullTimelinePostCount);
}


void TimeLine::updateFuzzyTimestamps()
{
    foreach (Post *post, postsInTimeline)
    {
        post->setFuzzyTimestamps();
    }
}

bool TimeLine::commentingOnAnyPost()
{
    foreach (Post *post, postsInTimeline)
    {
        if (post->isBeingCommented())
        {
            return true;
        }
    }

    return false;
}

void TimeLine::notifyBlockedUpdates()
{
    QString tlName = PumpController::getFeedNameAndPath(timelineType).first();
    this->globalObj->setStatusMessage(tr("'%1' cannot be updated "
                                         "because a comment is currently "
                                         "being composed.",
                                         "%1 = feed's name").arg(tlName));
}


/*
 * Return list of pointers to Post() objects currently in the timeline
 *
 */
QList<Post *> TimeLine::getPostsInTimeline()
{
    return this->postsInTimeline;
}



/*****************************************************************************/
/*****************************************************************************/
/********************************** SLOTS ************************************/
/*****************************************************************************/
/*****************************************************************************/


void TimeLine::setTimeLineContents(QVariantList postList, int postsPerPage,
                                   QString previousLink, QString nextLink,
                                   int totalItems)
{
    qDebug() << "TimeLine::setTimeLineContents()";
    int postListSize = postList.size();

    // Remove all previous posts in timeline, when switching pages
    if (firstLoad || !wasOnFirstPage || favoritesTimeline || timelineOffset > 0)
    {
        // Hide the widget while it reloads; helps performance a lot
        this->hide();

        qDebug() << "Removing previous posts from timeline";
        this->clearTimeLineContents();
        this->unreadPostsCount = 0;
        this->highlightedPostsCount = 0;

        // Ask mainWindow to scroll the QScrollArea containing the timeline to the top
        //  emit scrollTo(QAbstractSlider::SliderToMinimum);
        ////////// TMP FIXME: don't scroll to top; make it optional

        this->previousPageLink = previousLink;
        this->nextPageLink = nextLink;
        qDebug() << "Prev/Next links:" << previousPageLink << nextPageLink;
    }
    else
    {
        if (!previousLink.isEmpty())
        {
            this->previousPageLink = previousLink;
            // Just the previousLink; don't store nextLink, keep the old one
        }

        if (postListSize > 0)
        {
            // Disable to avoid clicks to posts (which would mark them as read)
            this->setDisabled(true); // until fully updated
        }
    }


    this->postsPerPage = postsPerPage;

    int totalPostDifference = totalItems - this->fullTimelinePostCount;
    this->fullTimelinePostCount = totalItems;


    // Check how many more posts need to be received, if more than max are pending
    pendingToReceiveNextTime += totalPostDifference;
    pendingToReceiveNextTime -= postListSize;
    if (pendingToReceiveNextTime > 0)
    {
        if (firstLoad)
        {
            // The difference in pending posts is in the older pages, so doesn't count
            this->pendingToReceiveNextTime = 0;

            // FIXME 1.3.1: On first load, should display the "pending" number
            // at the bottom or at the "older" button
        }
        else
        {
            // Button at the top to fetch the pending messages, even more new stuff
            this->getNewPendingButton->setText(tr("Get %1 pending posts")
                                               .arg(pendingToReceiveNextTime)
                                               + "   " // 3 spaces, then a watch
                                               + QString::fromUtf8("\342\214\232"));
            this->getNewPendingButton->show();
        }
    }
    else
    {
        this->pendingToReceiveNextTime = 0; // In case it was less than 0
        this->getNewPendingButton->hide();
    }


    int newPostCount = 0;
    int newHighlightedPostsCount = 0;
    int newDeletedPostsCount = 0;
    int newFilteredPostsCount = 0;
    bool allNewPostsCounted = false;
    int insertedPosts = 0;
    // Here we'll store the postID for the first (newest) post in the timeline
    QString newestPostId; // (actually activity ID)
    // With it, we can know how many new posts (if any) we receive next time

    QList<Post *> postsInsertedThisTime;

    // Fill timeline with new contents
    foreach (QVariant singlePost, postList)
    {
        if (singlePost.type() == QVariant::Map)
        {
            bool postIsNew = false;

            QVariantMap activityMap;
            // Since "Favorites" is a collection of objects, not activities,
            // we need to put "Favorites" posts into fake activities
            if (!favoritesTimeline)
            {
                // Data is already an activity
                activityMap = singlePost.toMap();
            }
            else
            {
                // Put object into the empty/fake VariantMap for the activity
                activityMap.insert("object", singlePost.toMap());
                activityMap.insert("actor",  singlePost.toMap()
                                                       .value("author").toMap());
                activityMap.insert("id",     singlePost.toMap()
                                                       .value("id").toString());
            }

            ASActivity *activity = new ASActivity(activityMap, this);

            // See if it's deleted
            QString postDeletedTime = activity->object()->getDeletedTime();

            // See if we have to filter it out (or highlight it)
            int filtered = this->fChecker->validateActivity(activity);

            // See if we hide the post if it's already visible in the timeline
            bool postIsDuplicated = false;
            if (globalObj->getHideDuplicates()) // Depending on the setting
            {
                if (this->objectsIdList.contains(activity->object()->getId()))
                {
                    postIsDuplicated = true;
                }
            }

            if (newestPostId.isEmpty()) // only first time, for newest post
            {
                if (gettingNew)
                {
                    newestPostId = activity->getId();
                }
                else
                {
                    newestPostId = this->previousNewestPostId;
                    allNewPostsCounted = true;
                }
            }


            if (!allNewPostsCounted)
            {
                if (activity->getId() == this->previousNewestPostId)
                {
                    allNewPostsCounted = true;
                }
                else
                {
                    // If post is NOT deleted or filtered, not ours, and
                    // this is not the Favorites timeline, add it to the count
                    if (postDeletedTime.isEmpty()
                        && filtered != FilterChecker::FilterOut
                        && !postIsDuplicated
                        && activity->author()->getId() != pController->currentUserId()
                        && activity->object()->author()->getId() != pController->currentUserId()
                        && !favoritesTimeline)
                    {
                        ++newPostCount;

                        // Mark current post as new
                        postIsNew = true;
                    }
                    else
                    {
                        if (!postDeletedTime.isEmpty())
                        {
                            ++newDeletedPostsCount;
                        }
                        else if (filtered == FilterChecker::FilterOut)
                        {
                            ++newFilteredPostsCount;
                        }
                    }
                }
            }



            bool highlightedByFilter = false;
            if (filtered == FilterChecker::Highlight)
            {
                highlightedByFilter = true;
            }

            Post *newPost = new Post(activity,
                                     highlightedByFilter,
                                     false,  // NOT standalone
                                     pController,
                                     globalObj,
                                     this);
            if (postIsNew)
            {
                newPost->setPostAsNew();
                connect(newPost, SIGNAL(postRead(bool)),
                        this, SLOT(decreaseUnreadPostsCount(bool)));

                if (newPost->getHighlightType() != Post::NoHighlight)
                {
                    ++newHighlightedPostsCount;
                }
            }


            this->objectsIdList.append(newPost->getObjectId());
            postsInsertedThisTime.append(newPost);
            this->postsLayout->insertWidget(insertedPosts, newPost);
            this->postsInTimeline.insert(insertedPosts, newPost);
            ++insertedPosts;

            // FIXME: this signal should go directly via a global shared object, instead
            connect(newPost, SIGNAL(commentingOnPost(QWidget*)),
                    this, SIGNAL(commentingOnPost(QWidget*)));


            // If post has been filtered out or hidden because it's a duplicate
            if (filtered == FilterChecker::FilterOut || postIsDuplicated)
            {
                newPost->hide(); // For now; maybe make it so that it can be clicked to show - FIXME
                qDebug() << "Post filtered out or hidden because it's a duplicate\n"
                         << "Filter action:" << filtered
                         << "(0=filter out; 1=highlight, 999=no filtering)\n"
                         << "Duplicated:" << postIsDuplicated;
            }
        }
        else  // singlePost.type() is not a QVariant::Map
        {
            qDebug() << "Expected a Map, got something else";
            qDebug() << postList;
        }
    } // end foreach

    qApp->processEvents(); // pre-resize posts

    this->firstLoad = false;

    if (!newestPostId.isEmpty())
    {
        this->previousNewestPostId = newestPostId;
    }

    this->unreadPostsCount += newPostCount;
    this->highlightedPostsCount += newHighlightedPostsCount;
    qDebug() << "New posts:" << newPostCount
             << "\nActual total new from previous update:" << totalPostDifference
             << "\nNewest post ID:" << previousNewestPostId
             << "\nNew highlighted:" << newHighlightedPostsCount
             << "\nNew deleted: " << newDeletedPostsCount
             << "\nNew filtered out: " << newFilteredPostsCount
             << "\nTotal posts:" << fullTimelinePostCount
             << "\nTotal currently loaded posts:" << this->postsInTimeline.size();


    if (postListSize > 0)
    {
        this->updateCurrentPageNumber();

        // Resize the posts, but only the ones added in this update
        this->resizePosts(postsInsertedThisTime);
    }

    if (gettingNew)
    {
        emit timelineRendered(this->timelineType, newPostCount,
                              newHighlightedPostsCount, newDeletedPostsCount,
                              newFilteredPostsCount, pendingToReceiveNextTime);
        emit unreadPostsCountChanged(this->timelineType,
                                     unreadPostsCount,
                                     highlightedPostsCount,
                                     fullTimelinePostCount);

        // Clean up, keeping at least the posts that were just received
        if (postListSize > 0)  // but only if there was _something_
        {
            this->removeOldPosts(postListSize);
        }
    }
    else
    {
        emit timelineRendered(this->timelineType, postListSize,
                              -1, newDeletedPostsCount,
                              newFilteredPostsCount, -1);
        emit unreadPostsCountChanged(this->timelineType, 0, 0,
                                     fullTimelinePostCount);
    }

    // Show and enable timeline again, since everything is added and drawn
    this->show();
    this->setEnabled(true);

    qDebug() << "setTimeLineContents() /END";
}


/*
 * Update data in all currently-visible posts matching the object ID sent
 * by the minor feed
 *
 */
void TimeLine::updatePostsFromMinorFeed(ASObject *object)
{
    /* FIXME: This should handle cases where a comment might be visible as a
     * post in the timeline (shared) _and_ be a comment in a visible post
     *
     * Also, something could be a note, therefore visible in the timeline,
     * but also be in reply to something else.
     *
     */

    if (object->getInReplyToId().isEmpty()) // Parent object
    {
        foreach (Post *post, postsInTimeline)
        {
            if (post->getObjectId() == object->getId())
            {
                post->updateDataFromObject(object);
            }
        }
    }
    else                                    // Reply to something
    {
        foreach (Post *post, postsInTimeline)
        {
            if (post->getObjectId() == object->getInReplyToId())
            {
                post->updateCommentFromObject(object);
            }
        }
    }
}



void TimeLine::addLikesFromMinorFeed(QString objectId, QString objectType,
                                     QString actorId, QString actorName,
                                     QString actorUrl)
{
    // FIXME: handle updating likes in comments
    foreach (Post *post, postsInTimeline)
    {
        if (post->getObjectId() == objectId)
        {
            post->appendLike(actorId, actorName, actorUrl);
        }
    }
}

void TimeLine::removeLikesFromMinorFeed(QString objectId, QString objectType,
                                        QString actorId)
{
    // FIXME: handle updating likes in comments
    foreach (Post *post, postsInTimeline)
    {
        if (post->getObjectId() == objectId)
        {
            post->removeLike(actorId);
        }
    }
}


/*
 * Add one single comment read from a minor feed, to the
 * corresponding parent post in this timeline
 *
 */
void TimeLine::addReplyFromMinorFeed(ASObject *object)
{
    QString parentPostId = object->getInReplyToId();

    foreach (Post *post, postsInTimeline)
    {
        if (post->getObjectId() == parentPostId)
        {
            post->appendComment(object);
        }
    }
}

void TimeLine::setPostsDeletedFromMinorFeed(ASObject *object)
{
    // FIXME: handle marking deleted comments

    foreach (Post *post, postsInTimeline)
    {
        if (post->getObjectId() == object->getId())
        {
            post->setPostDeleted(object->getDeletedOnString());
        }
    }
}





/*
 * Add the full list of likes to a post
 *
 */
void TimeLine::setLikesInPost(QVariantList likesList, QString originatingPostURL)
{
    //qDebug() << "TimeLine::setLikesInPost()";
    QString originatingPostCleanUrl = originatingPostURL.split("?").at(0);
    //qDebug() << "Originating post URL:" << originatingPostCleanUrl;


    // Look for the originating Post() object
    qDebug() << "Looking for the originating Post() object";
    foreach (Post *post, postsInTimeline)
    {
        if (post->likesUrl() == originatingPostCleanUrl)
        {
            qDebug() << "Found originating Post; setting likes on it...";
            post->setLikes(likesList);
            /* Don't break, so likes get set in other
             * visible copies of the post too */
        }
    }
}


/*
 * Add the full list of comments to a post
 *
 */
void TimeLine::setCommentsInPost(QVariantList commentsList,
                                 QString originatingPostURL)
{
    qDebug() << "TimeLine::setCommentsInPost()";
    QString originatingPostCleanUrl = originatingPostURL.split("?").at(0);
    //qDebug() << "Originating post URL:" << originatingPostCleanUrl;


    // Look for the originating Post() object
    qDebug() << "Looking for the originating Post() object";
    foreach (Post *post, postsInTimeline)
    {
        if (post->commentsURL() == originatingPostCleanUrl)
        {
            qDebug() << "Found originating Post; setting comments on it...";
            post->setComments(commentsList);

            // break;
            /* Don't break, so comments get set in copies of the post too,
               like if JohnDoe posted something and JaneDoe shared it soon
               after, so both the original post and its shared copy are visible
               in the timeline. */
        }
    }
}





void TimeLine::goToFirstPage()
{
    qDebug() << "TimeLine::goToFirstPage()";

    if (this->commentingOnAnyPost())
    {
        // Update is blocked because a post is being commented
        this->notifyBlockedUpdates();
        return;
    }

    this->gettingNew = true;

    if (timelineOffset == 0) // On page 1
    {
        this->wasOnFirstPage = true;
    }
    else
    {
        this->wasOnFirstPage = false;

        this->previousPageLink.clear();  // Full reload of newest stuff
        this->timelineOffset = 0;
    }

    /*
     * If this is the favorites timeline, previousPageLink will be empty,
     * which means the first posts at offset 0 will be loaded anyway
     *
     */
    pController->getFeed(this->timelineType,
                         this->postsPerPage,
                         this->previousPageLink);
}



void TimeLine::goToPreviousPage()
{
    qDebug() << "TimeLine::goToPreviousPage()";
    if (this->commentingOnAnyPost())
    {
        this->notifyBlockedUpdates();
        return;
    }

    this->gettingNew = false;

    this->timelineOffset -= this->postsPerPage;
    if (timelineOffset < 0)
    {
        timelineOffset = 0;
    }

    if (!favoritesTimeline) // Not favorites, use PreviousLink
    {
        pController->getFeed(this->timelineType,
                             this->postsPerPage,
                             this->previousPageLink);
    }
    else
    {
        pController->getFeed(this->timelineType,
                             this->postsPerPage,
                             "",
                             this->timelineOffset);
    }
}



void TimeLine::goToNextPage()
{
    qDebug() << "TimeLine::goToNextPage()";
    if (this->commentingOnAnyPost())
    {
        this->notifyBlockedUpdates();
        return;
    }

    this->gettingNew = false;

    this->timelineOffset += this->postsPerPage;

    if (!favoritesTimeline) // Not favorites, use NextLink
    {
        pController->getFeed(this->timelineType,
                             this->postsPerPage,
                             this->nextPageLink);
    }
    else // Use offset
    {
        pController->getFeed(this->timelineType,
                             this->postsPerPage,
                             "",
                             this->timelineOffset);
    }
}


void TimeLine::goToSpecificPage(int pageNumber)
{
    qDebug() << "TimeLine::goToSpecificPage(): " << pageNumber;
    if (this->commentingOnAnyPost())
    {
        this->notifyBlockedUpdates();
        return;
    }

    this->timelineOffset = (pageNumber - 1) * this->postsPerPage;

    pController->getFeed(this->timelineType,
                         this->postsPerPage,
                         "", // No prev/next link, use offset instead
                         this->timelineOffset);
}


void TimeLine::showPageSelector()
{
    this->pageSelector->showForPage(this->getCurrentPage(),
                                    this->getTotalPages());
}



void TimeLine::scrollUp()
{
    emit scrollTo(QAbstractSlider::SliderSingleStepSub);
}

void TimeLine::scrollDown()
{
    emit scrollTo(QAbstractSlider::SliderSingleStepAdd);
}

void TimeLine::scrollPageUp()
{
    emit scrollTo(QAbstractSlider::SliderPageStepSub);
}

void TimeLine::scrollPageDown()
{
    emit scrollTo(QAbstractSlider::SliderPageStepAdd);
}

void TimeLine::scrollToTop()
{
    emit scrollTo(QAbstractSlider::SliderToMinimum);
}

void TimeLine::scrollToBottom()
{
    emit scrollTo(QAbstractSlider::SliderToMaximum);
}



/*
 * Decrease internal counter of unread posts (by 1), and inform
 * the parent window, so it can update its tab titles
 *
 */
void TimeLine::decreaseUnreadPostsCount(bool wasHighlighted)
{
    --unreadPostsCount;

    if (wasHighlighted)
    {
        --highlightedPostsCount;
    }

    emit unreadPostsCountChanged(this->timelineType,
                                 this->unreadPostsCount,
                                 this->highlightedPostsCount,
                                 this->fullTimelinePostCount);
}

void TimeLine::updateAvatarFollowStates()
{
    foreach (Post *post, postsInTimeline)
    {
        post->syncAvatarFollowState();
    }
}
