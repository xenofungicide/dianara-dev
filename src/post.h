/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef POST_H
#define POST_H

#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTextBrowser>
#include <QLabel>
#include <QIcon>
#include <QPixmap>
#include <QPushButton>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QFile>
#include <QVariantList>
#include <QMessageBox>
#include <QTimer>
#include <QSettings>

#include <QDebug>


#include "pumpcontroller.h"
#include "globalobject.h"
#include "timestamp.h"
#include "mischelpers.h"
#include "commenterblock.h"
#include "imageviewer.h"
#include "asactivity.h"
#include "avatarbutton.h"
#include "downloadwidget.h"
#include "hclabel.h"


class Post : public QFrame
{
    Q_OBJECT

public:
    Post(ASActivity *activity,
         bool highlightedByFilter,
         bool isStandalone,
         PumpController *pumpController,
         GlobalObject *globalObject,
         QWidget *parent);
    ~Post();

    void updateDataFromActivity(ASActivity *activity);
    void updateDataFromObject(ASObject *object);

    void updateCommentFromObject(ASObject *object);

    void setPostContents();
    void onResizeOrShow();
    void setPostHeight();
    void resetResizesCount();
    void getPendingImages();

    QString likesUrl();
    void setLikes(QVariantList likesList, int likesCount=-1);
    void appendLike(QString actorId, QString actorName, QString actorUrl);
    void removeLike(QString actorId);
    void refreshLikesInfo(int likesListSize, int likesCount);
    void setLikesLabel(int likesCount);

    QString commentsURL();
    void setComments(QVariantList commentsList);
    void appendComment(ASObject *comment);
    void setCommentsLabel(int commentsCount);

    QString sharesURL();
    void setShares(QVariantList sharesList, int sharesCount=-1);
    void setSharesLabel(int resharesCount);

    void setPostUnreadStatus();
    void setPostAsNew();
    void setPostAsRead(bool informTimeline=true);
    void setPostDeleted(QString postDeletedTime);

    int getHighlightType();
    QString getActivityId();
    QString getObjectId();

    void setFuzzyTimestamps();
    void syncAvatarFollowState();

    bool isBeingCommented();
    bool isNew();

    enum PostHightlightType
    {
        NoHighlight = -1,
        MessageForUserHighlight,
        OwnMessageHighlight,
        FilterRulesHighlight
    };


signals:
    void postRead(bool wasHighlighted);
    void commentingOnPost(QWidget *commenterWidget);


public slots:
    void likePost(bool like);
    void fixLikeButton(QString state);
    void getAllLikes();

    void commentOnPost();
    void sendComment(QString commentText);
    void updateComment(QString commentId, QString commentText);

    void getAllComments();
    void setAllComments(QVariantList commentsList,
                        QString originatingPostUrl);

    void sharePost();
    void unsharePost();

    void editPost();

    void deletePost();

    void joinGroup();

    void openClickedURL(QUrl url);
    void showHighlightedUrl(QString url);


    void openPostInBrowser();
    void copyPostUrlToClipboard();
    void openParentPost();
    void normalizeTextFormat();

    void triggerResize();

    void redrawImages(QString imageUrl);
    void onImageFailed(QString imageUrl);


protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void leaveEvent(QEvent *event);
    virtual void closeEvent(QCloseEvent *event);
    virtual void showEvent(QShowEvent *event);


private:
    QHBoxLayout *mainLayout;
    QVBoxLayout *leftColumnLayout;
    QVBoxLayout *rightColumnLayout;
    QVBoxLayout *outerLayout;

    QFrame *leftColumnFrame;
    QFrame *rightColumnFrame;

    QString activityId;
    QString postId;
    QString postType;
    QString postUrl;
    QString postAuthorId;
    QString postAuthorName;

    QString postSharedById;
    QString postShareInfoString;
    QString postSharedToCCString;
    QString postShareTime;

    bool postIsOwn;
    bool postIsUnread;
    bool postIsDeleted;
    int highlightType;
    QString unreadPostColor;

    AvatarButton *postAuthorAvatarButton;
    QAction *openPostInBrowserAction;
    QAction *copyPostUrlAction;
    QAction *normalizeTextAction;
    QAction *closeAction;

    QLabel *postAuthorNameLabel;
    HClabel *postCreatedAtLabel;
    QLabel *postGeneratorLabel;
    HClabel *postLocationLabel;
    QLabel *postToLabel;
    QLabel *postCCLabel;
    QLabel *postIsSharedLabel;

    HClabel *postLikesCountLabel;
    QLabel *postCommentsCountLabel;
    HClabel *postSharesCountLabel;

    QPushButton *openParentPostButton;

    //QPushButton *closeButton;

    QLabel *postTitleLabel;
    QLabel *postSummaryLabel;
    QTextBrowser *postText;

    QHBoxLayout *buttonsLayout;
    QPushButton *likeButton;
    QPushButton *commentButton;
    QPushButton *shareButton;

    QPushButton *editButton;
    QPushButton *deleteButton;

    QPushButton *joinLeaveButton;
    QLabel *groupInfoLabel;

    DownloadWidget *downloadWidget;

    QString postTitle;
    QString postImageUrl;
    bool postImageIsAnimated;
    bool postImageFailed;
    QString postAudioUrl;
    QString postVideoUrl;
    QString postFileUrl;
    QString postFileMimeType;
    QString postAttachmentPureUrl;
    QString postOriginalText;
    QVariantMap postParentMap;

    QString postCreatedAtString;
    QString postUpdatedAtString;
    QString postGeneratorString;

    int postWidth;
    int resizesCount;

    QString postLikesUrl;
    int postLikesCount;
    QVariantMap postLikesMap;
    QString postCommentsUrl;
    QString postSharesUrl;


    QStringList pendingImagesList;

    bool standalone;

    QString seeFullImageString;
    QString downloadAttachmentString;

    PumpController *pController;
    GlobalObject *globalObj;
    CommenterBlock *commenter;
};

#endif // POST_H
