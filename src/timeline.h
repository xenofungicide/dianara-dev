/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef TIMELINE_H
#define TIMELINE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QVariantList>
#include <QMap>
#include <QPushButton>
#include <QtCore/qmath.h>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "asobject.h"
#include "asactivity.h"
#include "post.h"
#include "filterchecker.h"
#include "pageselector.h"


class TimeLine : public QWidget
{
    Q_OBJECT

public:
    TimeLine(PumpController::requestTypes timelineType,
             PumpController *pumpController,
             GlobalObject *globalObject,
             FilterChecker *filterChecker,
             QWidget *parent = 0);
    ~TimeLine();

    void clearTimeLineContents();
    void removeOldPosts(int minimumToKeep);
    int getCurrentPage();
    int getTotalPages();
    void updateCurrentPageNumber();

    void resizePosts(QList<Post *> postsToResize, bool resizeAll=false);

    void markPostsAsRead();

    void updateFuzzyTimestamps();

    bool commentingOnAnyPost();
    void notifyBlockedUpdates();

    QList<Post *> getPostsInTimeline();


signals:
    void scrollTo(QAbstractSlider::SliderAction sliderAction);

    void timelineRendered(PumpController::requestTypes timelineType,
                          int newPostCount,
                          int highlightedPostsCount,
                          int deletedPostsCount,
                          int filteredPostsCount,
                          int pendingForNextUpdate);
    void unreadPostsCountChanged(PumpController::requestTypes timelineType,
                                 int newPostCount,
                                 int highlightedCount,
                                 int fullTimelinePostCount);

    void commentingOnPost(QWidget *commenterWidget);


public slots:
    void setTimeLineContents(QVariantList postList, int postsPerPage,
                             QString previousLink, QString nextLink,
                             int totalItems);

    void updatePostsFromMinorFeed(ASObject *object);
    void addLikesFromMinorFeed(QString objectId, QString objectType,
                               QString actorId, QString actorName,
                               QString actorUrl);
    void removeLikesFromMinorFeed(QString objectId, QString objectType,
                                  QString actorId);
    void addReplyFromMinorFeed(ASObject *object);
    void setPostsDeletedFromMinorFeed(ASObject *object);


    void setLikesInPost(QVariantList likesList, QString originatingPostURL);
    void setCommentsInPost(QVariantList commentsList, QString originatingPostURL);
    //void setSharesInPost(...)

    void goToFirstPage();
    void goToPreviousPage();
    void goToNextPage();
    void goToSpecificPage(int pageNumber);

    void showPageSelector();

    void scrollUp();
    void scrollDown();
    void scrollPageUp();
    void scrollPageDown();
    void scrollToTop();
    void scrollToBottom();

    void decreaseUnreadPostsCount(bool wasHighlighted);

    void updateAvatarFollowStates();

protected:


private:
    QVBoxLayout *mainLayout;
    QVBoxLayout *postsLayout;
    QHBoxLayout *bottomLayout;

    PumpController *pController;
    GlobalObject *globalObj;
    FilterChecker *fChecker;

    QPushButton *firstPageButton;
    QPushButton *currentPageButton;
    QPushButton *previousPageButton;
    QPushButton *nextPageButton;

    QPushButton *getNewPendingButton;

    QString previousPageLink;
    QString nextPageLink;
    int fullTimelinePostCount;
    int pendingToReceiveNextTime;
    bool firstLoad;
    bool gettingNew;
    bool wasOnFirstPage;

    int timelineOffset;
    int postsPerPage;
    int unreadPostsCount;
    int highlightedPostsCount;

    QAction *scrollUpAction;
    QAction *scrollDownAction;
    QAction *scrollPageUpAction;
    QAction *scrollPageDownAction;
    QAction *scrollTopAction;
    QAction *scrollBottomAction;

    QAction *previousPageAction;
    QAction *nextPageAction;

    PageSelector *pageSelector;

    PumpController::requestTypes timelineType;
    bool favoritesTimeline;


    QString previousNewestPostId;

    QList<Post *> postsInTimeline;
    QStringList objectsIdList;
};

#endif // TIMELINE_H
