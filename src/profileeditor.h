/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PROFILEEDITOR_H
#define PROFILEEDITOR_H

#include <QWidget>
#include <QFormLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include <QAction>
#include <QCloseEvent>

#include <QDebug>


#include "pumpcontroller.h"
#include "mischelpers.h"


class ProfileEditor : public QWidget
{
    Q_OBJECT

public:
    explicit ProfileEditor(PumpController *pumpController,
                           QWidget *parent = 0);
    ~ProfileEditor();

    void setProfileData(QString avatarUrl, QString fullName,
                        QString hometown, QString bio,
                        QString eMail);

    void toggleWidgetsEnabled(bool state);

signals:

public slots:
    void findAvatarFile();
    void saveProfile();
    void sendProfileData(QString newImageUrl = QString());


protected:
    virtual void closeEvent(QCloseEvent *event);


private:
    PumpController *pController;


    QVBoxLayout *mainLayout;
    QFormLayout *topLayout;
    QHBoxLayout *avatarLayout;
    QHBoxLayout *bottomLayout;

    QLabel *avatarLabel;
    QPushButton *changeAvatarButton;
    bool avatarChanged;

    QLabel *webfingerLabel;
    QLabel *emailLabel;

    QLineEdit *fullNameLineEdit;
    QLineEdit *hometownLineEdit;
    QTextEdit *bioTextEdit;

    QPushButton *saveButton;
    QPushButton *cancelButton;

    QAction *cancelAction;

    QString currentAvatarURL;
    QString newAvatarFilename;
    QString newAvatarContentType;
};

#endif // PROFILEEDITOR_H
