/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QWidget>
#include <QIcon>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QStackedWidget>
#include <QTabWidget>
#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QSettings>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QCloseEvent>

#include <QDebug>

#include "globalobject.h"
#include "fontpicker.h"
#include "colorpicker.h"
#include "notifications.h"
#include "proxydialog.h"


class ConfigDialog : public QWidget
{
    Q_OBJECT

public:
    ConfigDialog(GlobalObject *globalObject,
                 QString dataDirectory,
                 int updateInterval,
                 int tabsPosition,
                 bool tabsMovable,
                 FDNotifications *notifier,
                 QWidget *parent);
    ~ConfigDialog();

    void syncNotifierOptions();
    QString checkNotifications(int notificationStyle);


signals:
    void configurationChanged();
    void filterEditorRequested();


public slots:
    void saveConfiguration();
    void pickCustomIconFile();
    void showDemoNotification(int notificationStyle);


protected:
    virtual void closeEvent(QCloseEvent *event);
    virtual void hideEvent(QHideEvent *event);


private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *topLayout;


    QListWidget *categoriesListWidget;
    QStackedWidget *categoriesStackedWidget;


    // Page 1, general options
    QWidget *generalOptionsWidget;
    QFormLayout *generalOptionsLayout;

    QSpinBox *updateIntervalSpinbox;
    QCheckBox *publicPostsCheckbox;
    QComboBox *tabsPositionCombobox;
    QCheckBox *tabsMovableCheckbox;

    QPushButton *proxyConfigButton;
    ProxyDialog *proxyDialog;

    QPushButton *filterEditorButton;


    // Page 2, fonts
    QWidget *fontOptionsWidget;
    QVBoxLayout *fontOptionsLayout;

    FontPicker *fontPicker1;
    FontPicker *fontPicker2;
    FontPicker *fontPicker3;
    FontPicker *fontPicker4;


    // Page 3, colors
    QWidget *colorOptionsWidget;
    QVBoxLayout *colorOptionsLayout;

    ColorPicker *colorPicker1;
    ColorPicker *colorPicker2;
    ColorPicker *colorPicker3;
    ColorPicker *colorPicker4;
    ColorPicker *colorPicker5;
    ColorPicker *colorPicker6;


    // Page 4, timelines options
    QWidget *timelinesOptionsWidget;
    QFormLayout *timelinesOptionsLayout;

    QSpinBox *postsPerPageMainSpinbox;
    QSpinBox *postsPerPageOtherSpinbox;

    QComboBox *minorFeedSnippetsCombobox;
    QSpinBox *snippetLimitSpinbox;

    QCheckBox *showDeletedCheckbox;
    QCheckBox *hideDuplicatesCheckbox;


    // Page 5, posts options
    QWidget *postsOptionsWidget;
    QFormLayout *postsOptionsLayout;

    QComboBox *postAvatarSizeCombobox;
    QCheckBox *showExtendedSharesCheckbox;
    QCheckBox *showExtraInfoCheckbox;
    QCheckBox *postHLAuthorCommentsCheckbox;
    QCheckBox *postHLOwnCommentsCheckbox;

    QCheckBox *postIgnoreSslInImages;


    // Page 6, composer options
    QWidget *composerOptionsWidget;
    QFormLayout *composerOptionsLayout;

    QCheckBox *useFilenameAsTitleCheckbox;
    QCheckBox *showCharacterCounterCheckbox;


    // Page 7, notifications options
    QWidget *notificationOptionsWidget;
    QFormLayout *notificationOptionsLayout;

    QComboBox *notificationStyleCombobox;
    QLabel *notificationsStatusLabel;
    QCheckBox *notifyNewTLCheckbox;
    QCheckBox *notifyHLTLCheckbox;
    QCheckBox *notifyNewMWCheckbox;
    QCheckBox *notifyHLMWCheckbox;


    // Page 8, system tray options
    QWidget *systrayOptionsWidget;
    QFormLayout *systrayOptionsLayout;

    QComboBox *systrayIconTypeCombobox;
    QPushButton *systrayCustomIconButton;
    QString systrayCustomIconFN;


    // Widgets below the tab widget
    QLabel *dataDirectoryLabel;

    QHBoxLayout *buttonsLayout;
    QPushButton *saveConfigButton;
    QPushButton *cancelButton;


    QAction *closeAction;

    FDNotifications *fdNotifier;
    GlobalObject *globalObj;
};

#endif // CONFIGDIALOG_H
