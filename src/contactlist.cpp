/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "contactlist.h"

ContactList::ContactList(PumpController *pumpController,
                         GlobalObject *globalObject,
                         QString listType,
                         QWidget *parent) : QWidget(parent)
{
    this->pController = pumpController;
    this->globalObj = globalObject;


    QString filterNote = tr("Type a partial name or ID to find a contact...")
                         + " (Control+F)";
    this->filterLineEdit = new QLineEdit(this);
    filterLineEdit->setPlaceholderText(filterNote);
    filterLineEdit->setToolTip("<b></b>" + filterNote); // HTMLized for wordwrapping
    connect(filterLineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(filterList(QString)));

    this->removeFilterButton = new QPushButton(QIcon::fromTheme("view-list-icons"),
                                               tr("F&ull List"),
                                               this);
    removeFilterButton->setDisabled(true); // Disabled initially, until a search happens
    connect(removeFilterButton, SIGNAL(clicked()),
            filterLineEdit, SLOT(clear()));


    goToFilterAction = new QAction(this);
    goToFilterAction->setShortcut(QKeySequence("Ctrl+F"));
    connect(goToFilterAction, SIGNAL(triggered()),
            filterLineEdit, SLOT(setFocus()));
    this->addAction(goToFilterAction);


    // Layout
    this->filterLayout = new QHBoxLayout();
    filterLayout->addWidget(filterLineEdit);
    filterLayout->addWidget(removeFilterButton);

    this->contactsLayout = new QVBoxLayout();

    this->contactsWidget = new QWidget(this);
    contactsWidget->setLayout(contactsLayout);

    this->contactsScrollArea = new QScrollArea(this);
    contactsScrollArea->setWidget(contactsWidget);
    contactsScrollArea->setWidgetResizable(true);
    contactsScrollArea->setFrameStyle(QFrame::NoFrame);
    contactsScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);


    this->mainLayout = new QVBoxLayout();
    mainLayout->addWidget(contactsScrollArea);
    mainLayout->addLayout(filterLayout);
    this->setLayout(mainLayout);


    // Add demo contacts
    QVariantMap demoContactData;
    QVariantMap demoContactHometown;
    QVariantMap demoContactFollowed;

    this->isFollowing = false;
    if (listType == "following")
    {
        isFollowing = true;

        demoContactData.insert("displayName",  "Demo Contact");
        demoContactData.insert("id",           "democontact@pumpserver.org");
        demoContactData.insert("url",          "http://jancoding.wordpress.com");
        demoContactFollowed.insert("followed", "true");
    }
    else
    {
        demoContactData.insert("displayName",  "Demo Follower");
        demoContactData.insert("id",           "demofollower@pumpserver.org");
        demoContactData.insert("url",          "http://dianara.nongnu.org");
        demoContactFollowed.insert("followed", "false");
    }

    demoContactHometown.insert("displayName",  "Some city");
    demoContactData.insert("location", demoContactHometown);

    demoContactData.insert("pump_io",  demoContactFollowed);

    demoContactData.insert("published", "2013-05-01T00:00:00Z"); // Dianara's birthday


    ASPerson *demoContactPerson = new ASPerson(demoContactData);
    ContactCard *demoContactCard = new ContactCard(this->pController,
                                                   this->globalObj,
                                                   demoContactPerson, this);
    this->contactsLayout->addWidget(demoContactCard);
    this->contactsInList.append(demoContactCard);


    qDebug() << "ContactList created";
}

ContactList::~ContactList()
{
    qDebug() << "ContactList destroyed";
}



void ContactList::clearListContents()
{
    foreach (ContactCard *card, contactsInList)
    {
        this->contactsLayout->removeWidget(card);
        delete card;
    }
    this->contactsInList.clear();

    this->contactsStringForExport.clear();

    if (this->isFollowing)
    {
        this->globalObj->clearNickCompletionModel();
    }
}


void ContactList::setListContents(QVariantList contactList)
{
    qDebug() << "ContactList; Setting list contents";

    QString contactInfoLineString;
    QStringList followingIdStringList;

    foreach (QVariant contact, contactList)
    {
        ASPerson *person = new ASPerson(contact.toMap());
        ContactCard *contactCard = new ContactCard(this->pController,
                                                   this->globalObj,
                                                   person, this);
        this->contactsLayout->addWidget(contactCard);
        this->contactsInList.append(contactCard);

        // Info for the string list used when exporting
        contactInfoLineString = person->getName()
                              + "  <"
                              + person->getId()
                              + ">\n";
        contactsStringForExport.append(contactInfoLineString);

        if (this->isFollowing)
        {
            // Add to internal following list for PumpController
            followingIdStringList.append(person->getId());

            // Add also to GlobalObject's model for nick completion
            this->globalObj->addToNickCompletionModel(person->getId(),
                                                      person->getNameWithFallback(),
                                                      person->getUrl());
        }
    } // end foreach


    // Batch of contacts added to list, add them also to the internal list
    if (this->isFollowing)
    {
        this->pController->updateInternalFollowingIdList(followingIdStringList);
        this->globalObj->sortNickCompletionModel(); // FIXME: should only sort when the whole list is done
    }

    this->filterLineEdit->clear(); // Unfilter the list
}



QString ContactList::getContactsStringForExport()
{
    return this->contactsStringForExport;
}



/*****************************************************************************/
/*********************************** SLOTS ***********************************/
/*****************************************************************************/


void ContactList::filterList(QString filterText)
{
    qDebug() << "Filtering for contacts matching:" << filterText;

    if (!filterText.isEmpty())
    {
        foreach (ContactCard *card, contactsInList)
        {
            if (card->getNameAndIdString().contains(filterText, Qt::CaseInsensitive))
            {
                card->show();
            }
            else
            {
                card->hide();
            }
        }

        removeFilterButton->setEnabled(true);
    }
    else // If no filter at all, more optimized version showing all
    {
        foreach (ContactCard *card, contactsInList)
        {
            card->show();
        }

        removeFilterButton->setDisabled(true);
    }
}


void ContactList::addSingleContact(ASPerson *contact)
{
    ContactCard *card = new ContactCard(this->pController,
                                        this->globalObj,
                                        contact,
                                        this);

    this->contactsLayout->insertWidget(0, card);
    this->contactsInList.append(card);

    emit contactCountChanged(1);

    // This check is actually unnecessary, since this slot is only called
    if (this->isFollowing) // for the 'following' list
    {
        QStringList contactsToAdd;
        contactsToAdd.append(contact->getId());
        this->pController->updateInternalFollowingIdList(contactsToAdd);

        // Add also to GlobalObject's model for nick completion
        this->globalObj->addToNickCompletionModel(contact->getId(),
                                                  contact->getNameWithFallback(),
                                                  contact->getUrl());
    }
}


void ContactList::removeSingleContact(ASPerson *contact)
{
    foreach (ContactCard *card, contactsInList)
    {
        if (card->getId() == contact->getId())
        {
            emit contactCountChanged(-1);

            this->pController->removeFromInternalFollowingList(contact->getId());

            // Remove from GlobalObject's model for nick completion, too
            this->globalObj->removeFromNickCompletionModel(contact->getId());

            card->setDisabled(true);
        }
    }

    contact->deleteLater();
}
