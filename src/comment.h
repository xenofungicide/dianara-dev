/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef COMMENT_H
#define COMMENT_H

#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QVariantMap>
#include <QMessageBox>
#include <QEvent>
#include <QResizeEvent>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "mischelpers.h"
#include "timestamp.h"
#include "asobject.h"
#include "avatarbutton.h"
#include "hclabel.h"


class Comment : public QFrame
{
    Q_OBJECT

public:
    explicit Comment(PumpController *pumpController,
                     GlobalObject *globalObject,
                     ASObject *commentObject,
                     QWidget *parent = 0);
    ~Comment();

    void updateDataFromObject(ASObject *object);

    void fixLikeLabelText();
    void setLikesCount(QString count, QVariantList namesVariantList);

    void setFuzzyTimestamps();
    void syncAvatarFollowState();

    void setCommentContents();
    void getPendingImages();

    QString getObjectId();

    void setHint(QString color="");


signals:
    void commentQuoteRequested(QString content);
    void commentEditRequested(QString id, QString content);


public slots:
    void likeComment(QString clickedLink);
    void saveCommentSelectedText();
    void quoteComment();
    void editComment();
    void deleteComment();

    void showUrlInfo(QString url);

    void redrawImages(QString imageUrl);

protected:
    virtual void leaveEvent(QEvent *event);
    virtual void resizeEvent(QResizeEvent *event);


private:
    QHBoxLayout *mainLayout;
    QVBoxLayout *leftLayout;
    QVBoxLayout *rightLayout;
    QHBoxLayout *rightTopLayout;

    AvatarButton *avatarButton;
    QLabel *fullNameLabel;
    HClabel *timestampLabel;
    QLabel *contentLabel;
    HClabel *likesCountLabel;

    QWidget *hintWidget;

    QLabel *likeLabel;
    QLabel *quoteLabel;
    QLabel *editLabel;
    QLabel *deleteLabel;


    QString commentId;
    QString objectType;

    bool commentIsOwn;
    bool commentIsLiked;
    QString createdAt;
    QString updatedAt;
    QString commentOriginalText;
    QStringList pendingImagesList;

    QString commentSelectedText;

    PumpController *pController;
    GlobalObject *globalObj;
};

#endif // COMMENT_H
