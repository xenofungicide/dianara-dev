/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PUMPCONTROLLER_H
#define PUMPCONTROLLER_H

#include <QObject>
#include <QStringList>
#include <QByteArray>
#include <QUrl>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QUrlQuery>
#endif
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QSslError> // TMP
#include <QNetworkProxy>
#include <QTimer>
#include <QFile>

#include <QSettings>
#include <QDesktopServices>

#include <QDebug>


/// For JSON parsing
#include <QVariant>
#include <QVariantMap>
#include <qjson/parser.h>
#include <qjson/serializer.h>

// For OAuth authentication
#include <QtOAuth>

#include "mischelpers.h"
#include "asperson.h"
#include "asobject.h"


class PumpController : public QObject
{
    Q_OBJECT

public:
    enum requestTypes
    {
        NoRequest,

        ClientRegistrationRequest,
        TokenRequest,

        UserProfileRequest,
        UpdateProfileRequest,

        FollowingListRequest,
        FollowersListRequest,
        ListsListRequest,
        CreatePersonListRequest,
        DeletePersonListRequest,
        PersonListRequest,
        AddMemberToListRequest,
        RemoveMemberFromListRequest,

        CreateGroupRequest,
        DeleteGroupRequest,
        JoinGroupRequest,
        LeaveGroupRequest,

        MainTimelineRequest,
        DirectTimelineRequest,
        ActivityTimelineRequest,
        FavoritesTimelineRequest,

        PostLikesRequest,
        PostCommentsRequest,
        PostSharesRequest,

        MinorFeedMainRequest,
        MinorFeedDirectRequest,
        MinorFeedActivityRequest,

        PublishPostRequest,
        LikePostRequest,
        CommentPostRequest,
        SharePostRequest,
        UnsharePostRequest,
        DeletePostRequest,

        UpdatePostRequest,
        UpdateCommentRequest,

        FollowContactRequest,
        UnfollowContactRequest,


        AvatarRequest,
        ImageRequest,
        MediaRequest,

        UploadFileRequest,
        UploadMediaForPostRequest,
        UploadAvatarRequest,
        PublishAvatarRequest
    };


    explicit PumpController(QObject *parent = 0);
    ~PumpController();

    void setProxyConfig(QNetworkProxy::ProxyType proxyType,
                        QString hostname, int port,
                        bool useAuth,
                        QString user, QString password);
    bool needsProxyPassword();
    void setProxyPassword(QString password);

    void setPostsPerPageMain(int ppp);
    void setPostsPerPageOther(int ppp);


    void setNewUserId(QString userId);
    void setUserCredentials(QString userId);
    QString currentUserId();
    QString currentUsername();
    QString currentFollowersUrl();
    int currentFollowersCount();
    int currentFollowingCount();
    bool currentlyAuthorized();

    void getUserProfile(QString userId);
    void updateUserProfile(QString avatarUrl, QString fullName,
                           QString hometown, QString bio);

    void enqueueAvatarForDownload(QString url);
    void enqueueImageForDownload(QString url);
    void getAvatar(QString avatarUrl);
    void getImage(QString imageUrl);
    QNetworkReply *getMedia(QString mediaUrl);
    void notifyAvatarStored(QString avatarUrl, QString avatarFilename);
    void notifyImageStored(QString imageUrl);

    void getContactList(QString listType, int offset=0);
    bool userInFollowing(QString contactId);
    void updateInternalFollowingIdList(QStringList idList);
    void removeFromInternalFollowingList(QString id);

    void getListsList();
    void createPersonList(QString name, QString description);
    void deletePersonList(QString id);
    void getPersonList(QString url);
    void addPersonToList(QString listId, QString personId);
    void removePersonFromList(QString listId, QString personId);

    void createGroup(QString name, QString summary, QString description);
    void joinGroup(QString id);
    void leaveGroup(QString id);

    void getPostLikes(QString postLikesUrl);
    void getPostComments(QString postCommentsUrl);
    void getPostShares(QString postSharesUrl);

    void getFeed(requestTypes feedType, int itemCount,
                 QString url = "", int feedOffset = 0);
    static QStringList getFeedNameAndPath(int feedType);
    QString getFeedApiUrl(int feedType);

    QNetworkRequest prepareRequest(QString url,
                                   QOAuth::HttpMethod method,
                                   requestTypes requestType,
                                   QOAuth::ParamMap paramMap = QOAuth::ParamMap(),
                                   QString contentTypeString="application/json");

    QByteArray prepareJSON(QVariantMap jsonVariantMap);

    QNetworkReply *uploadFile(QString filename, QString contentType,
                              requestTypes uploadType = UploadFileRequest);

    QList<QVariantList> processAudience(QMap<QString,QString> audienceMap);

    bool urlIsInOurHost(QString url);

    void showTransientMessage(QString message);

    void showStatusMessageAndLogIt(QString message, QString url="");

    void setIgnoreSslErrors(bool state);
    void setIgnoreSslInImages(bool state);


signals:
    void openingAuthorizeURL(QUrl url);
    void authorizationFailed(QString errorTitle, QString errorMessage);
    void authorizationStatusChanged(bool authorized);
    void initializationCompleted();

    void profileReceived(QString avatarURL, QString fullName,
                         QString hometown, QString bio,
                         QString email);
    void contactListReceived(QString listType, QVariantList contactList,
                             int totalReceivedCount);
    void contactFollowed(ASPerson *contact);
    void contactUnfollowed(ASPerson *contact);
    void followingListChanged();

    void listsListReceived(QVariantList listsList);
    void personListReceived(QVariantList personList, QString listUrl);
    void personAddedToList(QString id, QString name, QString avatarUrl);
    void personRemovedFromList(QString id);


    void mainTimelineReceived(QVariantList postList, int postsPerPage,
                              QString previousLink, QString nextLink,
                              int totalItems);
    void directTimelineReceived(QVariantList postList, int postsPerPage,
                                QString previousLink, QString nextLink,
                                int totalItems);
    void activityTimelineReceived(QVariantList postList, int postsPerPage,
                                  QString previousLink, QString nextLink,
                                  int totalItems);
    void favoritesTimelineReceived(QVariantList postList, int postsPerPage,
                                   QString previousLink, QString nextLink,
                                   int totalItems);

    void likesReceived(QVariantList likesList, QString originatingPostURL);
    void commentsReceived(QVariantList commentsList, QString originatingPostURL);

    void minorFeedMainReceived(QVariantList activitiesList,
                               QString previousLink, QString nextLink,
                               int totalItemCount);
    void minorFeedDirectReceived(QVariantList activitiesList,
                                 QString previousLink, QString nextLink,
                                 int totalItemCount);
    void minorFeedActivityReceived(QVariantList activitiesList,
                                   QString previousLink, QString nextLink,
                                   int totalItemCount);

    void avatarPictureReceived(QByteArray pictureData, QString pictureUrl);
    void imageReceived(QByteArray pictureData, QString pictureUrl);
    void imageFailed(QString imageUrl);
    void downloadCompleted(QString fileUrl);
    void downloadFailed(QString fileUrl);
    void avatarStored(QString avatarUrl, QString avatarFilename);
    void imageStored(QString imageUrl);

    void postPublished();
    void postPublishingFailed();
    void likeSet();
    void commentPosted();
    void commentPostingFailed();

    void userDidSomething();

    void avatarUploaded(QString url);

    void showNotification(QString message);
    void currentJobChanged(QString message);
    void transientStatusBarMessage(QString message);
    void logMessage(QString message, QString url="");


public slots:
    void requestFinished(QNetworkReply *reply);

    void sslErrorsHandler(QNetworkReply *reply, QList<QSslError> errorList);

    void getToken();
    void authorizeApplication(QString verifierCode);

    void getInitialData();


    void postNote(QMap<QString,QString> audienceMap,
                  QString postText,
                  QString postTitle);
    QNetworkReply *postMedia(QMap<QString,QString> audienceMap,
                             QString postText, QString postTitle,
                             QString mediaFilename, QString mediaType,
                             QString mimeContentType);
    void postMediaStepTwo(QString id);

    void postAvatarStepTwo(QString id);


    void updatePost(QString id, QString type, QString content, QString title);

    void likePost(QString postID, QString postType, bool like);

    void addComment(QString comment, QString postID, QString postType);
    void updateComment(QString id, QString content);

    void sharePost(QString postID, QString postType);
    void unsharePost(QString postId, QString postType);
    void deletePost(QString postID, QString postType);

    void followContact(QString address);
    void unfollowContact(QString address);


private:
    QNetworkAccessManager nam;
    QByteArray userAgentString;

    // QOAuth-related
    QOAuth::Interface *qoauth;

    bool isApplicationAuthorized;

    QString clientID;
    QString clientSecret;

    QByteArray token;
    QByteArray tokenSecret;


    QString userId;  // Full webfinger address, user@host.tld
    QString userName;
    QString serverURL;
    QString apiBaseUrl;
    QString apiFeedUrl;

    bool proxyUsesAuth;

    QString userFollowersURL;
    int userFollowersCount;
    int userFollowingCount;
    QStringList followingIdList;
    int totalReceivedFollowers;
    int totalReceivedFollowing;

    QTimer *initialDataTimer;
    int initialDataStep;
    int initialDataAttempts;
    bool haveProfile;
    bool haveFollowing;
    bool haveFollowers;
    bool havePersonLists;
    bool haveMainTL;
    bool haveDirectTL;
    bool haveActivityTL;
    bool haveFavoritesTL;
    bool haveMainMF;
    bool haveDirectMF;
    bool haveActivityMF;


    int postsPerPageMain;
    int postsPerPageOther;


    // For multi-step operations in posts
    QString currentPostTitle;
    QString currentPostDescription;
    QMap<QString, QString> currentAudienceMap;
    QString currentPostType;

    // Avatars / Images queue
    QStringList pendingAvatarsList;
    QStringList pendingImagesList;

    bool ignoreSslErrors;
    bool ignoreSslInImages;
};

#endif // PUMPCONTROLLER_H
