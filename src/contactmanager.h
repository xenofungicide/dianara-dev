/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef CONTACTMANAGER_H
#define CONTACTMANAGER_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QVariantList>
#include <QMap>
#include <QString>
#include <QPushButton>
#include <QLineEdit>
#include <QTabWidget>
#include <QScrollArea>
#include <QMenu>
#include <QFileDialog>
#include <QResizeEvent>

#include <QDebug>

#include "pumpcontroller.h"
#include "contactlist.h"
#include "listsmanager.h"
#include "globalobject.h"



class ContactManager : public QWidget
{
    Q_OBJECT

public:
    ContactManager(PumpController *pumpController,
                   GlobalObject *globalObject,
                   QWidget *parent = 0);
    ~ContactManager();

    void setTabLabels();

    void exportContactsToFile(QString listType);

signals:

public slots:
    void setContactListsContents(QString listType,
                                QVariantList contactList,
                                int totalReceivedCount);
    void setListsListContents(QVariantList listsList);

    void changeFollowingCount(int difference);

    void refreshFollowing();
    void refreshFollowers();

    void exportFollowing();
    void exportFollowers();

    void refreshPersonLists();

    void toggleFollowButton(QString currentAddress);
    void followContact();


protected:


private:
    QVBoxLayout *mainLayout;

    QHBoxLayout *topLayout;
    QLabel *enterAddressLabel;
    QLineEdit *addressLineEdit;
    QPushButton *followButton;

    QTabWidget *tabWidget;

    ContactList *followingWidget;
    int followingCount;

    ContactList *followersWidget;
    int followersCount;

    ListsManager *listsManager;
    QScrollArea *listsScrollArea;
    int listsCount;


    QPushButton *optionsButton;
    QMenu *optionsMenu;

    PumpController *pController;
};

#endif // CONTACTMANAGER_H
