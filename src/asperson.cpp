/*
 *   This file is part of Dianara
 *   Copyright 2012-2015  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "asperson.h"

ASPerson::ASPerson(QVariantMap personMap,
                   QObject *parent) : QObject(parent)
{
    this->id = this->cleanupId(personMap.value("id").toString());
    this->name = personMap.value("displayName").toString().trimmed();
    this->avatar = personMap.value("image").toMap().value("url").toString();
    this->url = personMap.value("url").toString();
    this->hometown = personMap.value("location").toMap().value("displayName").toString().trimmed();
    this->bio = personMap.value("summary").toString();

    this->followed = personMap.value("pump_io").toMap().value("followed").toString();

    this->createdAt = personMap.value("published").toString();
    this->updatedAt = personMap.value("updated").toString();


    //qDebug() << "ASPerson created" << this->id;
}


ASPerson::~ASPerson()
{
    //qDebug() << "ASPerson destroyed" << this->id;
}



void ASPerson::updateDataFromPerson(ASPerson *person)
{
    this->id = person->getId();
    this->name = person->getNameWithFallback();
    this->avatar = person->getAvatar();
    this->url = person->getUrl();
    this->hometown = person->getHometown();
    this->bio = person->getBio();

    this->followed = person->isFollowed() ? "true" : "false";

    this->createdAt = person->getCreatedAt();
    this->updatedAt = person->getupdatedAt();
}


QString ASPerson::getId()
{
    return id;
}

QString ASPerson::cleanupId(QString originalId)
{
    if (originalId.startsWith("acct:"))
    {
        originalId.remove(0,5);
    }

    return originalId;
}



QString ASPerson::getName()
{
    return name;
}

QString ASPerson::getNameWithFallback()
{
    if (!name.isEmpty())
    {
        return name;
    }
    else
    {
        return id;
    }
}

QString ASPerson::getHometown()
{
    return hometown;
}

QString ASPerson::getBio()
{
    return bio;
}

QString ASPerson::getAvatar()
{
    return avatar;
}

QString ASPerson::getUrl()
{
    return url;
}

QString ASPerson::getTooltipInfo()
{
    if (this->id.isEmpty())
    {
        return QString(); // If there's no ID, there's no valid person data
    }

    QString tooltipContents = "<b>" + this->name + "</b><br>";
    tooltipContents.append("<b><i>" + this->id + "</i></b>");

    if (!this->hometown.isEmpty() || !this->bio.isEmpty())
    {
        // More data coming, add a line
        tooltipContents.append("<hr><br>");
    }

    if (!this->hometown.isEmpty())
    {
        tooltipContents.append("<b>" + tr("Hometown") + "</b>: "
                               + hometown);
        tooltipContents.append("<br><br>");
    }

    if (!this->bio.isEmpty())
    {
        tooltipContents.append(this->bio);
    }

    tooltipContents.replace("\n", "<br>"); // HTML newlines


    return tooltipContents;
}



bool ASPerson::isFollowed()
{
    if (this->followed == "true")
    {
        return true;
    }
    else
    {
        return false;
    }
}


QString ASPerson::getCreatedAt()
{
    return this->createdAt;
}

QString ASPerson::getupdatedAt()
{
    return this->updatedAt;
}
