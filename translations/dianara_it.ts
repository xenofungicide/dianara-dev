<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="88"/>
        <location filename="../src/asactivity.cpp" line="121"/>
        <source>Public</source>
        <translation>Pubblico</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="389"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 di %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="231"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="236"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>Articolo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="241"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="246"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="251"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="256"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="261"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>Commento</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="266"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="271"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>Collezione</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="276"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="332"/>
        <source>No detailed location</source>
        <translation>Nessuna località dettagliata</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="357"/>
        <source>Deleted on %1</source>
        <translation>Eliminato il %1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="513"/>
        <location filename="../src/asobject.cpp" line="583"/>
        <source>and one other</source>
        <translation>e nessun altro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="517"/>
        <location filename="../src/asobject.cpp" line="587"/>
        <source>and %1 others</source>
        <translation>e %1 altri</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="138"/>
        <source>Hometown</source>
        <translation>Città</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="74"/>
        <source>Your Pump.io address:</source>
        <translation>Il tuo indirizzo Pump.io:</translation>
    </message>
    <message>
        <source>Webfinger ID, like username@pumpserver.org</source>
        <translation type="obsolete">Webfinger ID, esempio nomeutente@pumpserver.org</translation>
    </message>
    <message>
        <source>Your address, as username@server</source>
        <translation type="obsolete">Il tuo indirizzo, come nomeutente@server</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="81"/>
        <source>Get &amp;Verifier Code</source>
        <translation>Ottieni il Codice di &amp;Verifica</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="107"/>
        <source>Verifier code:</source>
        <translation>Codice di Verifica:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="109"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation>Inserisci o incolla il codice di verifica fornito dal tuo server Pump qui</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="143"/>
        <source>&amp;Save Details</source>
        <translation>&amp;Salva i Dati</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="357"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>Se il browser non si apre automaticamente, copia questo indirizzo manualmente</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>Configurazione Account</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="42"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>Prima inserisci il tuo Webfinger ID, il tuo indirizzo Pump.io.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="44"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>Il tuo indirizzo assomiglia a nomeutente@pumpserver.org, e lo puoi trovare sul tuo profilo, nell&apos;interfaccia web.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="48"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>Se il tuo profilo, per esempio, è https://pumpserver.org/nomeutente, di conseguenza il tuo indirizzo è nomeutente@pumpserver.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="52"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>Se non hai ancora un account, puoi iscriverti su %1. Questo link ti porterà su un server pubblico casuale.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="60"/>
        <source>If you need help: %1</source>
        <translation>Se hai bisogno di aiuto: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="63"/>
        <source>Pump.io User Guide</source>
        <translation>Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="76"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation>Il tuo indirizzo, tipo nomeutente@serverpump.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="83"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>Dopo aver cliccato questo bottone, il browser web si dovrebbe aprire, richiedendo l&apos;autorizzazione per Dianara</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="90"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>Una volta autorizzato Dianara dall&apos;interfaccia web del tuo server Pump.io, riceverai un codice chiamato VERIFIER. Copialo e incollalo nel campo qui sotto.</translation>
    </message>
    <message>
        <source>Enter the verifier code provided by your Pump server here</source>
        <translation type="obsolete">Inserisci il codice di verifica ottenuto dal tuo server Pump.io qui</translation>
    </message>
    <message>
        <source>Paste the verifier here</source>
        <translation type="obsolete">Incolla il codice di verifica qui</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="116"/>
        <source>&amp;Authorize Application</source>
        <translation>&amp;Autorizza Applicazione</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="151"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="159"/>
        <source>Your account is properly configured.</source>
        <translation>Il tuo account è configurato correttamente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="163"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>Premi Sblocca se vuoi configurare un altro account.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="171"/>
        <source>&amp;Unlock</source>
        <translation>&amp;Sblocca</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="298"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>Ora si avviera il browser web, dove potrai ottenere il codice di verifica</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="307"/>
        <source>Your Pump address is invalid</source>
        <translation>Il tuo indirizzo Pump.io non è valido</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="326"/>
        <source>Verifier code is empty</source>
        <translation>Il campo del codice di verifica è vuoto</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="338"/>
        <source>Dianara is authorized to access your data</source>
        <translation>Dianara è autorizzato ad acceder ai tuoi dati</translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="32"/>
        <source>&apos;To&apos; List</source>
        <translation>Lista &apos;A&apos;</translation>
    </message>
    <message>
        <source>&apos;CC&apos; List</source>
        <translation type="obsolete">Lista &apos;CC&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="36"/>
        <source>&apos;Cc&apos; List</source>
        <translation>Lista &apos;Cc&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="57"/>
        <source>&amp;Add to Selected</source>
        <translation>&amp;Aggiungi ai Selezionati</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="70"/>
        <source>All Contacts</source>
        <translation>Tutti i Contatti</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="75"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <translation>Seleziona le persone dalla lista a sinistra.
Puoi trascinarle con il mouse, click o doppio click su di esse, o selezionarle e usare il bottone qui sotto.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="89"/>
        <source>Clear &amp;List</source>
        <translation>Pulisci la &amp;Lista</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="95"/>
        <source>&amp;Done</source>
        <translation>&amp;Fatto</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="100"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="119"/>
        <source>Selected People</source>
        <translation>Persone Selezionate</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="138"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>Apri il profilo di %1 nel browser web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="142"/>
        <source>Open your profile in web browser</source>
        <translation>Apri il tuo profilo nel browser web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="161"/>
        <source>Send message to %1</source>
        <translation>Invia messaggio a %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="212"/>
        <source>Stop following</source>
        <translation>Smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="223"/>
        <source>Follow</source>
        <translation>Segui</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="264"/>
        <source>Stop following?</source>
        <translation>Smettere di seguire?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="265"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Sei sicuro di voler smettere di seguire %1?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="267"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Si, smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="267"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="34"/>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="249"/>
        <source>Posted on %1</source>
        <translation>Pubblicato il %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="254"/>
        <source>Modified on %1</source>
        <translation>Modificato il %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="112"/>
        <source>Like or unlike this comment</source>
        <translation>Decidi se ti piace o non ti piace questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="119"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>Quotare</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="126"/>
        <source>Reply quoting this comment</source>
        <translation>Rispondi quotando questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="134"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="140"/>
        <source>Modify this comment</source>
        <translation>Modifica questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="146"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="152"/>
        <source>Erase this comment</source>
        <translation>Elimina questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="293"/>
        <source>Unlike</source>
        <translation>Non mi piace</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="299"/>
        <source>Like</source>
        <translation>Mi piace</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="321"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>A %1 piace questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="315"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>A %1 piace questo commento</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="506"/>
        <source>WARNING: Delete comment?</source>
        <translation>ATTENZIONE: Cancellare il commento?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="507"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>Sei sicuro di voler cancellare questo commento?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="508"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, cancellalo</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="508"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="120"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>Premi Control+Invio per inviare il commento con la tastiera</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="37"/>
        <source>Reload comments</source>
        <translation>Ricarica commenti</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="117"/>
        <location filename="../src/commenterblock.cpp" line="416"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>Commentare</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="127"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancella</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="129"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>Premi ESC per annullare il commento, se non c&apos;è testo</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="192"/>
        <source>Show all %1 comments</source>
        <translation>Mostra tutti i %1 commenti</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="451"/>
        <source>Error: Already composing</source>
        <translation>Errore: stai già scrivendo</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="452"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>Non puoi modificare ora il commento, perchè ne stai già scrivendo un altro.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="465"/>
        <source>Editing comment</source>
        <translation>Modificando il commento</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="520"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>Invio del commento fallito.

Prova di nuovo.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="550"/>
        <source>Sending comment...</source>
        <translation>Inviando il commento...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="555"/>
        <source>Updating comment...</source>
        <translation>Aggiornando il commento...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="564"/>
        <source>Comment is empty.</source>
        <translation>Il commento è vuoto.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="170"/>
        <source>Type a message here to post it</source>
        <translation>Scrivi qui un messaggio per pubblicarlo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="35"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>Clicca qui o premi Control+N per pubblicare una nota...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="42"/>
        <source>Symbols</source>
        <translation>Simboli</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Formatting</source>
        <translation>Formattazione</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="62"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="66"/>
        <source>Bold</source>
        <translation>Grassetto</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="71"/>
        <source>Italic</source>
        <translation>Corsivo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="76"/>
        <source>Underline</source>
        <translation>Sottolineato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="81"/>
        <source>Strikethrough</source>
        <translation>Barrato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="88"/>
        <source>Header</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="93"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="97"/>
        <source>Table</source>
        <translation>Tabella</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="101"/>
        <source>Preformatted block</source>
        <translation>Blocco preformattato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="105"/>
        <source>Quote block</source>
        <translation>Blocco citazioni</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="113"/>
        <source>Make a link</source>
        <translation>Crea un link</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="118"/>
        <source>Insert an image from a web site</source>
        <translation>Inserisci un&apos;immagine da un sito web</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="123"/>
        <source>Insert line</source>
        <translation>Inserisci una linea</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="413"/>
        <source>Insert as image?</source>
        <translation>Inserire come immagine?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="414"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>Il collegamento che stai incollando sembra puntare ad un&apos;immagine.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="416"/>
        <source>Insert as visible image</source>
        <translation>Inserisci come immagine visibile</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="417"/>
        <source>Insert as link</source>
        <translation>Inserisci come collegamento</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="557"/>
        <source>Table Size</source>
        <translation>Dimensioni Tabella</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="561"/>
        <source>How many rows (height)?</source>
        <translation>Quante righe (altezza)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="572"/>
        <source>How many columns (width)?</source>
        <translation>Quante colonne (larghezza)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="649"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>Scrivi o incolla un indirizzo web qui.
Puoi anche selezionare del testo, per convertirlo poi in un link.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="697"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>Scrivi o incolla l&apos;indirizzo dell&apos;immagine qui.
Il link deve puntare direttamente all&apos;immagine.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="134"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="138"/>
        <source>Text Formatting Options</source>
        <translation>Opzioni di formattazione del testo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="143"/>
        <source>Paste Text Without Formatting</source>
        <translation>Incolla testo senza formattazione</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="177"/>
        <source>Type a comment here</source>
        <translation>Scrivi un commento qui</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="648"/>
        <source>Insert a link</source>
        <translation>Inserisci un link</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="659"/>
        <source>Make a link from selected text</source>
        <translation>Crea un link con il testo selezionato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="660"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>Digita o incolla un indirizzo web qui.
Il testo selezionato (%1) sarà convertito in un link.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="696"/>
        <source>Insert an image from a URL</source>
        <translation>Inserisci un&apos;immagine da un URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="713"/>
        <source>Error: Invalid URL</source>
        <translation>Errore: URL non valido</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="714"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation>L&apos;indirizzo inserito (%1) non è valido. Gli indirizzi di un&apos;immagine dovrebbero iniziare con http:// o https://</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="805"/>
        <source>Cancel message?</source>
        <translation>Cancellare il messaggio?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="806"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation>Sei sicuro di voler cancellare questo messaggio?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="807"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;Si, cancellalo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="807"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="68"/>
        <source>minutes</source>
        <translation>minuti</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="82"/>
        <source>Top</source>
        <translation>In alto</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="84"/>
        <source>Bottom</source>
        <translation>In basso</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="86"/>
        <source>Left side</source>
        <translation>A sinistra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="88"/>
        <source>Right side</source>
        <translation>A destra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>Configurazione del programma</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="70"/>
        <source>Timeline &amp;update interval</source>
        <translation>Intervallo di &amp;aggiornamento della timeline</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="90"/>
        <source>&amp;Tabs position</source>
        <translation>Posizione delle &amp;tab</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="96"/>
        <source>&amp;Movable tabs</source>
        <translation>&amp;Tab mobili</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="136"/>
        <source>Minor Feeds</source>
        <translation>Feed Minori</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="210"/>
        <source>posts</source>
        <comment>Goes after a number, as: 25 posts</comment>
        <translation>elementi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="214"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>&amp;Elementi per pagina, timeline principale</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="219"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>elementi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="223"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>Elementi per pagina, &amp;altre timeline</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="245"/>
        <source>Show frame of deleted posts</source>
        <comment>UNDECIDED string; FIXME</comment>
        <translation>Mostra il box dei post cancellati</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="300"/>
        <source>Ignore SSL errors in images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="319"/>
        <source>Public posts as &amp;default</source>
        <translation>Messaggi pubblici come &amp;default</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="108"/>
        <source>Pro&amp;xy Settings</source>
        <translation>Impostazioni del Pro&amp;xy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="111"/>
        <source>Network configuration</source>
        <translation>Configurazione di Rete</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="116"/>
        <source>Set Up F&amp;ilters</source>
        <translation>Imposta F&amp;iltri</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="119"/>
        <source>Filtering rules</source>
        <translation>Regole di filtraggio</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="227"/>
        <source>Highlighted activities, except mine</source>
        <translation>Attività selezionate, escluse le mie</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="228"/>
        <source>Any highlighted activity</source>
        <translation>Qualsiasi attività selezionata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="229"/>
        <source>Always</source>
        <translation>Sempre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="230"/>
        <source>Never</source>
        <translation>Mai</translation>
    </message>
    <message>
        <source>Show snippets in minor feed</source>
        <translation type="obsolete">Mostra snippets nella timeline laterale</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="237"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>caratteri</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="240"/>
        <source>Snippet limit</source>
        <translation>Limite Snippet</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="130"/>
        <source>Post Titles</source>
        <translation>Titoli dei Post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="132"/>
        <source>Post Contents</source>
        <translation>Contenuto dei post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="134"/>
        <source>Comments</source>
        <translation>Commenti</translation>
    </message>
    <message>
        <source>Minor Feed</source>
        <translation type="obsolete">Feed laterale</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="156"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>Sei tra i destinatari dell&apos;attività, per esempio un commento indirizzato a te.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="160"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation>Usato anche quando i post in evidenza sono indirizzati a te nelle timeline.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="166"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>L&apos;attività è in risposta a qualcosa fatto da te, come un commento postato in risposta a una delle tue note.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="173"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation>Sei l&apos;oggetto dell&apos;attività, per esempio qualcuno ti ha aggiunto ad una lista.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="179"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>L&apos;attività è legata a uno dei tuoi oggetti, per esempio a qualcuno piace un tuo post.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="183"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>Usato anche per evidenziare i tuoi post nelle timeline.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="189"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>Oggetto evidenziato in base alle regole dei filtri.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="194"/>
        <source>Item is new.</source>
        <translation>Nuovo Elemento.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="232"/>
        <source>Show snippets in minor feeds</source>
        <translation>Mostra snippets nei feed minori</translation>
    </message>
    <message>
        <source>Show deleted posts</source>
        <translation type="obsolete">Mostra post eliminati</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Hide duplicated posts</source>
        <translation>Nascondi post duplicati</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="290"/>
        <source>Avatar size</source>
        <translation>Dimensione avatar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="292"/>
        <source>Show extended share information</source>
        <translation>Mostra informazioni di condivisione estese</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="294"/>
        <source>Show extra information</source>
        <translation>Mostra informazioni extra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="296"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>Evidenzia i commenti dell&apos;autore del post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="298"/>
        <source>Highlight your own comments</source>
        <translation>Evidenza i tuoi commenti</translation>
    </message>
    <message>
        <source>Use media filename as initial post title</source>
        <translation type="obsolete">Usa il nome del media come titolo iniziale del post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="323"/>
        <source>Show character counter</source>
        <translation>Mostra contatore di caratteri</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="336"/>
        <source>As system notifications</source>
        <translation>Come notifiche di sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="338"/>
        <source>Using own notifications</source>
        <translation>Usando proprie notifiche</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="340"/>
        <source>Don&apos;t show notifications</source>
        <translation>Non mostrare notifiche</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="343"/>
        <source>Notification Style</source>
        <translation>Stile di notifica</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="352"/>
        <source>Notify when receiving:</source>
        <translation>Notifica quando ricevi:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="357"/>
        <source>New posts</source>
        <translation>Nuovi post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="362"/>
        <source>Highlighted posts</source>
        <translation>Post in evidenza</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="367"/>
        <source>New activities in minor feed</source>
        <translation>Nuove attività nel feed secondario</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="372"/>
        <source>Highlighted activities in minor feed</source>
        <translation>Attività in evidenza nel feed secondario</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="393"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="394"/>
        <source>System iconset, if available</source>
        <translation>Icone di sistema, se disponibili</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="395"/>
        <source>Show your current avatar</source>
        <translation>Mostra il tuo avatar attuale</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="396"/>
        <source>Custom icon</source>
        <translation>Icona personalizzata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="399"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>&amp;Tipo di Icona di Sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="443"/>
        <source>General Options</source>
        <translation>Opzioni Generali</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="446"/>
        <source>Fonts</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="449"/>
        <source>Colors</source>
        <translation>Colori</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="452"/>
        <source>Timelines</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="455"/>
        <source>Posts</source>
        <translation>Post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="458"/>
        <source>Composer</source>
        <translation>Componi</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="461"/>
        <source>Notifications</source>
        <translation>Notifiche</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="464"/>
        <source>System Tray</source>
        <translation>Icone di Sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="402"/>
        <source>S&amp;elect...</source>
        <translation>S&amp;eleziona...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="416"/>
        <source>Custom &amp;Icon</source>
        <translation>&amp;Icona Personalizzata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="731"/>
        <source>Select custom icon</source>
        <translation>Seleziona un&apos;icona personalizzata</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="492"/>
        <source>Dianara stores data in this folder:</source>
        <translation>Dianara salva i dati in questa cartella:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="282"/>
        <source>Only for images inserted from web sites.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="285"/>
        <source>Use with care.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="321"/>
        <source>Use attachment filename as initial post title</source>
        <translation>Usa il nome del file dell&apos;allegato come titolo iniziale del post</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="502"/>
        <source>&amp;Save Configuration</source>
        <translation>&amp;Salva Configurazione</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="507"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="573"/>
        <source>This is a system notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="577"/>
        <source>System notifications are not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="579"/>
        <source>Own notifications will be used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="590"/>
        <source>This is a basic notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="733"/>
        <source>Image files</source>
        <translation>File immagine</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="735"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="749"/>
        <source>Invalid image</source>
        <translation>Immagine non valida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="750"/>
        <source>The selected image is not valid.</source>
        <translation>L&apos;immagine selezionata non è valida.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="71"/>
        <source>Hometown</source>
        <translation>Città</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="78"/>
        <source>Joined: %1</source>
        <translation>Membro dal: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="86"/>
        <source>Updated: %1</source>
        <translation>Aggiornato: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="105"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>Bio di %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="118"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>Questo utente non ha una biografia</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="122"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>Nessuna biografia per %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="142"/>
        <source>Open Profile in Web Browser</source>
        <translation>Apri il profilo nel browser web</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="149"/>
        <source>Send Message</source>
        <translation>Invia Messaggio</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="154"/>
        <source>In Lists...</source>
        <translation>Nelle liste...</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="169"/>
        <source>User Options</source>
        <translation>Opzioni utente</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="202"/>
        <source>Follow</source>
        <translation>Segui</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="214"/>
        <source>Stop Following</source>
        <translation>Smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="275"/>
        <source>Stop following?</source>
        <translation>Smettere di seguire?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="276"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>Sei sicuro di voler smettere di seguire %1?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="278"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Si, smetti di seguire</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="278"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="32"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>Scrivi parzialmente un nome o l&apos;ID per trovare un contatto...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="41"/>
        <source>F&amp;ull List</source>
        <translation>Lista Com&amp;pleta</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="42"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>nomeutente@pumpserver.org o https://pumpserver.org/nomeutente</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="45"/>
        <source>&amp;Enter address to follow:</source>
        <translation>&amp;Inserisci l&apos;indirizzo da seguire:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="65"/>
        <source>&amp;Follow</source>
        <translation>&amp;Segui</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="107"/>
        <source>Reload Followers</source>
        <translation>Ricarica Followers</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="112"/>
        <source>Reload Following</source>
        <translation>Ricarica Following</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="118"/>
        <source>Export Followers</source>
        <translation>Esporta Followers</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="123"/>
        <source>Export Following</source>
        <translation>Esporta Following</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="129"/>
        <source>Reload Lists</source>
        <translation>Ricarica Liste</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="179"/>
        <source>Follo&amp;wers</source>
        <translation>Follo&amp;wers</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="182"/>
        <source>Followin&amp;g</source>
        <translation>Foll&amp;owing</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="185"/>
        <source>&amp;Lists</source>
        <translation>&amp;Liste</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="200"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation>Esporta la lista dei &apos;Following&apos; in un file</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="201"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>Esporta la lista dei &apos;Followers&apos; in un file</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="46"/>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Save the attached file to your folders</source>
        <translation>Salva il file allegato nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="59"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="116"/>
        <source>Save File As...</source>
        <translation>Salva File Come...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="119"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="153"/>
        <source>Abort download?</source>
        <translation>Fermare download?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="154"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>Vuoi fermare il download del file allegato?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;Si, ferma</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;No, continue</source>
        <translation>&amp;No, continua</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="162"/>
        <source>Download aborted</source>
        <translation>Download fermato</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="176"/>
        <source>Download completed</source>
        <translation>Download completato</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="185"/>
        <source>Download failed</source>
        <translation>Download fallito</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="205"/>
        <source>Downloading %1 KiB...</source>
        <translation>Scaricando %1 KiB...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="207"/>
        <source>%1 KiB downloaded</source>
        <translation>%1 KiB scaricati</translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>Modifica Filtri</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="75"/>
        <source>Application</source>
        <translation>Applicazione</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="36"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1 se %2 contiene: %3</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="43"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation>Qui puoi impostare regole per nascondere o mettere in evidenza le attività. Puoi filtrare per contenuto, autore o applicazione.

Per esempio, puoi filtrare messaggi postati dall&apos;applicazione Open Farm Game, o quelli che contengono la parola NSFW nel messaggio. Puoi anche evidenziare i messaggi che contengono il tuo nome.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="60"/>
        <source>Hide</source>
        <translation>Nascondi</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="63"/>
        <source>Highlight</source>
        <translation>Evidenzia</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Post Contents</source>
        <translation>Contenuto del post</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Author ID</source>
        <translation>ID dell&apos;autore</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Activity Description</source>
        <translation>Descrizione dell&apos;attività</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Keywords...</source>
        <translation>Parole chiave...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="85"/>
        <source>&amp;Add Filter</source>
        <translation>&amp;Aggiungi Filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="93"/>
        <source>Filters in use</source>
        <translation>Filtri in uso</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="97"/>
        <source>&amp;Remove Selected Filter</source>
        <translation>&amp;Rimuovi i Filtri Selezionati</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="104"/>
        <source>&amp;Save Filters</source>
        <translation>&amp;Salva Filtri</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="110"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="124"/>
        <source>if</source>
        <translation>se</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="128"/>
        <source>contains</source>
        <translation>contiene</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="134"/>
        <source>&amp;New Filter</source>
        <translation>&amp;Nuovo Filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="141"/>
        <source>C&amp;urrent Filters</source>
        <translation>Filt&amp;ri Salvati</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>Aiuto di Base</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation>Per iniziare</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="70"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation>La prima volta che fai partire Dianara, dovresti vedere la finestra di Configurazione dell&apos;Account. Inserisci il tuo indirizzo Pump.io come nome@server e premi Ottieni Codice di Verifica.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="75"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation>Poi, il tuo browser predefinito dovrebbe caricare la pagina di autorizzazione nel tuo server Pump.io. Qui, dovrai copiare il VERIFIER code, e incollarlo nel secondo campo di Dianara. Quindi premi Autorizza Applicazione e, una volta confermato, premi Salva Dettagli.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="81"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>A questo punto il tuo profilo, le liste contatti e le timeline saranno caricate.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>Dovresti dare uno sguardo alla finestra di Configurazione del Programma, sotto Impostazioni - Configura Dianara. Ci sono diverse opzioni interessanti là.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="104"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>Puoi configurare diverse cose secondo le tue preferenze, come il tempo di aggiornamento delle timeline, quanti post per pagina vuoi vedere, colori di evidenziazione, notifiche o come visualizzare l&apos;icona di sistema.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Contenuti</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="88"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>Tieni presente che ci sono molti posti in Dianara dove puoi ottenere più informazioni fermandoti sopra alcuni testi o bottoni col mouse, e aspettare che il suggerimento appaia.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="109"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation>Qui puoi anche attivare l&apos;opzione di pubblicare sempre i tuoi post pubblici per default. Puoi sempre cambiare questa preferenza al momento di postare.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="122"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>La timeline principale, dove puoi vedere tutti gli elementi postati o condivisi da persone che segui.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="125"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>La timeline dei messaggi, dove vedrai messaggi inviati a te specificatamente. Questi messaggi potrebbero essere stati inviati anche ad altre persone.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="129"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>La timeli delle attività, dove vedrai i tuoi post e i post che hai condiviso.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="132"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>La timeline dei preferiti, dove vedrai i post e i commenti che ti piacciono. Può essere usata come sistema di bookmark.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="137"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <translation>La quinta timeline è la timeline minore, conosciuta anche come Meanwhile (nel frattempo). Questa è visibile a sinistra, ma può anche essere nascosta. Qui vedrai attività di tutte le persone che segui, come commenti, mi piace e segui persone.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="148"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>Queste attività potrebbero avere un &apos;+&apos; vicino. Premilo per aprire il post al quale sono riferite. Anche qui potrai avere ulteriori informazioni se rimani fermo col mouse sul pulsante.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>Postare</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="93"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>Se sei nuovo su Pump.io, dai uno sguardo a questa guida:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>Nuovi messaggi appaiono evidenziati in un colore diffeerente. Puoi segnarli come già letti cliccando in una parte vuota del messaggio.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation>Puoi postare note cliccando il campo di testo in alto oppure premendo Control+N. Impostare un titolo per il post è opzionale, ma caldamente consigliato, perchè aiuta gli altri a identificarlo nella timeline laterale, nelle notifiche mail, ecc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="170"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>Allegare immagini, video o audio ed altri file come PDF è ora possibile.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="173"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>Puoi usare il pulsante Formattazione per formattare il tuo testo, per esempio renderlo grassetto o corsivo. Alcune di queste opzioni richiedono che del testo sia selezionato prima che siano usate.</translation>
    </message>
    <message>
        <source>You can select who will see your post by using the To and CC buttons.</source>
        <translation type="obsolete">Puoi selezionare chi vedrà il tuo post usando i pulsanti A e CC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="180"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>Se aggiungi specifiche persone alla lista A, riceveranno una notifica del tuo messaggio nella tab dei messaggi diretti.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="191"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>Puoi creare messaggi privati aggiungendo specifiche persone a queste liste e rimuovendo Followers e Pubblico.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>Gestire i contatti</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="201"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>Puoi vedere le liste di persone che segui e che ti seguono dalla tab Contatti.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="204"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>Qui puoi anche gestire liste di persone, usate principalmente per inviare post a specifici gruppi.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="207"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>Puoi cliccare su qualsiasi avatar nei post, nei commenti, e nella timeline laterale e comparirà un menù con diverse opzioni, una di queste serve per seguire o smettere di seguire quella persona.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>Controlli da Tastiera</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="95"/>
        <source>Pump.io User Guide</source>
        <translation>Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="119"/>
        <source>There are seven timelines:</source>
        <translation>Ci sono sette timeline:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="143"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>La sesta e la settima timeline sono timeline minori o secondarie, simili a &quot;Nel Frattempo&quot;, ma contiene solo attività indirizzate direttamente a te (Menzioni) e attività create da te (Azioni).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="177"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>Puoi selezionare chi vedrà il tuo post usando i pulsanti A e Cc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>Puoi anche digitare &apos;@&apos; e il primo carattere del nome di un contatto per far apparire un popup con le scelte corrispondenti.</translation>
    </message>
    <message>
        <source>Choose one with the arrows and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation type="obsolete">Scegline uno con le frecce e premi Invio per completare il nome. Questo aggiungerà la persona alla lista dei destinatari.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="212"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>Puoi anche inviare un messaggio diretto (inizialmente privato) al contatto da questo menu.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="215"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>Puoi trovare una lista con alcuni utenti Pump.io ed altre informazioni qui:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="227"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>Le azioni più comuni che si trovano nei menù hanno scorciatorie da tastiera scritte vicino a loro, come F5 o Control+N.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="231"/>
        <source>Besides that, you can use:</source>
        <translation>Oltre a quello, puoi usare:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="234"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation>Control+Su/Giù/PgSu/PgGiù/Home/Fine per muoverti nelle timeline.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="237"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>Control+Sinistra/Destra per saltare da una pagina all&apos;altra nella timeline.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="240"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>Control+G va in qualsiasi pagina della timeline direttamente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="243"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>Control+1/2/3 per spostarti tra i feed minori.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="246"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>Control+Invio per inviare il post quando hai terminato di comporre la nota o il commento. Se la nota è vuota, puoi cancellarla premendo ESC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Opzioni da riga di comando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="187"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>Scegline uno con i tasti freccia e premi Invio per completare un nome. Questo aggiungerà quella persona alla lista di destinatari.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="250"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>Mentre componi una nota, premi Invio per saltare dal titolo al messaggio. Premendo la freccia Su mentre sei all&apos;inizio del messaggio, torni al titolo.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="255"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>Control+Invio per terminare la creazione della lista di destinatari per il post, per le liste &apos;A&apos; o &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="265"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>Puoi usare il parametro --config per avviare il programma con una configurazione differente. Questo può essere utile per usare due o più account. Puoi anche avviare due istanze di Dianara contemporaneamente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="270"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Usa il parametro --debug per avere informazioni extra nel tuo terminale, su quello che il programma sta facendo.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="284"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="56"/>
        <source>Image</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="64"/>
        <source>Resolution and file size</source>
        <translation>Risoluzione e dimensione del file</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="67"/>
        <source>ESC to close, secondary-click for options</source>
        <translation>ESC per chiudere, click destro per opzioni</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="72"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Salva come...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="87"/>
        <source>&amp;Restart Animation</source>
        <translation>&amp;Replay Animazione</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="97"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="146"/>
        <source>Save Image...</source>
        <translation>Salvataggio Immagine...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="153"/>
        <source>Close Viewer</source>
        <translation>Chiudi Visualizzatore</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="182"/>
        <source>Save Image As...</source>
        <translation>Salva Immagine Come...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="184"/>
        <source>Image files</source>
        <translation>Files Immagine</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="185"/>
        <source>All files</source>
        <translation>Tutti i Files</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="196"/>
        <source>Error saving image</source>
        <translation>Errore durante il salvataggio dell&apos;immagine</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="197"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>C&apos;è stato un problema salvando %1.

L&apos;estensione del file dovrebbe essere .jpg o .png.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Members</source>
        <translation>Membri</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="51"/>
        <source>Add Mem&amp;ber</source>
        <translation>Aggiungi Mem&amp;bro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="58"/>
        <source>&amp;Remove Member</source>
        <translation>&amp;Rimuovi Membro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="66"/>
        <source>&amp;Delete Selected List</source>
        <translation>&amp;Cancella la Lista Selezionata</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="73"/>
        <source>Add New &amp;List</source>
        <translation>Aggiungi Nuova &amp;Lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="90"/>
        <source>Create L&amp;ist</source>
        <translation>Crea L&amp;ista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="97"/>
        <source>&amp;Add to List</source>
        <translation>&amp;Aggiungi alla Lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="304"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation>Sei sicuro di voler cancellare %1?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="436"/>
        <source>Remove person from list?</source>
        <translation>Rimuovere la persona dalla lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="437"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>Sei sicuro di voler rimuovere %1 dalla lista %2?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="442"/>
        <source>&amp;Yes</source>
        <translation>&amp;Si</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="76"/>
        <source>Type a name for the new list...</source>
        <translation>Digita un nome per la nuova lista...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="84"/>
        <source>Type an optional description here</source>
        <translation>Digita una descrizione opzionale qui</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="303"/>
        <source>WARNING: Delete list?</source>
        <translation>ATTENZIONE: Cancellare la lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="306"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, cancellala</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="306"/>
        <location filename="../src/listsmanager.cpp" line="442"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>Pulisci &amp;Log</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="60"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Meanwhile...</source>
        <translation type="obsolete">Nel frattempo...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="961"/>
        <source>Side &amp;Panel</source>
        <translation>Pannello &amp;laterale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="981"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;Barra di stato</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2141"/>
        <source>Total posts: %1</source>
        <translation>Post totali: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>&amp;Timeline</source>
        <translation>T&amp;imeline</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2147"/>
        <source>The main timeline</source>
        <translation>Timeline principale</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2160"/>
        <source>&amp;Activity</source>
        <translation>&amp;Attività</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2161"/>
        <source>Your own posts</source>
        <translation>I tuoi messaggi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>Your favorited posts</source>
        <translation>I tuoi messaggi preferiti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2153"/>
        <source>&amp;Messages</source>
        <translation>&amp;Messaggi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2154"/>
        <source>Messages sent explicitly to you</source>
        <translation>Messaggi inviati esplicitamente a te</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>&amp;Contacts</source>
        <translation>&amp;Contatti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="662"/>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Initializing...</source>
        <translation>Inizializzando...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="697"/>
        <source>Your account is not configured yet.</source>
        <translation>Il tuo account non è ancora stato configurato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="685"/>
        <source>Dianara started.</source>
        <translation>Dianara è partito.</translation>
    </message>
    <message>
        <source>Actions</source>
        <translation type="obsolete">Azioni</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="393"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>Le persone che segui, quelle che ti seguono, e le tue liste di persone</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="686"/>
        <source>Running with Qt v%1.</source>
        <translation>In funzione con Qt v%1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="815"/>
        <source>&amp;Session</source>
        <translation>&amp;Sessione</translation>
    </message>
    <message>
        <source>Update Minor &amp;Feed</source>
        <translation type="obsolete">Aggiorna la timeline late&amp;rale</translation>
    </message>
    <message>
        <source>Update Mentions</source>
        <translation type="obsolete">Aggiorna Menzioni</translation>
    </message>
    <message>
        <source>Update Actions</source>
        <translation type="obsolete">Aggiorna Azioni</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="912"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>Auto-aggiorna &amp;Timeline</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Mark All as Read</source>
        <translation>Segna tutti come letti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="935"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;Pubblica una nota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="949"/>
        <source>&amp;Quit</source>
        <translation>&amp;E&amp;sci</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="958"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizzazione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="993"/>
        <source>Full &amp;Screen</source>
        <translation>Sc&amp;hermo Intero</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>&amp;Log</source>
        <translation>&amp;Log</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1016"/>
        <source>S&amp;ettings</source>
        <translation>Configura&amp;zione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1020"/>
        <source>Edit &amp;Profile</source>
        <translation>Modifica &amp;profilo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>&amp;Account</source>
        <translation>A&amp;ccount</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Basic &amp;Help</source>
        <translation>Ai&amp;uto di Base</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1077"/>
        <source>Report a &amp;Bug</source>
        <translation>Riporta un &amp;Bug</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>&amp;Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1099"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>Elenco di alcuni &amp;Utenti Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Auto-updating enabled</source>
        <translation>Auto-aggiornamenti abilitati</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1223"/>
        <source>Auto-updating disabled</source>
        <translation>Autp-aggiornamenti disabilitati</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1660"/>
        <source>Proxy password required</source>
        <translation>Password Proxy Richiesta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1661"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>Hai configurato un server proxy con autenticazione, ma la password non è impostata.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1666"/>
        <source>Enter the password for your proxy server:</source>
        <translation>Inserisci la password del tuo server proxy:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1739"/>
        <source>Your biography is empty</source>
        <translation>La tua biografia è vuota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1774"/>
        <source>Click to edit your profile</source>
        <translation>Clicca per modificare il tuo profilo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1799"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>Attiva l&apos;aggiornamento automatico delle timeline, ogni %1 minuti.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1806"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>Ferma l&apos;aggiornamento automatico delle timeline.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1937"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2095"/>
        <source>Last update: %1</source>
        <translation>Ultimo aggiornamento: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1944"/>
        <location filename="../src/mainwindow.cpp" line="2307"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>&apos;%1&apos; aggiornato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1968"/>
        <source>1 pending to receive.</source>
        <comment>singular, one post</comment>
        <translation>1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1973"/>
        <source>%1 pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation>%1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1988"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 evidenziato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1993"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 evidenziati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2299"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2937"/>
        <source>System tray icon is not available.</source>
        <translation>L&apos;icona di sistema non è disponibile.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2939"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>Dianara non può essere nascosto nella barra delle notifiche di sistema.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2942"/>
        <source>Do you want to close the program completely?</source>
        <translation>Vuoi chiudere completamente il programma?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1950"/>
        <source>Timeline updated at %1.</source>
        <translation>Timeline aggiornata alle %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="647"/>
        <source>Press F1 for help</source>
        <translation>Premi F1 per aiuto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="818"/>
        <source>Update %1</source>
        <translation>Aggiorna %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2060"/>
        <source>No new posts.</source>
        <translation>Nessun nuovo post.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2223"/>
        <source>Your Pump.io account is not configured</source>
        <translation>Il tuo account Pump.io non è configurato</translation>
    </message>
    <message>
        <source>Mentions</source>
        <translation type="obsolete">Menzioni</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2318"/>
        <source>There is 1 new activity.</source>
        <translation>C&apos;è una nuova attività.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2322"/>
        <source>There are %1 new activities.</source>
        <translation>Ci sono %1 nuove attività.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2334"/>
        <source>1 pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation>1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2339"/>
        <source>%1 pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation>%1 in attesa di ricezione.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2356"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation>1 evidenziato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2361"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation>%1 evidenziati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2406"/>
        <source>No new activities.</source>
        <translation>Nessuna nuova attività.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2500"/>
        <source>Link to: %1</source>
        <translation>Link a: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2792"/>
        <source>Dianara is licensed under the GNU GPL license, and uses some Oxygen icons: http://www.oxygen-icons.org/ (LGPL licensed)</source>
        <translation>Dianara è distribuita su licenza GNU GPL, ed utilizza alcune icone Oxygen: http://www.oxygen-icons.org/ (licenza LGPL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2858"/>
        <location filename="../src/mainwindow.cpp" line="2936"/>
        <source>Quit?</source>
        <translation>Chiudere?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2859"/>
        <source>You are composing a note or a comment.</source>
        <translation>Stai componendo una nota o un commento.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2861"/>
        <source>Do you really want to close Dianara?</source>
        <translation>Vuoi veramente chiudere Dianara?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2862"/>
        <location filename="../src/mainwindow.cpp" line="2944"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;Si, chiudi il programma</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2862"/>
        <location filename="../src/mainwindow.cpp" line="2944"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>&amp;Configure Dianara</source>
        <translation>Configura &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation>Attività minori fatte da chiunque, come rispondere ai post</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Minor activities addressed to you</source>
        <translation>Attività minori indirizzate a te</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="185"/>
        <source>Minor activities done by you</source>
        <translation>Attività minori create da te</translation>
    </message>
    <message>
        <source>&amp;Update Timeline</source>
        <translation type="obsolete">Aggiorna Timeli&amp;ne</translation>
    </message>
    <message>
        <source>Update &amp;Messages</source>
        <translation type="obsolete">Aggiorna &amp;Messaggi</translation>
    </message>
    <message>
        <source>Update &amp;Activity</source>
        <translation type="obsolete">Aggiorna &amp;Attività</translation>
    </message>
    <message>
        <source>Update Fa&amp;vorites</source>
        <translation type="obsolete">Aggiorna &amp;Preferiti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="972"/>
        <source>&amp;Toolbar</source>
        <translation>Barra degli Strumen&amp;ti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>&amp;Filters and Highlighting</source>
        <translation>&amp;Filtri e Evidenziazione</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>&amp;Help</source>
        <translation>Ai&amp;uto</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Visit &amp;Website</source>
        <translation>Visita il sito &amp;web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1093"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>Alcuni consigli per &amp;Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>About &amp;Dianara</source>
        <translation>Informazioni su &amp;Dianara </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Toolbar</source>
        <translation>Barra degli Strumenti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Open the log viewer</source>
        <translation>Apri il visualizzatore del log</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2005"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 filtrato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2010"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 filtrati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2023"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 cancellato.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2028"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 cancellati.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2167"/>
        <source>Favor&amp;ites</source>
        <translation>Prefer&amp;iti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2312"/>
        <source>Minor feed updated at %1.</source>
        <translation>Timeline laterale aggiornata %1.</translation>
    </message>
    <message>
        <source>Minor feed updated.</source>
        <translation type="obsolete">Timeline laterale aggiornata.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2769"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>Con Dianara puoi vedere le tue timeline, creare nuovi post, caricare immagini e altri media, interagire con post, organizzare i tuoi contatti e seguire nuove persone.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2778"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traduzione italiana a cura di Howcanuhavemyusername (or Metal Biker) (howcanuhavemyusername@microca.st).</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2816"/>
        <source>&amp;Hide Window</source>
        <translation>&amp;Nascondi Finestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2826"/>
        <source>&amp;Show Window</source>
        <translation>&amp;Mostra la finestra</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1955"/>
        <source>There is 1 new post.</source>
        <translation>C&apos;è 1 nuovo messaggio.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1959"/>
        <source>There are %1 new posts.</source>
        <translation>Ci sono %1 nuovi messaggi.</translation>
    </message>
    <message>
        <source>Timeline updated.</source>
        <translation type="obsolete">Timeline aggiornata.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2757"/>
        <source>About Dianara</source>
        <translation>Informazioni su Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2766"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>Dianara è un client per il social network pump.io.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2785"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>Grazie a tutti i tester, i traduttori e i manutentori dei pacchetti, che hanno aiutato Dianara a migliorare!</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="64"/>
        <source>Older Activities</source>
        <translation>Attività più vecchie</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="67"/>
        <source>Get previous minor activities</source>
        <translation>Ottieni attività precedenti</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="94"/>
        <source>There are no activities to show yet.</source>
        <translation>Non ci sono ancora attività da mostrare.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="468"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation>Scarica %1 nuove</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="67"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>Via %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="81"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>A: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="88"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>Cc: %1</translation>
    </message>
    <message>
        <source>CC: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation type="obsolete">CC: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="257"/>
        <source>Open referenced post</source>
        <translation>Apri l&apos;elemento relativo</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="238"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>Salta alla pagina</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="33"/>
        <source>Page number:</source>
        <translation>Pagina numero:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="45"/>
        <source>&amp;Go</source>
        <translation>&amp;Vai</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="51"/>
        <source>Go to &amp;last page</source>
        <translation>Vai all&apos;u&amp;ltima pagina</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="58"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>&amp;Cerca:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>Inserisci qui un nome per cercarlo</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="95"/>
        <source>Add a contact to a list</source>
        <translation>Aggiungi un contatto ad una lista</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="1671"/>
        <source>Like</source>
        <translation>Mi piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1670"/>
        <source>Like this post</source>
        <translation>Dì che ti piace questo messaggio</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>Clicca pe rscaricare l&apos;allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="130"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>Pubblica</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="237"/>
        <location filename="../src/post.cpp" line="889"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>Via %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="322"/>
        <location filename="../src/post.cpp" line="505"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="882"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="896"/>
        <source>Modified on %1</source>
        <translation>Modificato il %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="484"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>Padre</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="492"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>Apri il post padre, quello al quale questo post risponde</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="654"/>
        <source>Modify this post</source>
        <translation>Modifica questo post</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="728"/>
        <source>Join Group</source>
        <translation>Unisciti al Gruppo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="733"/>
        <source>%1 members in the group</source>
        <translation>%1 membri nel gruppo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1047"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>L&apos;immagine è animata. Clicca per riprodurre.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1071"/>
        <source>Loading image...</source>
        <translation>Caricando l&apos;immagine...</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1110"/>
        <source>Attached file</source>
        <translation>File allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1304"/>
        <source>1 like</source>
        <translation>1 mi piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1373"/>
        <source>1 comment</source>
        <translation>1 commento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1457"/>
        <source>Shared %1 times</source>
        <translation>Condiviso %1 volte</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="233"/>
        <source>Shared on %1</source>
        <translation>Condiviso su %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1585"/>
        <source>Edited: %1</source>
        <translation>Modificato: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="937"/>
        <source>In</source>
        <translation>In</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="628"/>
        <source>Share</source>
        <translation>Condividi</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="652"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1309"/>
        <source>%1 likes</source>
        <translation>%1 mi piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1378"/>
        <source>%1 comments</source>
        <translation>%1 commenti</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="663"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="187"/>
        <source>Via %1</source>
        <translation>Via %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="880"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>Pubblicato il %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="196"/>
        <location filename="../src/post.cpp" line="400"/>
        <source>To</source>
        <translation>A</translation>
    </message>
    <message>
        <source>CC</source>
        <translation type="obsolete">CC</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="609"/>
        <source>If you select some text, it will be quoted.</source>
        <translation>Se selezioni del testo, verrà quotato.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="636"/>
        <source>Unshare</source>
        <translation>Rimuovi condivisione</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="638"/>
        <source>Unshare this post</source>
        <translation>Rimuovi la condivisione di questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="282"/>
        <source>Open post in web browser</source>
        <translation>Apri il messaggio nel browser web</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="202"/>
        <location filename="../src/post.cpp" line="416"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="290"/>
        <source>Copy post link to clipboard</source>
        <translation>Copia il link al messaggio negli appunti</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="309"/>
        <source>Normalize text colors</source>
        <translation>Normalizza i colori del testo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="605"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>Commenta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="607"/>
        <source>Reply to this post.</source>
        <translation>Rispondi a questo post.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>Share this post with your contacts</source>
        <translation>Condividi questo post con i tuoi contatti</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="665"/>
        <source>Erase this post</source>
        <translation>Cancella questo post</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1066"/>
        <source>Couldn&apos;t load image!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1090"/>
        <source>Attached Audio</source>
        <translation>Audio allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1100"/>
        <source>Attached Video</source>
        <translation>Video allegato</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1278"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>A %1 piace questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1283"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation>A %1 piace questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1418"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 ha condiviso questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1423"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 hanno condiviso questo elemento</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1452"/>
        <source>Shared once</source>
        <translation>Condiviso una volta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1663"/>
        <source>You like this</source>
        <translation>Ti piace</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1664"/>
        <source>Unlike</source>
        <translation>Non mi piace più</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1777"/>
        <source>Share post?</source>
        <translation>Condividi il messaggio?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1778"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>Vuoi condividere il messaggio di %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1780"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;Si, condividilo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1780"/>
        <location filename="../src/post.cpp" line="1799"/>
        <location filename="../src/post.cpp" line="1843"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1796"/>
        <source>Unshare post?</source>
        <translation>Rimuovere la condivisione di questo elemento?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1797"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>Vuoi rimuovere la condivisione dell&apos;elemento di %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1799"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;Si, non condividerlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1840"/>
        <source>WARNING: Delete post?</source>
        <translation>ATTENZIONE: Eliminare il messaggio?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1841"/>
        <source>Are you sure you want to delete this post?</source>
        <translation>Sei sicuro di voler eliminare questo messaggio?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1843"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Si, eliminalo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>Clicca l&apos;immagine per vederla nelle dimensioni originali</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>Editor del profilo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="43"/>
        <source>This is your Pump address</source>
        <translation>Questo è il tuo indirizzo Pump.io</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="49"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>Questa è l&apos;indirizzo e-mail associato al tuo accout, per inviare notifiche e recuperare la password</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="60"/>
        <source>Change &amp;Avatar...</source>
        <translation>Cambia &amp;avatar...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="68"/>
        <source>This is your visible name</source>
        <translation>Questo è il tuo nome visibile</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="81"/>
        <source>&amp;Save Profile</source>
        <translation>&amp;Salva profilo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="86"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancella</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="106"/>
        <source>Webfinger ID</source>
        <translation>Webfinger ID</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="107"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>Avatar</source>
        <translation>Avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="111"/>
        <source>Full &amp;Name</source>
        <translation>&amp;Nome Completo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="112"/>
        <source>&amp;Hometown</source>
        <translation>Ci&amp;ttà</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="113"/>
        <source>&amp;Bio</source>
        <translation>&amp;Bio</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="155"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>Non impostata</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="212"/>
        <source>Select avatar image</source>
        <translation>Scegli l&apos;immagine per l&apos;avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="214"/>
        <source>Image files</source>
        <translation>Files immagine</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="216"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="238"/>
        <source>Invalid image</source>
        <translation>Immagine non valida</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="239"/>
        <source>The selected image is not valid.</source>
        <translation>L&apos;immagine selezionata non è valida.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>Configurazione Proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>Non usare un proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>Il tuo username del proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>Nota: la password non è salvata in modo sicuro. Se lo desideri, puoi lasciare questo campo vuoto e ti verrà chiesta la password all&apos;avvio.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="74"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="80"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="95"/>
        <source>Proxy &amp;Type</source>
        <translation>&amp;Tipo di Proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="97"/>
        <source>&amp;Hostname</source>
        <translation>&amp;Hostname</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="99"/>
        <source>&amp;Port</source>
        <translation>&amp;Porta</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="101"/>
        <source>Use &amp;Authentication</source>
        <translation>Usa Autenti&amp;cazione</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="103"/>
        <source>&amp;User</source>
        <translation>&amp;Utente</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="105"/>
        <source>Pass&amp;word</source>
        <translation>Pass&amp;word</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="251"/>
        <source>Picture</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="257"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="263"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="285"/>
        <source>Ad&amp;d...</source>
        <translation>Aggiun&amp;gi...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="287"/>
        <source>Upload media, like pictures or videos</source>
        <translation>Carica media, come immagini o video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="800"/>
        <source>Select Picture...</source>
        <translation>Selezionare l&apos;immagine...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="802"/>
        <source>Find the picture in your folders</source>
        <translation>Trova l&apos;immagine nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="150"/>
        <location filename="../src/publisher.cpp" line="183"/>
        <location filename="../src/publisher.cpp" line="430"/>
        <location filename="../src/publisher.cpp" line="447"/>
        <source>Public</source>
        <translation>Pubblico</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="155"/>
        <location filename="../src/publisher.cpp" line="188"/>
        <location filename="../src/publisher.cpp" line="434"/>
        <location filename="../src/publisher.cpp" line="451"/>
        <source>Followers</source>
        <translation>Followers</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="160"/>
        <location filename="../src/publisher.cpp" line="193"/>
        <source>Lists</source>
        <translation>Liste</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="175"/>
        <source>To...</source>
        <translation>A...</translation>
    </message>
    <message>
        <source>CC...</source>
        <translation type="obsolete">CC...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="210"/>
        <source>Select who will get a copy of this post</source>
        <translation>Scegli chi riceverà una copia di questo messaggio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="269"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="319"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="321"/>
        <source>Cancel the post</source>
        <translation>Cancella il messaggio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="804"/>
        <source>Picture not set</source>
        <translation>Immagine non selezionata</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="823"/>
        <source>Select Audio File...</source>
        <translation>Seleziona File Audio...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="825"/>
        <source>Find the audio file in your folders</source>
        <translation>Cerca il file audio nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="827"/>
        <source>Audio file not set</source>
        <translation>Il file audio non è impostato</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="847"/>
        <source>Select Video...</source>
        <translation>Seleziona Video...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="849"/>
        <source>Find the video in your folders</source>
        <translation>Cerca il file video nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="851"/>
        <source>Video not set</source>
        <translation>Video non impostato</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="870"/>
        <source>Select File...</source>
        <translation>Seleziona File...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="872"/>
        <source>Find the file in your folders</source>
        <translation>Trova il file nelle tue cartelle</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="874"/>
        <source>File not set</source>
        <translation>File non impostato</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="911"/>
        <location filename="../src/publisher.cpp" line="954"/>
        <source>Error: Already composing</source>
        <translation>Errore: stai già scrivendo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="912"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>Non puoi modificare il messaggio ora, perchè stai già scrivendo un messaggio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="928"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="941"/>
        <source>Editing post</source>
        <translation>Modifica il messaggio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="955"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>Non puoi creare un messaggio per %1 al momento, perche un post è già in fase di composizione.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1021"/>
        <source>Posting failed.

Try again.</source>
        <translation>Invio fallito.

Prova di nuovo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1310"/>
        <source>Warning: You have no followers yet</source>
        <translation>Avviso: non hai ancora followers</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1311"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>Stai cercando di postare solo per i tuoi followers, ma non ne hai ancora.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1315"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>Se posti con queste impostazioni, nessuno sarà in grado di leggere il tuo messaggio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1318"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>Vuoi rendere il post pubblico invece che renderlo visibile solo ai tuoi followers?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1321"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;Si, rendilo pubblico</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1323"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;Annulla e torna al post</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1373"/>
        <source>Updating...</source>
        <translation>Aggiornando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1384"/>
        <source>Post is empty.</source>
        <translation>Il messaggio è vuoto.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1388"/>
        <source>File not selected.</source>
        <translation>File non selezionato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1409"/>
        <source>Select one image</source>
        <translation>Scegli un&apos;immagine</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1410"/>
        <source>Image files</source>
        <translation>Files immagine</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1439"/>
        <source>Select one file</source>
        <translation>Seleziona un file</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1442"/>
        <source>Invalid file</source>
        <translation>File non valido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1443"/>
        <source>The file type cannot be detected.</source>
        <translation>Il tipo di file non può essere identificato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1450"/>
        <source>All files</source>
        <translation>Tutti i files</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1465"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>Visto che stai caricando un&apos;immagine, potresti ridurre la sua risoluzione o salvarla in un formato più compresso, come JPG.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1470"/>
        <source>File is too big</source>
        <translation>File troppo grande</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1471"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>Dianara limita la dimensione dei file caricati a 10MiB per post, in modo da prevenire problemi di spazio e rete dei server.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1476"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>Questa è una misura temporanea, in quanto i server non possono ancora impostare i loro limiti.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1481"/>
        <source>Sorry for the inconvenience.</source>
        <translation>Ci scusiamo per il disagio.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1501"/>
        <source>Resolution</source>
        <translation>Risoluzione</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1512"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1514"/>
        <source>Size</source>
        <translation>Dimensioni</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1554"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 KiB di %2 KiB caricati</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1413"/>
        <source>Invalid image</source>
        <translation>Immagine non valida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="64"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>Impostare un titolo rende il feed laterale più informativo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="78"/>
        <source>Add a brief title for the post (recommended)</source>
        <translation>Aggiungi un breve titolo per il post (raccomandato)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="105"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="107"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>Rimuovi l&apos;allegato e torna al post regolare</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="208"/>
        <source>Cc...</source>
        <translation>Cc...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="311"/>
        <location filename="../src/publisher.cpp" line="712"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>Pubblica</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1322"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;No, rendilo visibile solo ai miei follower</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1414"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>Il formato dell&apos;immagine non è stato riconosciuto. L&apos;estensione può essere sbagliata, come un&apos;immagine GIF rinominata in immagine.jpg o altro.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1420"/>
        <source>Select one audio file</source>
        <translation>Seleziona un file audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1421"/>
        <source>Audio files</source>
        <translation>Files audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1424"/>
        <source>Invalid audio file</source>
        <translation>File audio non valido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1425"/>
        <source>The audio format cannot be detected.</source>
        <translation>Il formato del file audio non può essere rilevato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1429"/>
        <source>Select one video file</source>
        <translation>Seleziona un file video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1430"/>
        <source>Video files</source>
        <translation>Files Video</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1434"/>
        <source>Invalid video file</source>
        <translation>File video non valido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1435"/>
        <source>The video format cannot be detected.</source>
        <translation>Il formato del file video non può essere rilevato.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1343"/>
        <source>Posting...</source>
        <translation>Pubblicando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="170"/>
        <location filename="../src/publisher.cpp" line="203"/>
        <source>People...</source>
        <translation>Persone...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="177"/>
        <source>Select who will see this post</source>
        <translation>Scegli chi potrà vedere il messaggio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="313"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>Premi Control+Invio per pubblicare con la tastiera</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="567"/>
        <source>Creating person list...</source>
        <translation>Creando la lista delle persone...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="579"/>
        <source>Deleting person list...</source>
        <translation>Eliminando la lista delle persone...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="594"/>
        <source>Getting a person list...</source>
        <translation>Ottenendo la lista della persona...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="632"/>
        <source>Adding person to list...</source>
        <translation>Aggiungendo una persona alla lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="668"/>
        <source>Removing person from list...</source>
        <translation>Rimuovendo una persona dalla lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="716"/>
        <source>Creating group...</source>
        <translation>Creando un gruppo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="746"/>
        <source>Joining group...</source>
        <translation>Unendosi al gruppo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="771"/>
        <source>Leaving group...</source>
        <translation>Lasciando il gruppo...</translation>
    </message>
    <message>
        <source>Main timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Richiesto un aggiornamento della timeline principale, ma gli aggiornamenti sono bloccati.</translation>
    </message>
    <message>
        <source>Direct timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Richiesto un aggiornamento dei messaggi diretti, ma gli aggiornamenti sono bloccati.</translation>
    </message>
    <message>
        <source>Getting direct messages timeline...</source>
        <translation type="obsolete">Ricevendo la timeline dei messaggi diretti...</translation>
    </message>
    <message>
        <source>Activity timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Richiesto un aggiornamento della timeline delle attività, ma gli aggiornamenti sono bloccati.</translation>
    </message>
    <message>
        <source>Favorites timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Richiesto un aggiornamento dei preferiti, ma gli aggiornamenti sono bloccati.</translation>
    </message>
    <message>
        <source>Getting favorites timeline...</source>
        <translation type="obsolete">Ricevendo la timeline dei preferiti...</translation>
    </message>
    <message>
        <source>Timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Richiesto un aggiornamento della Timeline, ma gli aggiornamenti son bloccati.</translation>
    </message>
    <message>
        <source>Getting timeline...</source>
        <translation type="obsolete">Ottenendo la timeline...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="786"/>
        <source>Getting likes...</source>
        <translation>Ricevendo i &quot;mi piace&quot;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="818"/>
        <source>Getting comments...</source>
        <translation>Ricevendo i commenti...</translation>
    </message>
    <message>
        <source>Getting minor feed...</source>
        <translation type="obsolete">Ricevendo la timeline laterale...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="870"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Ottenendo &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="914"/>
        <source>Timeline</source>
        <translation>Timeline</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="917"/>
        <source>Messages</source>
        <translation>Messaggi</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1044"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>Caricando %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1225"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>Errore HTTP</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1237"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>Gateway Timeout</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1247"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>Servizio non Disponibile</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1265"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation>Non implementato</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1275"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>Errore Interno del Server</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1295"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>Sparito</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1305"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>Non Trovato</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1315"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>Proibito</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1325"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>Non Autorizzato</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1336"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>Richiesta Errata</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1355"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>Spostato Temporaneamente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1365"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>Spostato Permanentemente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1374"/>
        <source>Error connecting to %1</source>
        <translation>Errore durante la connessione a %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1383"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>Codice di errore HTTP non gestito: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1491"/>
        <source>Profile received.</source>
        <translation>Profilo ricevuto.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1493"/>
        <source>Followers</source>
        <translation>Followers</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1496"/>
        <source>Following</source>
        <translation>Following</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1510"/>
        <source>Profile updated.</source>
        <translation>Profilo aggiornato.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1521"/>
        <source>Post published successfully.</source>
        <translation>Messaggio pubblicato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1560"/>
        <source>Avatar published successfully.</source>
        <translation>Avatar pubblicato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1589"/>
        <source>Post updated successfully.</source>
        <translation>Messaggio aggiornato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1595"/>
        <source>Comment updated successfully.</source>
        <translation>Commento aggiornato con successo.</translation>
    </message>
    <message>
        <source>Comment posted successfully.</source>
        <translation type="obsolete">Commento pubblicato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1671"/>
        <source>1 comment received.</source>
        <translation>1 commento ricevuto.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1676"/>
        <source>%1 comments received.</source>
        <translation>%1 commenti ricevuti.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1703"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>Post di %1 condiviso con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1732"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Ricevuto &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1745"/>
        <source>Adding items...</source>
        <translation>Aggiungendo oggetti...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2244"/>
        <source>SSL errors in connection to %1!</source>
        <translation>Errori SSL durante la connessione a %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>OAuth error while authorizing application.</source>
        <translation>Errore OAuth in fase di autorizzazione dell&apos;appliazione.</translation>
    </message>
    <message>
        <source>Minor feed received.</source>
        <translation type="obsolete">Timeline laterale ricevuta.</translation>
    </message>
    <message>
        <source>Main timeline</source>
        <translation type="obsolete">Timeline principale</translation>
    </message>
    <message>
        <source>Direct messages</source>
        <translation type="obsolete">Messaggi diretti</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="920"/>
        <source>Activity</source>
        <translation>Attività</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="923"/>
        <source>Favorites</source>
        <translation>Preferiti</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="927"/>
        <source>Meanwhile</source>
        <translation>Nel frattempo</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="930"/>
        <source>Mentions</source>
        <translation>Menzioni</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="933"/>
        <source>Actions</source>
        <translation>Azioni</translation>
    </message>
    <message>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is a feed&apos;s name</comment>
        <translation type="obsolete">Ottenendo &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1903"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>Lista dei &apos;following&apos; ricevuta completamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1911"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>Lista dei &apos;following&apos; ricevuta parzialmente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1929"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>Lista dei &apos;followers&apos; ricevuta completamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1936"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>Lista dei &apos;followers&apos; ricevuta parzialmente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2005"/>
        <source>Person list deleted successfully.</source>
        <translation>Lista di persone eliminata correttamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2025"/>
        <source>Person list received.</source>
        <translation>Lista di persone ricevuta.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2154"/>
        <source>File downloaded successfully.</source>
        <translation>File scaricato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2191"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>File caricato con successo. Pubblicando il messaggio...</translation>
    </message>
    <message>
        <source>Timeline received. Updating post list...</source>
        <translation type="obsolete">Timeline ricevuta. Aggiornando la lista dei messaggi...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="225"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>Autorizzato ad utilizzare l&apos;account %1. Ricevendo i dati iniziali.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="230"/>
        <source>There is no authorized account.</source>
        <translation>Non ci sono account autorizzati.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="466"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation>Ricevendo la lista dei &apos;Following&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="478"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>Ricevendo la lista dei &apos;Followers&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="531"/>
        <source>Getting list of person lists...</source>
        <translation>Rocevendo le liste delle persone...</translation>
    </message>
    <message>
        <source>Getting main timeline...</source>
        <translation type="obsolete">Ricevendo la timeline principale...</translation>
    </message>
    <message>
        <source>Getting activity timeline...</source>
        <translation type="obsolete">Ricevendo la timeline delle attività...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="813"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1606"/>
        <source>Message liked or unliked successfully.</source>
        <translation>Messaggio aggiunto o rimosso dai &quot;Mi piace&quot; correttamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1620"/>
        <source>Likes received.</source>
        <translation>&quot;Mi piace&quot; ricevuto.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1646"/>
        <source>Comment &apos;%1&apos; posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1834"/>
        <source>Message deleted successfully.</source>
        <translation>Messaggio cancellato correttamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1850"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Segui %1 (%2) con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1860"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Hai smesso di seguire %1 (%2) con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1973"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation>Lista delle &apos;Liste&apos; ricevuta.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1994"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>Lista di persone &apos;%1&apos; creata con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2058"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) aggiunto alla lista con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2078"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>%1 (%2) rimosso dalla lista con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2089"/>
        <source>Group %1 created successfully.</source>
        <translation>Gruppo %1 creato con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2100"/>
        <source>Group %1 joined successfully.</source>
        <translation>Unito al gruppo %1 con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2113"/>
        <source>Left the %1 group successfully.</source>
        <translation>Lasciato il gruppo %1 con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2200"/>
        <source>Avatar uploaded.</source>
        <translation>Avatar caricato.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2256"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2285"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>L&apos;applicazione non è ancora autorizzata con il tuo server. Registrazione...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2322"/>
        <source>Getting OAuth token...</source>
        <translation>Ricevendo il token OAuth...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2343"/>
        <source>OAuth support error</source>
        <translation>Errore supporto OAuth</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2344"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>La tua installazione di QOAuth, libreria usata da Dianara, sembra che non abbia il supporto HMAC-SHA1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2348"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>Probabilmente hai bisogno di installare il plugin OpenSSL per QCA: %1, %2 o simili.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2405"/>
        <source>Authorization error</source>
        <translation>Errore di Autorizzazione</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2406"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>C&apos;è stato un errore OAuth nel tentativo di ottenere il token di autorizzazione.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2409"/>
        <source>QOAuth error %1</source>
        <translation>Errore QOAuth %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2441"/>
        <source>Application authorized successfully.</source>
        <translation>Applicazione autorizzata con successo.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2478"/>
        <source>Waiting for proxy password...</source>
        <translation>In attesa della password del proxy...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2507"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>Aspettando ancora il tuo profilo. Provo ancora...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2616"/>
        <source>Some initial data was not received. Restarting initialization.</source>
        <translation>Alcuni dati iniziali non sono stati ricevuti. Riavvio inizializzazione.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2624"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>Alcuni dati iniziali non sono stati ricevuti anche dopo diversi tentativi. Qualcosa potrebbe non funzionare sul tuo server. Potresti essere comunque in grado di usare il servizio normalmente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2634"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>Tutti i dati iniziali ricevuti. Inizializzazione completa.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2645"/>
        <source>Ready.</source>
        <translation>Pronto.</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="66"/>
        <source>Welcome to Dianara</source>
        <translation>Benvenuto in Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="68"/>
        <source>Dianara is a &lt;b&gt;pump.io&lt;/b&gt; client.</source>
        <translation>Dianara è un client &lt;b&gt;pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="77"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>Premi &lt;b&gt;F1&lt;/b&gt; se vuoi aprire la finestra di Aiuto.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="80"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>Prima di tutto, configura il tuo account nel menù &lt;b&gt;Configurazione - Account&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="83"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>Completato il processo, il tuo profilo e le timelines dovrebbero aggiornarsi automaticamente.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="87"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>Prenditi un momento per dare uno sguardo ai menù ed alla finestra di Configurazione.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="91"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>Puoi anche impostare i dettagli e la foto del tuo profilo nel menu &lt;b&gt;Configurazione - Modifica profilo&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="102"/>
        <source>Dianara&apos;s blog</source>
        <translation>Blog di Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="179"/>
        <source>Newest</source>
        <translation>Ultimi</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="199"/>
        <source>Newer</source>
        <translation>Più recenti</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="203"/>
        <source>Older</source>
        <translation>Più vecchi</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="480"/>
        <source>Page %1 of %2.</source>
        <translation>Pagina %1 di %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="484"/>
        <source>Showing %1 posts per page.</source>
        <translation>Mostra %1 posts per pagina.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="487"/>
        <source>%1 posts in total.</source>
        <translation>%1 post in totale.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="491"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>Clicca qui o premi Control+G per saltare ad una pagina specifica</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="572"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>&apos;%1&apos; non può essere aggiornato perchè un commento è in fase di composizione.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="661"/>
        <source>Get %1 pending posts</source>
        <translation>Ottieni %1 post in attesa</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="71"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>se non hai ancora un account Pump, puoi averne uno al seguente indirizzo, per esempio:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="95"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>Ci sono tooltip ovunque, quindi se ti fermi col mouse sopra ad un bottone o ad un testo, probabilmente vedrai informazioni extra.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="104"/>
        <source>Pump.io User Guide</source>
        <translation>Guida Utente Pump.io</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="114"/>
        <source>Direct Messages Timeline</source>
        <translation>Timeline dei messaggi diretti</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="115"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>Qui potrai vedere messaggi indirizzati specificatamente a te.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="124"/>
        <source>Activity Timeline</source>
        <translation>Timeline delle Attività</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="125"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>Qui vedrai i tuoi messaggi.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="133"/>
        <source>Favorites Timeline</source>
        <translation>Timeline dei Preferiti</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="134"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>Messaggi e commenti che ti sono piaciuti.</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>Timestamp non Valida!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>Un minuto fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>%1 minuti fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>Un&apos;ora fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>%1 ore fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>In questo istante</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>Prossimamente</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>Ieri</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>%1 giorni fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>Un mese fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>%1 mesi fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>Un anno fa</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>%1 anni fa</translation>
    </message>
</context>
</TS>
