<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>ASActivity</name>
    <message>
        <location filename="../src/asactivity.cpp" line="88"/>
        <location filename="../src/asactivity.cpp" line="121"/>
        <source>Public</source>
        <translation>Público</translation>
    </message>
    <message>
        <location filename="../src/asactivity.cpp" line="389"/>
        <source>%1 by %2</source>
        <comment>1=kind of object: note, comment, etc; 2=author&apos;s name</comment>
        <translation>%1 de %2</translation>
    </message>
</context>
<context>
    <name>ASObject</name>
    <message>
        <location filename="../src/asobject.cpp" line="231"/>
        <source>Note</source>
        <comment>Noun, an object type</comment>
        <translation>Nota</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="236"/>
        <source>Article</source>
        <comment>Noun, an object type</comment>
        <translation>Artículo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="241"/>
        <source>Image</source>
        <comment>Noun, an object type</comment>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="246"/>
        <source>Audio</source>
        <comment>Noun, an object type</comment>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="251"/>
        <source>Video</source>
        <comment>Noun, an object type</comment>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="256"/>
        <source>File</source>
        <comment>Noun, an object type</comment>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="261"/>
        <source>Comment</source>
        <comment>Noun, as in object type: a comment</comment>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="266"/>
        <source>Group</source>
        <comment>Noun, an object type</comment>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="271"/>
        <source>Collection</source>
        <comment>Noun, an object type</comment>
        <translation>Colección</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="276"/>
        <source>Other</source>
        <comment>As in: other type of post</comment>
        <translation>Otro</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="332"/>
        <source>No detailed location</source>
        <translation>No hay ubicación detallada</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="357"/>
        <source>Deleted on %1</source>
        <translation>Eliminado el %1</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="513"/>
        <location filename="../src/asobject.cpp" line="583"/>
        <source>and one other</source>
        <translation>y uno más</translation>
    </message>
    <message>
        <location filename="../src/asobject.cpp" line="517"/>
        <location filename="../src/asobject.cpp" line="587"/>
        <source>and %1 others</source>
        <translation>y %1 más</translation>
    </message>
</context>
<context>
    <name>ASPerson</name>
    <message>
        <location filename="../src/asperson.cpp" line="138"/>
        <source>Hometown</source>
        <translation>Ciudad</translation>
    </message>
</context>
<context>
    <name>AccountDialog</name>
    <message>
        <location filename="../src/accountdialog.cpp" line="74"/>
        <source>Your Pump.io address:</source>
        <translation>Tu dirección pump.io:</translation>
    </message>
    <message>
        <source>Webfinger ID, like username@pumpserver.org</source>
        <translation type="obsolete">ID Webfinger, tipo usuario@servidorpump.org</translation>
    </message>
    <message>
        <source>Your address, as username@server</source>
        <translation type="obsolete">Tu dirección, como usuario@servidor</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="81"/>
        <source>Get &amp;Verifier Code</source>
        <translation>Obtener código de &amp;verificación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="107"/>
        <source>Verifier code:</source>
        <translation>Código de verificación:</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="109"/>
        <source>Enter or paste the verifier code provided by your Pump server here</source>
        <translation>Introduce o pega aquí el codigo de verificación proporcionado por tu servidor Pump</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="143"/>
        <source>&amp;Save Details</source>
        <translation>&amp;Guardar datos</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="357"/>
        <source>If the browser doesn&apos;t open automatically, copy this address manually</source>
        <translation>Si el navegador web no se abre automáticamente, copia esta dirección manualmente</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="25"/>
        <source>Account Configuration</source>
        <translation>Configuración de la cuenta</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="42"/>
        <source>First, enter your Webfinger ID, your pump.io address.</source>
        <translation>Primero, introduce tu identificador Webfinger, tu dirección pump.io.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="44"/>
        <source>Your address looks like username@pumpserver.org, and you can find it in your profile, in the web interface.</source>
        <translation>Tu dirección es como usuario@servidorpump.org, y puedes encontrarla en tu perfil, en la interfaz web.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="48"/>
        <source>If your profile is at https://pump.example/yourname, then your address is yourname@pump.example</source>
        <translation>Si tu perfil está en https://pump.ejemplo/tunombre, entonces tu dirección es tunombre@pump.ejemplo</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="52"/>
        <source>If you don&apos;t have an account yet, you can sign up for one at %1. This link will take you to a random public server.</source>
        <comment>1=link to website</comment>
        <translation>Si aún no tienes una cuenta, puedes registrar una en %1. Este enlace te llevará a un servidor público aleatorio.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="60"/>
        <source>If you need help: %1</source>
        <translation>Si necesitas ayuda: %1</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="63"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="76"/>
        <source>Your address, like username@pumpserver.org</source>
        <translation>Tu dirección, como usuario@servidorpump.org</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="83"/>
        <source>After clicking this button, a web browser will open, requesting authorization for Dianara</source>
        <translation>Cuando pulses este botón, se abrirá un navegador web, solicitando autorización para Dianara</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="90"/>
        <source>Once you have authorized Dianara from your Pump server web interface, you&apos;ll receive a code called VERIFIER.
Copy it and paste it into the field below.</source>
        <comment>Don&apos;t translate the VERIFIER word!</comment>
        <translation>Una vez que hayas autorizado a Dianara desde la interfaz web de tu servidor Pump, recibirás un código llamado VERIFIER.
Cópialo y pégalo en el campo de abajo.</translation>
    </message>
    <message>
        <source>Enter the verifier code provided by your Pump server here</source>
        <translation type="obsolete">Introduce aquí el codigo de verificación proporcionado por tu servidor Pump</translation>
    </message>
    <message>
        <source>Paste the verifier here</source>
        <translation type="obsolete">Pega el verificador aquí</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="116"/>
        <source>&amp;Authorize Application</source>
        <translation>&amp;Autorizar la aplicación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="151"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="159"/>
        <source>Your account is properly configured.</source>
        <translation>Tu cuenta está configurada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="163"/>
        <source>Press Unlock if you wish to configure a different account.</source>
        <translation>Pulsa Desbloquear si quieres configurar una cuenta diferente.</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="171"/>
        <source>&amp;Unlock</source>
        <translation>&amp;Desbloquear</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="298"/>
        <source>A web browser will start now, where you can get the verifier code</source>
        <translation>Ahora se iniciará un navegador web, donde podrás obtener el código de verificación</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="307"/>
        <source>Your Pump address is invalid</source>
        <translation>Tu dirección Pump no es válida</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="326"/>
        <source>Verifier code is empty</source>
        <translation>El código de verificación está vacío</translation>
    </message>
    <message>
        <location filename="../src/accountdialog.cpp" line="338"/>
        <source>Dianara is authorized to access your data</source>
        <translation>Dianara tiene autorización para acceder a tu cuenta</translation>
    </message>
</context>
<context>
    <name>AudienceSelector</name>
    <message>
        <location filename="../src/audienceselector.cpp" line="32"/>
        <source>&apos;To&apos; List</source>
        <translation>Lista &apos;Para&apos;</translation>
    </message>
    <message>
        <source>&apos;CC&apos; List</source>
        <translation type="obsolete">Lista &apos;CC&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="36"/>
        <source>&apos;Cc&apos; List</source>
        <translation>Lista &apos;Cc&apos;</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="57"/>
        <source>&amp;Add to Selected</source>
        <translation>&amp;Añadir a seleccionados</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="70"/>
        <source>All Contacts</source>
        <translation>Todos los contactos</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="75"/>
        <source>Select people from the list on the left.
You can drag them with the mouse, click or double-click on them, or select them and use the button below.</source>
        <translation>Selecciona gente de la lista de la izquierda.
Puedes arrastrarlos con el ratón, hacer clic o doble clic en ellos, o seleccionarlos y usar el botón de abajo.</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="89"/>
        <source>Clear &amp;List</source>
        <translation>Borrar &amp;lista</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="95"/>
        <source>&amp;Done</source>
        <translation>&amp;Hecho</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="100"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/audienceselector.cpp" line="119"/>
        <source>Selected People</source>
        <translation>Gente seleccionada</translation>
    </message>
</context>
<context>
    <name>AvatarButton</name>
    <message>
        <location filename="../src/avatarbutton.cpp" line="138"/>
        <source>Open %1&apos;s profile in web browser</source>
        <translation>Abrir el perfil de %1 en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="142"/>
        <source>Open your profile in web browser</source>
        <translation>Abrir tu perfil en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="161"/>
        <source>Send message to %1</source>
        <translation>Enviar mensaje a %1</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="212"/>
        <source>Stop following</source>
        <translation>Dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="223"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="264"/>
        <source>Stop following?</source>
        <translation>¿Dejar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="265"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>¿Estás seguro de que quieres dejar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="267"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/avatarbutton.cpp" line="267"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../src/colorpicker.cpp" line="34"/>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../src/comment.cpp" line="249"/>
        <source>Posted on %1</source>
        <translation>Publicado el %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="254"/>
        <source>Modified on %1</source>
        <translation>Modificado el %1</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="112"/>
        <source>Like or unlike this comment</source>
        <translation>Decir que te gusta o que ya no te gusta este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="119"/>
        <source>Quote</source>
        <comment>This is a verb, infinitive</comment>
        <translation>Citar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="126"/>
        <source>Reply quoting this comment</source>
        <translation>Responder citando este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="134"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="140"/>
        <source>Modify this comment</source>
        <translation>Modificar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="146"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="152"/>
        <source>Erase this comment</source>
        <translation>Borrar este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="293"/>
        <source>Unlike</source>
        <translation>Ya no me gusta</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="299"/>
        <source>Like</source>
        <translation>Me gusta</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="321"/>
        <source>%1 like this comment</source>
        <comment>Plural: %1=list of people like John, Jane, Smith</comment>
        <translation>A %1 les gusta este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="315"/>
        <source>%1 likes this comment</source>
        <comment>Singular: %1=name of just 1 person</comment>
        <translation>A %1 le gusta este comentario</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="506"/>
        <source>WARNING: Delete comment?</source>
        <translation>ADVERTENCIA: ¿Eliminar comentario?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="507"/>
        <source>Are you sure you want to delete this comment?</source>
        <translation>¿Estás seguro de que quieres eliminar este comentario?</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="508"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminarlo</translation>
    </message>
    <message>
        <location filename="../src/comment.cpp" line="508"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>CommenterBlock</name>
    <message>
        <location filename="../src/commenterblock.cpp" line="120"/>
        <source>You can press Control+Enter to send the comment with the keyboard</source>
        <translation>Puedes pulsar Control+Enter para enviar el comentario con el teclado</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="37"/>
        <source>Reload comments</source>
        <translation>Actualizar comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="117"/>
        <location filename="../src/commenterblock.cpp" line="416"/>
        <source>Comment</source>
        <comment>Infinitive verb</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="127"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="129"/>
        <source>Press ESC to cancel the comment if there is no text</source>
        <translation>Pulsa ESC para cancelar el comentario si no hay texto</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="192"/>
        <source>Show all %1 comments</source>
        <translation>Mostrar los %1 comentarios</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="451"/>
        <source>Error: Already composing</source>
        <translation>Error: Ya se está redactando</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="452"/>
        <source>You can&apos;t edit a comment at this time, because another comment is already being composed.</source>
        <translation>No puedes editar un comentario en este momento, porque ya se está redactando otro comentario.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="465"/>
        <source>Editing comment</source>
        <translation>Editando comentario</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="520"/>
        <source>Posting comment failed.

Try again.</source>
        <translation>Ha fallado la publicación del comentario.

Prueba de nuevo.</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="550"/>
        <source>Sending comment...</source>
        <translation>Enviando comentario...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="555"/>
        <source>Updating comment...</source>
        <translation>Actualizando comentario...</translation>
    </message>
    <message>
        <location filename="../src/commenterblock.cpp" line="564"/>
        <source>Comment is empty.</source>
        <translation>El comentario está vacío.</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../src/composer.cpp" line="66"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="71"/>
        <source>Italic</source>
        <translation>Cursiva</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="113"/>
        <source>Make a link</source>
        <translation>Hacer un enlace</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="35"/>
        <source>Click here or press Control+N to post a note...</source>
        <translation>Haz clic aquí o pulsa Control+N para publicar una nota...</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="170"/>
        <source>Type a message here to post it</source>
        <translation>Escribe un mensaje aquí para publicarlo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="62"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="42"/>
        <source>Symbols</source>
        <translation>Símbolos</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="60"/>
        <source>Formatting</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="76"/>
        <source>Underline</source>
        <translation>Subrayado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="81"/>
        <source>Strikethrough</source>
        <translation>Tachado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="88"/>
        <source>Header</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="93"/>
        <source>List</source>
        <translation>Lista</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="97"/>
        <source>Table</source>
        <translation>Tabla</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="101"/>
        <source>Preformatted block</source>
        <translation>Bloque preformateado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="105"/>
        <source>Quote block</source>
        <translation>Bloque de cita</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="118"/>
        <source>Insert an image from a web site</source>
        <translation>Insertar una imagen desde un sitio web</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="123"/>
        <source>Insert line</source>
        <translation>Insertar línea</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="413"/>
        <source>Insert as image?</source>
        <translation>¿Insertar como imagen?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="414"/>
        <source>The link you are pasting seems to point to an image.</source>
        <translation>Parece que el enlace que estás pegando apunta a una imagen.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="416"/>
        <source>Insert as visible image</source>
        <translation>Insertar como imagen visible</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="417"/>
        <source>Insert as link</source>
        <translation>Insertar como enlace</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="557"/>
        <source>Table Size</source>
        <translation>Tamaño de la tabla</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="561"/>
        <source>How many rows (height)?</source>
        <translation>¿Cuántas filas (altura)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="572"/>
        <source>How many columns (width)?</source>
        <translation>¿Cuántas columnas (ancho)?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="649"/>
        <source>Type or paste a web address here.
You could also select some text first, to turn it into a link.</source>
        <translation>Escribe o pega una dirección web aquí.
También puedes seleccionar texto antes, para convertirlo en un enlace.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="697"/>
        <source>Type or paste the image address here.
The link must point to the image file directly.</source>
        <translation>Escribe o pega la dirección de la imagen aquí.
El enlace ha de apuntar al archivo de imagen directamente.</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="138"/>
        <source>Text Formatting Options</source>
        <translation>Opciones de formato de texto</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="143"/>
        <source>Paste Text Without Formatting</source>
        <translation>Pegar texto sin formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="696"/>
        <source>Insert an image from a URL</source>
        <translation>Insertar una imagen desde una URL</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="713"/>
        <source>Error: Invalid URL</source>
        <translation>Error: URL no válida</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="714"/>
        <source>The address you entered (%1) is not valid.
Image addresses should begin with http:// or https://</source>
        <translation>La dirección que has introducido (%1) no es válida.
Las direcciones de imagenes han de empezar por http:// o https://</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="805"/>
        <source>Cancel message?</source>
        <translation>¿Cancelar el mensaje?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="806"/>
        <source>Are you sure you want to cancel this message?</source>
        <translation>¿Seguro que quieres cancelar este mensaje?</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="807"/>
        <source>&amp;Yes, cancel it</source>
        <translation>&amp;Sí, cancelarlo</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="807"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="648"/>
        <source>Insert a link</source>
        <translation>Insertar un enlace</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="134"/>
        <source>&amp;Format</source>
        <comment>Button for text formatting and related options</comment>
        <translation>&amp;Formato</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="177"/>
        <source>Type a comment here</source>
        <translation>Escribe un comentario aquí</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="659"/>
        <source>Make a link from selected text</source>
        <translation>Hacer un link con el texto seleccionado</translation>
    </message>
    <message>
        <location filename="../src/composer.cpp" line="660"/>
        <source>Type or paste a web address here.
The selected text (%1) will be converted to a link.</source>
        <translation>Escribe o pega una dirección web aquí.
El texto seleccionado (%1) será convertido en un enlace.</translation>
    </message>
</context>
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../src/configdialog.cpp" line="68"/>
        <source>minutes</source>
        <translation>minutos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="82"/>
        <source>Top</source>
        <translation>Parte superior</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="84"/>
        <source>Bottom</source>
        <translation>Parte inferior</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="86"/>
        <source>Left side</source>
        <translation>Lado izquierdo</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="88"/>
        <source>Right side</source>
        <translation>Lado derecho</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="32"/>
        <source>Program Configuration</source>
        <translation>Configuración del programa</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="70"/>
        <source>Timeline &amp;update interval</source>
        <translation>Intervalo de &amp;actualización de la línea temporal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="90"/>
        <source>&amp;Tabs position</source>
        <translation>Posición de las &amp;pestañas</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="96"/>
        <source>&amp;Movable tabs</source>
        <translation>Pes&amp;tañas movibles</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="136"/>
        <source>Minor Feeds</source>
        <translation>Líneas temporales menores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="210"/>
        <source>posts</source>
        <comment>Goes after a number, as: 25 posts</comment>
        <translation>mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="214"/>
        <source>&amp;Posts per page, main timeline</source>
        <translation>&amp;Mensajes por página, línea temporal principal</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="219"/>
        <source>posts</source>
        <comment>This goes after a number, like: 10 posts</comment>
        <translation>mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="223"/>
        <source>Posts per page, &amp;other timelines</source>
        <translation>Mensajes por página, &amp;otras líneas temporales</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="245"/>
        <source>Show frame of deleted posts</source>
        <comment>UNDECIDED string; FIXME</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="319"/>
        <source>Public posts as &amp;default</source>
        <translation>Mensajes públicos de manera &amp;predefinida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="108"/>
        <source>Pro&amp;xy Settings</source>
        <translation>Ajustes de pro&amp;xy</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="111"/>
        <source>Network configuration</source>
        <translation>Configuración de red</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="116"/>
        <source>Set Up F&amp;ilters</source>
        <translation>Configurar f&amp;iltros</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="119"/>
        <source>Filtering rules</source>
        <translation>Normas de filtrado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="227"/>
        <source>Highlighted activities, except mine</source>
        <translation>Actividades destacadas, excepto las mías</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="228"/>
        <source>Any highlighted activity</source>
        <translation>Cualquier actividad destacada</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="229"/>
        <source>Always</source>
        <translation>Siempre</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="230"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <source>Show snippets in minor feed</source>
        <translation type="obsolete">Mostrar fragmentos en la línea temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="237"/>
        <source>characters</source>
        <comment>This is a suffix, after a number</comment>
        <translation>caracteres</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="240"/>
        <source>Snippet limit</source>
        <translation>Límite de los fragmentos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="130"/>
        <source>Post Titles</source>
        <translation>Títulos de los mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="132"/>
        <source>Post Contents</source>
        <translation>Contenido de los mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="134"/>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
    <message>
        <source>Minor Feed</source>
        <translation type="obsolete">Línea temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="156"/>
        <source>You are among the recipients of the activity, such as a comment addressed to you.</source>
        <translation>Estás entre los destinatarios de la actividad, como por ejemplo un comentario dirigido a ti.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="160"/>
        <source>Used also when highlighting posts addressed to you in the timelines.</source>
        <translation>Usado también cuando se destacan mensajes dirigidos a ti en las líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="166"/>
        <source>The activity is in reply to something done by you, such as a comment posted in reply to one of your notes.</source>
        <translation>La actividad es en respuesta a alguna cosa hecha por ti, como un comentario publicado en respuesta a una de tus notas.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="173"/>
        <source>You are the object of the activity, such as someone adding you to a list.</source>
        <translation>Eres el objeto de la actividad, como por ejemplo cuando alguien te añade a una lista.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="179"/>
        <source>The activity is related to one of your objects, such as someone liking one of your posts.</source>
        <translation>La actividad está relacionada con uno de tus objetos, como por ejemplo cuando a alguien le gusta uno de tus mensajes.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="183"/>
        <source>Used also when highlighting your own posts in the timelines.</source>
        <translation>Usado también cuando se destacan tus propios mensajes en las líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="189"/>
        <source>Item highlighted due to filtering rules.</source>
        <translation>Elemento destacado por normas de filtrado.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="194"/>
        <source>Item is new.</source>
        <translation>El elemento es nuevo.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="232"/>
        <source>Show snippets in minor feeds</source>
        <translation>Mostrar fragmentos en las líneas temporales menores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="251"/>
        <source>Hide duplicated posts</source>
        <translation>Ocultar mensajes duplicados</translation>
    </message>
    <message>
        <source>Only for embedded images</source>
        <translation type="obsolete">Solo para imágenes incrustadas...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="290"/>
        <source>Avatar size</source>
        <translation>Tamaño de los avatares</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="292"/>
        <source>Show extended share information</source>
        <translation>Mostrar información adicional en los compartidos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="294"/>
        <source>Show extra information</source>
        <translation>Mostrar información extra</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="296"/>
        <source>Highlight post author&apos;s comments</source>
        <translation>Destacar comentarios del autor del mensaje</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="298"/>
        <source>Highlight your own comments</source>
        <translation>Destacar tus propios comentarios</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="300"/>
        <source>Ignore SSL errors in images</source>
        <translation>Ignorar errores de SSL en las imágenes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="323"/>
        <source>Show character counter</source>
        <translation>Mostrar contador de caracteres</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="336"/>
        <source>As system notifications</source>
        <translation>Como notificaciones del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="338"/>
        <source>Using own notifications</source>
        <translation>Usando notificaciones propias</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="340"/>
        <source>Don&apos;t show notifications</source>
        <translation>No mostrar notificaciones</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="343"/>
        <source>Notification Style</source>
        <translation>Estilo de las notificaciones</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="352"/>
        <source>Notify when receiving:</source>
        <translation>Notificar cuando se reciban:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="357"/>
        <source>New posts</source>
        <translation>Mensajes nuevos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="362"/>
        <source>Highlighted posts</source>
        <translation>Mensajes destacados</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="367"/>
        <source>New activities in minor feed</source>
        <translation>Nuevas actividades en la línea temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="372"/>
        <source>Highlighted activities in minor feed</source>
        <translation>Actividades destacadas en la línea temporal menor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="393"/>
        <source>Default</source>
        <translatorcomment>el icono</translatorcomment>
        <translation>Predeterminado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="394"/>
        <source>System iconset, if available</source>
        <translation>Icono del sistema, si está disponible</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="395"/>
        <source>Show your current avatar</source>
        <translation>Mostrar tu avatar actual</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="396"/>
        <source>Custom icon</source>
        <translation>Icono personalizado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="399"/>
        <source>System Tray Icon &amp;Type</source>
        <translation>&amp;Tipo de icono en la bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="443"/>
        <source>General Options</source>
        <translation>Opciones generales</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="446"/>
        <source>Fonts</source>
        <translation>Fuentes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="449"/>
        <source>Colors</source>
        <translation>Colores</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="452"/>
        <source>Timelines</source>
        <translation>Líneas temporales</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="455"/>
        <source>Posts</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="458"/>
        <source>Composer</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="461"/>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="464"/>
        <source>System Tray</source>
        <translation>Bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="402"/>
        <source>S&amp;elect...</source>
        <translation>S&amp;eleccionar...</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="416"/>
        <source>Custom &amp;Icon</source>
        <translation>&amp;Icono personalizado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="731"/>
        <source>Select custom icon</source>
        <translation>Selecciona icono personalizado</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="492"/>
        <source>Dianara stores data in this folder:</source>
        <translation>Dianara guarda datos en esta carpeta:</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="282"/>
        <source>Only for images inserted from web sites.</source>
        <translation>Solo para imágenes insertadas desde sitios web.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="285"/>
        <source>Use with care.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="321"/>
        <source>Use attachment filename as initial post title</source>
        <translation>Usar nombre de archivo del adjunto como título inicial del mensaje</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="502"/>
        <source>&amp;Save Configuration</source>
        <translation>&amp;Guardar configuración</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="507"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="573"/>
        <source>This is a system notification</source>
        <translation>Esto es una notificación del sistema</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="577"/>
        <source>System notifications are not available!</source>
        <translation>¡Las notificaciones del sistema no están disponibles!</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="579"/>
        <source>Own notifications will be used.</source>
        <translation>Se usarán las notificaciones propias.</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="590"/>
        <source>This is a basic notification</source>
        <translation>Esto es una notificación básica</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="733"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="735"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="749"/>
        <source>Invalid image</source>
        <translation>Imagen no válida</translation>
    </message>
    <message>
        <location filename="../src/configdialog.cpp" line="750"/>
        <source>The selected image is not valid.</source>
        <translation>La imagen seleccionada no es válida.</translation>
    </message>
</context>
<context>
    <name>ContactCard</name>
    <message>
        <location filename="../src/contactcard.cpp" line="71"/>
        <source>Hometown</source>
        <translation>Ciudad</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="78"/>
        <source>Joined: %1</source>
        <translation>Se unió: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="86"/>
        <source>Updated: %1</source>
        <translation>Actualizado: %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="105"/>
        <source>Bio for %1</source>
        <comment>Abbreviation for Biography, but you can use the full word; %1=contact name</comment>
        <translation>Bio de %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="118"/>
        <source>This user doesn&apos;t have a biography</source>
        <translation>Este usuario no tiene una biografía</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="122"/>
        <source>No biography for %1</source>
        <comment>%1=contact name</comment>
        <translation>No hay biografía para %1</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="142"/>
        <source>Open Profile in Web Browser</source>
        <translation>Abrir el perfil en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="149"/>
        <source>Send Message</source>
        <translation>Enviar mensaje</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="154"/>
        <source>In Lists...</source>
        <translation>En listas...</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="169"/>
        <source>User Options</source>
        <translation>Opciones de usuario</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="202"/>
        <source>Follow</source>
        <translation>Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="214"/>
        <source>Stop Following</source>
        <translation>Dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="275"/>
        <source>Stop following?</source>
        <translation>¿Dejar de seguir?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="276"/>
        <source>Are you sure you want to stop following %1?</source>
        <translation>¿Estás seguro de que quieres dejar de seguir a %1?</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="278"/>
        <source>&amp;Yes, stop following</source>
        <translation>&amp;Sí, dejar de seguir</translation>
    </message>
    <message>
        <location filename="../src/contactcard.cpp" line="278"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <location filename="../src/contactlist.cpp" line="32"/>
        <source>Type a partial name or ID to find a contact...</source>
        <translation>Escribe una parte de un nombre o ID para encontrar un contacto...</translation>
    </message>
    <message>
        <location filename="../src/contactlist.cpp" line="41"/>
        <source>F&amp;ull List</source>
        <translation>Lista c&amp;ompleta</translation>
    </message>
</context>
<context>
    <name>ContactManager</name>
    <message>
        <location filename="../src/contactmanager.cpp" line="42"/>
        <source>username@server.org or https://server.org/username</source>
        <translation>usuario@servidor.org o https://servidor.org/usuario</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="45"/>
        <source>&amp;Enter address to follow:</source>
        <translation>&amp;Introduce dirección para seguir:</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="65"/>
        <source>&amp;Follow</source>
        <translation>&amp;Seguir</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="107"/>
        <source>Reload Followers</source>
        <translation>Actualizar Seguidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="112"/>
        <source>Reload Following</source>
        <translation>Actualizar Siguiendo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="118"/>
        <source>Export Followers</source>
        <translation>Exportar Seguidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="123"/>
        <source>Export Following</source>
        <translation>Exportar Siguiendo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="129"/>
        <source>Reload Lists</source>
        <translation>Actualizar listas</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="179"/>
        <source>Follo&amp;wers</source>
        <translation>Se&amp;guidores</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="182"/>
        <source>Followin&amp;g</source>
        <translation>&amp;Siguiendo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="185"/>
        <source>&amp;Lists</source>
        <translation>&amp;Listas</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="200"/>
        <source>Export list of &apos;following&apos; to a file</source>
        <translation>Exportar lista de &apos;siguiendo&apos; a un archivo</translation>
    </message>
    <message>
        <location filename="../src/contactmanager.cpp" line="201"/>
        <source>Export list of &apos;followers&apos; to a file</source>
        <translation>Exportar lista de &apos;seguidores&apos; a un archivo</translation>
    </message>
</context>
<context>
    <name>DownloadWidget</name>
    <message>
        <location filename="../src/downloadwidget.cpp" line="46"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="47"/>
        <source>Save the attached file to your folders</source>
        <translation>Guardar el archivo adjunto en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="59"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="116"/>
        <source>Save File As...</source>
        <translation>Guardar archivo como...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="119"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="153"/>
        <source>Abort download?</source>
        <translation>¿Interrumpir la descarga?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="154"/>
        <source>Do you want to stop downloading the attached file?</source>
        <translation>¿Quieres detener la descarga del archivo adjunto?</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;Yes, stop</source>
        <translation>&amp;Sí, detener</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="155"/>
        <source>&amp;No, continue</source>
        <translation>&amp;No, continuar</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="162"/>
        <source>Download aborted</source>
        <translation>Descarga interrumpida</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="176"/>
        <source>Download completed</source>
        <translation>Descarga completada</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="185"/>
        <source>Download failed</source>
        <translation>Ha fallado la descarga</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="205"/>
        <source>Downloading %1 KiB...</source>
        <translation>Descargando %1 KiB...</translation>
    </message>
    <message>
        <location filename="../src/downloadwidget.cpp" line="207"/>
        <source>%1 KiB downloaded</source>
        <translatorcomment>hmmm FIXME</translatorcomment>
        <translation>%1 KiB descargados</translation>
    </message>
</context>
<context>
    <name>FilterEditor</name>
    <message>
        <location filename="../src/filtereditor.cpp" line="28"/>
        <source>Filter Editor</source>
        <translation>Editor de filtros</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="75"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="36"/>
        <source>%1 if %2 contains: %3</source>
        <comment>This explains a filter rule, like: Hide if Author ID contains JohnDoe</comment>
        <translation>%1 si %2 contiene: %3</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="43"/>
        <source>Here you can set some rules for hiding or highlighting stuff. You can filter by content, author or application.

For instance, you can filter out messages posted by the application Open Farm Game, or which contain the word NSFW in the message. You could also highlight messages that contain your name.</source>
        <translation>Aquí puedes establecer algunas normas para ocultar o destacar cosas. Puedes filtrar por contenido, autor o aplicación.

Por ejemplo, puedes filtrar mensajes publicados por la aplicación Open Farm Game, o que contienen la palabra NSFW en el mensaje. También podrías destacar mensajes que contienen tu nombre.</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="60"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="63"/>
        <source>Highlight</source>
        <translation>Destacar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="69"/>
        <source>Post Contents</source>
        <translation>Contenido de la publicación</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="72"/>
        <source>Author ID</source>
        <translation>ID del autor</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="78"/>
        <source>Activity Description</source>
        <translation>Descripción de la actividad</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="81"/>
        <source>Keywords...</source>
        <translation>Palabras clave...</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="85"/>
        <source>&amp;Add Filter</source>
        <translation>&amp;Añadir filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="93"/>
        <source>Filters in use</source>
        <translation>Filtros en uso</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="97"/>
        <source>&amp;Remove Selected Filter</source>
        <translation>&amp;Eliminar filtro seleccionado</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="104"/>
        <source>&amp;Save Filters</source>
        <translation>&amp;Guardar filtros</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="110"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="124"/>
        <source>if</source>
        <translation>si</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="128"/>
        <source>contains</source>
        <translation>contiene</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="134"/>
        <source>&amp;New Filter</source>
        <translation>&amp;Nuevo filtro</translation>
    </message>
    <message>
        <location filename="../src/filtereditor.cpp" line="141"/>
        <source>C&amp;urrent Filters</source>
        <translation>Filtros &amp;actuales</translation>
    </message>
</context>
<context>
    <name>FontPicker</name>
    <message>
        <location filename="../src/fontpicker.cpp" line="50"/>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
</context>
<context>
    <name>HelpWidget</name>
    <message>
        <location filename="../src/helpwidget.cpp" line="26"/>
        <source>Basic Help</source>
        <translation>Ayuda básica</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="39"/>
        <source>Getting started</source>
        <translation>Empezando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="70"/>
        <source>The first time you start Dianara, you should see the Account Configuration dialog. There, enter your Pump.io address as name@server and press the Get Verifier Code button.</source>
        <translation>La primera vez que inicies Dianara, deberías ver la ventana de Configuración de la cuenta. Allí, introduce tu dirección de Pump.io como nombre@servidor y pulsa el botón Obtener código de verificación.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="75"/>
        <source>Then, your usual web browser should load the authorization page in your Pump.io server. There, you&apos;ll have to copy the full VERIFIER code, and paste it into Dianara&apos;s second field. Then press Authorize Application, and once it&apos;s confirmed, press Save Details.</source>
        <translation>A continuación, tu navegador web habitual debería cargar la página de autorización en tu servidor Pump.io. Allí, tendrás que copiar el código &apos;VERIFIER&apos; completo, y pegarlo en el segundo campo de Dianara. Entonces pulsa &apos;Autorizar la aplicación&apos;, y una vez que se confirme, pulsa &apos;Guardar datos&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="81"/>
        <source>At this point, your profile, contact lists and timelines will be loaded.</source>
        <translation>En este momento, se cargará tu perfil, las listas de contactos y las líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="84"/>
        <source>You should take a look at the Program Configuration window, under the Settings - Configure Dianara menu. There are several interesting options there.</source>
        <translation>Deberías echar un vistazo a la ventana &apos;Configuración del programa&apos;, en el menú Configuración - Configurar Dianara. Allí encontrarás varias opciones interesantes.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="40"/>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="104"/>
        <source>You can configure several things to your liking in the settings, like the time interval between timeline updates, how many posts per page you want, highlight colors, notifications or how the system tray icon looks.</source>
        <translation>Puedes ajustar varias cosas a tu gusto en la configuración, como el intervalo de tiempo entre actualizaciones de la línea temporal, cuantos mensajes por página quieres, colores para destacar mensajes, notificaciones o qué aspecto tendrá el icono de la bandeja del sistema.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="41"/>
        <source>Timelines</source>
        <translation>Líneas temporales</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="47"/>
        <source>Contents</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="88"/>
        <source>Keep in mind that there are a lot of places in Dianara where you can get more information by hovering over some text or button with your mouse, and waiting for the tooltip to appear.</source>
        <translation>Recuerda que hay muchos lugares en Dianara donde puedes obtener más información manteniendo el ratón sobre algún texto o botón, y esperando a que aparezca la descripción emergente (tooltip).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="109"/>
        <source>Here, you can also activate the option to always publish your posts as Public by default. You can always change that at the moment of posting.</source>
        <translation>Aquí, también puedes activar la opción para publicar siempre tus mensajes como públicos de forma predefinida. Siempre puedes cambiar esto en el momento de publicar.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="122"/>
        <source>The main timeline, where you&apos;ll see all the stuff posted or shared by the people you follow.</source>
        <translation>La línea temporal principal, donde verás todo lo que ha publicado o compartido la gente a la que sigues.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="125"/>
        <source>Messages timeline, where you&apos;ll see messages sent to you specifically. These messages might have been sent to other people too.</source>
        <translation>Línea temporal de mensajes, donde verás mensajes enviados a ti específicamente. Estos mensajes pueden haber sido enviados también a otras personas.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="129"/>
        <source>Activity timeline, where you&apos;ll see your own posts, or posts shared by you.</source>
        <translation>Línea temporal de actividad, donde verás tus propios mensajes, o mensajes que has compartido.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="132"/>
        <source>Favorites timeline, where you&apos;ll see the posts and comments you&apos;ve liked. This can be used as a bookmark system.</source>
        <translation>Línea temporal de favoritos, donde verás los mensajes que te han gustado. Esto puede usarse como un sistema de marcadores.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="137"/>
        <source>The fifth timeline is the minor timeline, also known as the Meanwhile. This is visible on the left side, though it can be hidden. Here you&apos;ll see minor activities done by everyone you follow, such as comment actions, liking posts or following people.</source>
        <translation>La quinta línea temporal es la línea temporal menor, también conocida como el &quot;Mientras tanto&quot;. Es visible en el lado izquierdo, aunque se puede esconder. Aquí verás actividades secundarias hechas por todo el mundo, como acciones de comentar, marcar mensajes con &quot;Me gusta&quot; o seguir a gente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="148"/>
        <source>These activities might have a &apos;+&apos; button in them. Press it to open the post they&apos;re referencing. Also, as in many other places, you can hover with your mouse to see relevant information in the tooltip.</source>
        <translation>Estas actividades pueden tener un botón &quot;+&quot;. Púlsalo para abrir el mensaje al que hacen referencia. Además, como en muchos otros lugares, puedes mantener el ratón encima para ver información relevante en la descripción emergente (tooltip).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="42"/>
        <source>Posting</source>
        <translation>Publicando</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="93"/>
        <source>If you&apos;re new to Pump.io, take a look at this guide:</source>
        <translation>Si es la primera vez que usas Pump.io, echa un vistazo a esta guía:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="153"/>
        <source>New messages appear highlighted in a different color. You can mark them as read just by clicking on any empty parts of the message.</source>
        <translation>Los mensajes nuevos aparecen destacados en un color diferente. Puedes marcarlos como leídos haciendo clic en cualquier parte vacía del mensaje.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="163"/>
        <source>You can post notes by clicking in the text field at the top of the window or by pressing Control+N. Setting a title for your post is optional, but highly recommended, as it will help to better identify references to your post in the minor feed, e-mail notifications, etc.</source>
        <translation>Puedes publicar notas haciendo clic en el campo de texto de la parte superior de la ventana o pulsando Control+N. Añadir un título al mensaje es opcional, pero muy recomendable, ya que ayudará a identificar mejor las referencias a tu mensaje en la línea temporal menor, notificaciones por e-mail, etc.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="170"/>
        <source>It is possible to attach images, audio, video, and general files, like PDF documents, to your post.</source>
        <translation>Es posible adjuntar imágenes, audio, video y archivos generales, como documentos PDF, a tu mensaje.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="173"/>
        <source>You can use the Format button to add formatting to your text, like bold or italics. Some of these options require text to be selected before they are used.</source>
        <translation>Puedes usar el botón Formato para añadir formato al texto, como negrita o cursiva. Algunas de estas opciones requieren que se seleccione el texto antes de usarlas.</translation>
    </message>
    <message>
        <source>You can select who will see your post by using the To and CC buttons.</source>
        <translation type="obsolete">Puedes seleccionar quien verá tu mensaje usando los botones Para y CC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="180"/>
        <source>If you add a specific person to the &apos;To&apos; list, they will receive your message in their direct messages tab.</source>
        <translation>Si añades una persona específica a la lista &apos;Para&apos;, recibirá tu mensaje en su pestaña de mensajes directos.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="191"/>
        <source>You can create private messages by adding specific people to these lists, and unselecting the Followers or the Public options.</source>
        <translation>Puedes hacer mensajes privados añadiendo gente específica a estas listas, y desmarcando las opciones Público y Seguidores.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="43"/>
        <source>Managing contacts</source>
        <translation>Gestionando los contactos</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="201"/>
        <source>You can see the lists of people you follow, and who follow you from the Contacts tab.</source>
        <translation>Puedes ver las listas de gente a la que sigues, y que te sigue, en la pestaña Contactos.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="204"/>
        <source>There, you can also manage person lists, used mainly to send posts to specific groups of people.</source>
        <translation>Allí puedes gestionar también las listas de personas, que se utilizan principalmente para enviar mensajes a grupos de gente específicos.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="207"/>
        <source>You can click on any avatars in the posts, the comments, and the Meanwhile column, and you will get a menu with several options, one of which is following or unfollowing that person.</source>
        <translation>Puedes hacer clic en cualquier avatar en los mensajes, los comentarios, y la columna &quot;Mientras tanto&quot;, y verás un menú con varias opciones, una de las cuales es seguir o dejar de seguir a esta persona.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="44"/>
        <source>Keyboard controls</source>
        <translation>Control por teclado</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="95"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="119"/>
        <source>There are seven timelines:</source>
        <translation>Hay siete líneas temporales:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="143"/>
        <source>The sixth and seventh timelines are also minor timelines, similar to the Meanwhile, but containing only activities directly addressed to you (Mentions) and activities done by you (Actions).</source>
        <translation>La sexta y séptima líneas temporales también son líneas temporales menores, parecidas al &quot;Mientras tanto&quot;, pero contienen únicamente actividades dirigidas a ti (Menciones) y actividades hechas por ti (Acciones).</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="177"/>
        <source>You can select who will see your post by using the To and Cc buttons.</source>
        <translation>Puedes seleccionar quien verá tu mensaje usando los botones &apos;Para&apos; y &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="183"/>
        <source>You can also type &apos;@&apos; and the first characters of the name of a contact to bring up a popup menu with matching choices.</source>
        <translation>También puedes teclear &apos;@&apos; y los primeros caracteres del nombre de un contacto para mostrar un menú emergente con opciones que coincidan.</translation>
    </message>
    <message>
        <source>Choose one with the arrows and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation type="obsolete">Elige una con las teclas de cursor y pulsa Enter para completar el nombre. Esto añadirá a esta persona a la lista de destinatarios.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="212"/>
        <source>You can also send a direct message (initially private) to that contact from this menu.</source>
        <translation>También puedes enviar un mensaje directo (inicialmente privado) a este contacto desde este menú.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="215"/>
        <source>You can find a list with some Pump.io users and other information here:</source>
        <translation>Puedes encontrar una lista con algunos usuarios de Pump.io y otros datos aquí:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="227"/>
        <source>The most common actions found on the menus have keyboard shortcuts written next to them, like F5 or Control+N.</source>
        <translation>Las acciones más comunes que se encuentran en los menús tienen atajos de teclado escritos al lado, como F5 o Control+N.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="231"/>
        <source>Besides that, you can use:</source>
        <translation>Aparte de eso, puedes usar:</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="234"/>
        <source>Control+Up/Down/PgUp/PgDown/Home/End to move around the timeline.</source>
        <translation>Control+Arriba/Abajo/RePag/AvPag/Inicio/Fin para moverte por la línea temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="237"/>
        <source>Control+Left/Right to jump one page in the timeline.</source>
        <translation>Control+Izquierda/Derecha para pasar una página en la línea temporal.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="240"/>
        <source>Control+G to go to any page in the timeline directly.</source>
        <translation>Control+G para ir a cualquier página de la línea temporal directamente.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="243"/>
        <source>Control+1/2/3 to switch between the minor feeds.</source>
        <translation>Control+1/2/3 para cambiar entre las líneas temporales menores.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="246"/>
        <source>Control+Enter to post, when you&apos;re done composing a note or a comment. If the note is empty, you can cancel it by pressing ESC.</source>
        <translation>Control+Enter para publicar, cuando hayas acabado de redactar una nota o un comentario. Si la nota está vacía, la puedes cancelar pulsando ESC.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="45"/>
        <source>Command line options</source>
        <translation>Opciones de línea de comandos</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="187"/>
        <source>Choose one with the arrow keys and press Enter to complete the name. This will add that person to the recipients list.</source>
        <translation>Elige una con las teclas de cursor y pulsa Enter para completar el nombre. Esto añadirá a esta persona a la lista de destinatarios.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="250"/>
        <source>While composing a note, press Enter to jump from the title to the message body. Also, pressing the Up arrow while you&apos;re at the start of the message, jumps back to the title.</source>
        <translation>Mientras redactas una nota, pulsa Enter para pasar del título al cuerpo del mensaje. Además, pulsando la flecha arriba cuando te encuentres al principio del mensaje, vuelve al título.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="255"/>
        <source>Control+Enter to finish creating a list of recipients for a post, in the &apos;To&apos; or &apos;Cc&apos; lists.</source>
        <translation>Control+Enter para acabar de crear una lista de destinatarios para un mensaje, en las listas &apos;Para&apos; o &apos;Cc&apos;.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="265"/>
        <source>You can use the --config parameter to run the program with a different configuration. This can be useful to use two or more different accounts. You can even run two instances of Dianara at the same time.</source>
        <translation>Puedes usar el parámetro --config para ejecutar el programa con una configuración diferente. Esto puede ser útil para usar dos o más cuentas diferentes. Incluso puedes ejecutar dos instancias de Dianara a la vez.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="270"/>
        <source>Use the --debug parameter to have extra information in your terminal window, about what the program is doing.</source>
        <translation>Usa el parámetro --debug para tener información adicional en la ventana de la terminal, sobre lo que está haciendo el programa.</translation>
    </message>
    <message>
        <location filename="../src/helpwidget.cpp" line="284"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../src/imageviewer.cpp" line="56"/>
        <source>Image</source>
        <translation>Imagen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="64"/>
        <source>Resolution and file size</source>
        <translation>Resolución y tamaño del archivo</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="67"/>
        <source>ESC to close, secondary-click for options</source>
        <translation>ESC para cerrar, clic secundario para opciones</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="72"/>
        <source>&amp;Save As...</source>
        <translation>&amp;Guardar como...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="87"/>
        <source>&amp;Restart Animation</source>
        <translation>&amp;Reiniciar animación</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="97"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="146"/>
        <source>Save Image...</source>
        <translation>Guardar imagen...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="153"/>
        <source>Close Viewer</source>
        <translation>Cerrar visor</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="182"/>
        <source>Save Image As...</source>
        <translation>Guardar imagen como...</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="184"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="185"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="196"/>
        <source>Error saving image</source>
        <translation>Error guardando la imagen</translation>
    </message>
    <message>
        <location filename="../src/imageviewer.cpp" line="197"/>
        <source>There was a problem while saving %1.

Filename should end in .jpg or .png extensions.</source>
        <translation>Ha habido un problema al guardar %1.

El nombre del archivo debería acabar con la extensión .jpg o .png.</translation>
    </message>
</context>
<context>
    <name>ListsManager</name>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="38"/>
        <source>Members</source>
        <translation>Miembros</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="51"/>
        <source>Add Mem&amp;ber</source>
        <translation>Añadir miem&amp;bro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="58"/>
        <source>&amp;Remove Member</source>
        <translation>&amp;Eliminar miembro</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="66"/>
        <source>&amp;Delete Selected List</source>
        <translation>&amp;Borrar lista seleccionada</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="73"/>
        <source>Add New &amp;List</source>
        <translation>Añadir nueva &amp;lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="90"/>
        <source>Create L&amp;ist</source>
        <translation>Crear l&amp;ista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="97"/>
        <source>&amp;Add to List</source>
        <translation>&amp;Añadir a lista</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="304"/>
        <source>Are you sure you want to delete %1?</source>
        <comment>1=Name of a person list</comment>
        <translation>¿Estás seguro de que quieres eliminar %1?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="436"/>
        <source>Remove person from list?</source>
        <translation>¿Quitar persona de la lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="437"/>
        <source>Are you sure you want to remove %1 from the %2 list?</source>
        <comment>1=Name of a person, 2=name of a list</comment>
        <translation>¿Estás seguro de que quieres quitar a %1 de la lista %2?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="442"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sí</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="76"/>
        <source>Type a name for the new list...</source>
        <translation>Escribe un nombre para la nueva lista...</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="84"/>
        <source>Type an optional description here</source>
        <translation>Escribe una descripción opcional aquí</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="303"/>
        <source>WARNING: Delete list?</source>
        <translation>ADVERTENCIA: ¿Eliminar lista?</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="306"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminarla</translation>
    </message>
    <message>
        <location filename="../src/listsmanager.cpp" line="306"/>
        <location filename="../src/listsmanager.cpp" line="442"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
</context>
<context>
    <name>LogViewer</name>
    <message>
        <location filename="../src/logviewer.cpp" line="25"/>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="54"/>
        <source>Clear &amp;Log</source>
        <translation>Borrar el &amp;registro</translation>
    </message>
    <message>
        <location filename="../src/logviewer.cpp" line="60"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="2153"/>
        <source>&amp;Messages</source>
        <translation>&amp;Mensajes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>&amp;Contacts</source>
        <translation>&amp;Contactos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="949"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="815"/>
        <source>&amp;Session</source>
        <translation>&amp;Sesión</translation>
    </message>
    <message>
        <source>Meanwhile...</source>
        <translation type="obsolete">Mientras tanto...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="961"/>
        <source>Side &amp;Panel</source>
        <translation>Panel &amp;lateral</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="981"/>
        <source>Status &amp;Bar</source>
        <translation>&amp;Barra de estado</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2146"/>
        <source>&amp;Timeline</source>
        <translation>Línea &amp;temporal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2160"/>
        <source>&amp;Activity</source>
        <translation>&amp;Actividad</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="662"/>
        <location filename="../src/mainwindow.cpp" line="1209"/>
        <source>Initializing...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="697"/>
        <source>Your account is not configured yet.</source>
        <translation>Tu cuenta no está configurada todavía.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="685"/>
        <source>Dianara started.</source>
        <translation>Dianara iniciado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="150"/>
        <source>Minor activities done by everyone, such as replying to posts</source>
        <translation>Actividades menores hechas por todo el mundo, tales como respuestas a mensajes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="169"/>
        <source>Minor activities addressed to you</source>
        <translation>Actividades menores dirigidas a ti</translation>
    </message>
    <message>
        <source>Actions</source>
        <translation type="obsolete">Acciones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="185"/>
        <source>Minor activities done by you</source>
        <translation>Actividades menores hechas por ti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="393"/>
        <source>The people you follow, the ones who follow you, and your person lists</source>
        <translation>La gente a la que sigues, la que te sigue, y tus listas de personas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="686"/>
        <source>Running with Qt v%1.</source>
        <translation>Funcionando con Qt v%1.</translation>
    </message>
    <message>
        <source>&amp;Update Timeline</source>
        <translation type="obsolete">Act&amp;ualizar línea temporal</translation>
    </message>
    <message>
        <source>Update &amp;Messages</source>
        <translation type="obsolete">Actualizar &amp;mensajes</translation>
    </message>
    <message>
        <source>Update &amp;Activity</source>
        <translation type="obsolete">Actualizar &amp;actividad</translation>
    </message>
    <message>
        <source>Update Fa&amp;vorites</source>
        <translation type="obsolete">Actualizar &amp;favoritos</translation>
    </message>
    <message>
        <source>Update Mentions</source>
        <translation type="obsolete">Actualizar menciones</translation>
    </message>
    <message>
        <source>Update Actions</source>
        <translation type="obsolete">Actualizar acciones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="912"/>
        <source>Auto-update &amp;Timelines</source>
        <translation>Auto-actualizar líneas &amp;temporales</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="924"/>
        <source>Mark All as Read</source>
        <translation>Marcar todo como leído</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="935"/>
        <source>&amp;Post a Note</source>
        <translation>&amp;Publicar una nota</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="958"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="972"/>
        <source>&amp;Toolbar</source>
        <translation>&amp;Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="993"/>
        <source>Full &amp;Screen</source>
        <translation>&amp;Pantalla completa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1004"/>
        <source>&amp;Log</source>
        <translation>&amp;Registro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1016"/>
        <source>S&amp;ettings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1020"/>
        <source>Edit &amp;Profile</source>
        <translation>Editar &amp;perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1028"/>
        <source>&amp;Account</source>
        <translation>&amp;Cuenta</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <source>&amp;Configure Dianara</source>
        <translation>&amp;Configurar Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1055"/>
        <source>&amp;Help</source>
        <translation>Ay&amp;uda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1058"/>
        <source>Basic &amp;Help</source>
        <translation>&amp;Ayuda básica</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1070"/>
        <source>Visit &amp;Website</source>
        <translation>Visitar sitio &amp;web</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1077"/>
        <source>Report a &amp;Bug</source>
        <translation>Informar de un &amp;fallo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <source>Pump.io User &amp;Guide</source>
        <translation>&amp;Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1099"/>
        <source>List of Some Pump.io &amp;Users</source>
        <translation>Lista de algunos &amp;usuarios de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1109"/>
        <source>About &amp;Dianara</source>
        <translation>Acerca de &amp;Dianara</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1156"/>
        <source>Toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1194"/>
        <source>Open the log viewer</source>
        <translation>Abrir el visor del registro</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1216"/>
        <source>Auto-updating enabled</source>
        <translation>Auto-actualizaciones activadas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1223"/>
        <source>Auto-updating disabled</source>
        <translation>Auto-actualizaciones desactivadas</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1660"/>
        <source>Proxy password required</source>
        <translation>Contraseña de proxy necesaria</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1661"/>
        <source>You have configured a proxy server with authentication, but the password is not set.</source>
        <translation>Has configurado un servidor proxy con autenticación, pero la contraseña no está definida.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1666"/>
        <source>Enter the password for your proxy server:</source>
        <translation>Introduce la contraseña para tu servidor proxy:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1739"/>
        <source>Your biography is empty</source>
        <translation>Tu biografía está vacía</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1774"/>
        <source>Click to edit your profile</source>
        <translation>Haz clic para editar tu perfil</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1799"/>
        <source>Starting automatic update of timelines, once every %1 minutes.</source>
        <translation>Iniciando actualización automática de líneas temporales, una vez cada %1 minutos.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1806"/>
        <source>Stopping automatic update of timelines.</source>
        <translation>Deteniendo actualización automática de líneas temporales.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1937"/>
        <source>Received %1 older posts in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of a timeline</comment>
        <translation>Se han recibido %1 mensajes anteriores en &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2095"/>
        <source>Last update: %1</source>
        <translation>Última actualización: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1944"/>
        <location filename="../src/mainwindow.cpp" line="2307"/>
        <source>&apos;%1&apos; updated.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>&apos;%1&apos; actualizada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1968"/>
        <source>1 pending to receive.</source>
        <comment>singular, one post</comment>
        <translation type="unfinished">1 pendiente de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1973"/>
        <source>%1 pending to receive.</source>
        <comment>plural, several posts</comment>
        <translation type="unfinished">%1 pendientes de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1988"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 destacado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1993"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 destacados.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2005"/>
        <source>1 filtered out.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 filtrado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2010"/>
        <source>%1 filtered out.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 filtrados.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2023"/>
        <source>1 deleted.</source>
        <comment>singular, refers to a post</comment>
        <translation>1 eliminado.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2028"/>
        <source>%1 deleted.</source>
        <comment>plural, refers to posts</comment>
        <translation>%1 eliminados.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2060"/>
        <source>No new posts.</source>
        <translation>No hay mensajes nuevos.</translation>
    </message>
    <message>
        <source>Mentions</source>
        <translation type="obsolete">Menciones</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2318"/>
        <source>There is 1 new activity.</source>
        <translation>Hay 1 actividad nueva.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2322"/>
        <source>There are %1 new activities.</source>
        <translation>Hay %1 actividades nuevas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2334"/>
        <source>1 pending to receive.</source>
        <comment>singular, 1 activity</comment>
        <translation type="unfinished">1 pendent de rebre.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2339"/>
        <source>%1 pending to receive.</source>
        <comment>plural, several activities</comment>
        <translation type="unfinished">%1 pendientes de recibir.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2356"/>
        <source>1 highlighted.</source>
        <comment>singular, refers to an activity</comment>
        <translation>1 destacada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2361"/>
        <source>%1 highlighted.</source>
        <comment>plural, refers to activities</comment>
        <translation>%1 destacadas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2406"/>
        <source>No new activities.</source>
        <translation>No hay actividades nuevas.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2500"/>
        <source>Link to: %1</source>
        <translation>Enlace a: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2769"/>
        <source>With Dianara you can see your timelines, create new posts, upload pictures and other media, interact with posts, manage your contacts and follow new people.</source>
        <translation>Con Dianara puedes ver tus líneas temporales, crear nuevos mensajes, subir fotos y multimedia, interactuar con los mensajes, gestionar tus contactos y seguir gente nueva.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2778"/>
        <source>English translation by JanKusanagi.</source>
        <comment>TRANSLATORS: Change this with your language and name. If there was another translator before you, add your name after theirs ;)</comment>
        <translation>Traducción al castellano por JanKusanagi.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2816"/>
        <source>&amp;Hide Window</source>
        <translation>&amp;Ocultar ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2826"/>
        <source>&amp;Show Window</source>
        <translation>&amp;Mostrar ventana</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2858"/>
        <location filename="../src/mainwindow.cpp" line="2936"/>
        <source>Quit?</source>
        <translation>¿Salir?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2859"/>
        <source>You are composing a note or a comment.</source>
        <translation>Estás redactando una nota o un comentario.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2861"/>
        <source>Do you really want to close Dianara?</source>
        <translation>¿Realmente quieres cerrar Dianara?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2862"/>
        <location filename="../src/mainwindow.cpp" line="2944"/>
        <source>&amp;Yes, close the program</source>
        <translation>&amp;Sí, cerrar el programa</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2862"/>
        <location filename="../src/mainwindow.cpp" line="2944"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2937"/>
        <source>System tray icon is not available.</source>
        <translation>El icono de la bandeja del sistema no está disponible.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2939"/>
        <source>Dianara cannot be hidden in the system tray.</source>
        <translation>Dianara no se puede ocultar en la bandeja del sistema.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2942"/>
        <source>Do you want to close the program completely?</source>
        <translation>¿Quieres cerrar el programa completamente?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1955"/>
        <source>There is 1 new post.</source>
        <translation>Hay 1 mensaje nuevo.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1959"/>
        <source>There are %1 new posts.</source>
        <translation>Hay %1 mensajes nuevos.</translation>
    </message>
    <message>
        <source>Timeline updated.</source>
        <translation type="obsolete">Línea temporal actualizada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1950"/>
        <source>Timeline updated at %1.</source>
        <translation>Línea temporal actualizada a las %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2141"/>
        <source>Total posts: %1</source>
        <translation>Total de mensajes: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2223"/>
        <source>Your Pump.io account is not configured</source>
        <translation>Tu cuenta Pump.io no está configurada</translation>
    </message>
    <message>
        <source>Received %1 older items in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation type="obsolete">Se han recibido %1 elementos anteriores en &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2766"/>
        <source>Dianara is a pump.io social networking client.</source>
        <translation>Dianara es un cliente de red social para pump.io.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2785"/>
        <source>Thanks to all the testers, translators and packagers, who help make Dianara better!</source>
        <translation>¡Gracias a todos los &apos;testers&apos;, traductores y empaquetadores, que ayudan a hacer Dianara mejor!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2792"/>
        <source>Dianara is licensed under the GNU GPL license, and uses some Oxygen icons: http://www.oxygen-icons.org/ (LGPL licensed)</source>
        <translation>Dianara está licenciado bajo la licencia GNU GPL, y utiliza algunos iconos Oxygen: http://www.oxygen-icons.org/ (Licencia LGPL)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2147"/>
        <source>The main timeline</source>
        <translation>La línea temporal principal</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="647"/>
        <source>Press F1 for help</source>
        <translation>Pulsa F1 para ver la ayuda</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="818"/>
        <source>Update %1</source>
        <translation>Actualizar %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2161"/>
        <source>Your own posts</source>
        <translation>Tus propios mensajes</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2168"/>
        <source>Your favorited posts</source>
        <translation>Los mensajes que te gustan</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2154"/>
        <source>Messages sent explicitly to you</source>
        <translation>Mensajes enviados explícitamente a ti</translation>
    </message>
    <message>
        <source>Update Minor &amp;Feed</source>
        <translatorcomment>meh...</translatorcomment>
        <translation type="obsolete">Actualizar línea temporal meno&amp;r</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1037"/>
        <source>&amp;Filters and Highlighting</source>
        <translatorcomment>hmmm</translatorcomment>
        <translation>&amp;Filtros y destacados</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1093"/>
        <source>Some Pump.io &amp;Tips</source>
        <translation>Algunos &amp;consejos sobre Pump.io</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2167"/>
        <source>Favor&amp;ites</source>
        <translation>Fa&amp;voritos</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2299"/>
        <source>Received %1 older activities in &apos;%2&apos;.</source>
        <comment>%1 is a number, %2 = name of feed</comment>
        <translation>Se han recibido %1 actividades anteriores en &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2312"/>
        <source>Minor feed updated at %1.</source>
        <translation>Línea temporal menor actualizada a las %1.</translation>
    </message>
    <message>
        <source>Minor feed updated.</source>
        <translation type="obsolete">Línea temporal menor actualizada.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2757"/>
        <source>About Dianara</source>
        <translation>Acerca de Dianara</translation>
    </message>
</context>
<context>
    <name>MinorFeed</name>
    <message>
        <location filename="../src/minorfeed.cpp" line="64"/>
        <source>Older Activities</source>
        <translation>Actividades anteriores</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="67"/>
        <source>Get previous minor activities</source>
        <translation>Obtener actividades menores anteriores</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="94"/>
        <source>There are no activities to show yet.</source>
        <translation>Aún no hay actividades para mostrar.</translation>
    </message>
    <message>
        <location filename="../src/minorfeed.cpp" line="468"/>
        <source>Get %1 newer</source>
        <comment>As in: Get 3 newer (activities)</comment>
        <translation type="unfinished">Recibir %1 más nuevas</translation>
    </message>
</context>
<context>
    <name>MinorFeedItem</name>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="67"/>
        <source>Using %1</source>
        <comment>Application used to generate this activity</comment>
        <translation>Usando %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="81"/>
        <source>To: %1</source>
        <comment>1=people to whom this activity was sent</comment>
        <translation>Para: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="88"/>
        <source>Cc: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation>Cc: %1</translation>
    </message>
    <message>
        <source>CC: %1</source>
        <comment>1=people to whom this activity was sent as CC</comment>
        <translation type="obsolete">CC: %1</translation>
    </message>
    <message>
        <location filename="../src/minorfeeditem.cpp" line="257"/>
        <source>Open referenced post</source>
        <translation>Abrir el mensaje referenciado</translation>
    </message>
</context>
<context>
    <name>MiscHelpers</name>
    <message>
        <location filename="../src/mischelpers.cpp" line="238"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
</context>
<context>
    <name>PageSelector</name>
    <message>
        <location filename="../src/pageselector.cpp" line="26"/>
        <source>Jump to page</source>
        <translation>Saltar a página</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="33"/>
        <source>Page number:</source>
        <translation>Número de página:</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="45"/>
        <source>&amp;Go</source>
        <translation>&amp;Ir</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="51"/>
        <source>Go to &amp;last page</source>
        <translation>Ir a la última &amp;página</translation>
    </message>
    <message>
        <location filename="../src/pageselector.cpp" line="58"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>PeopleWidget</name>
    <message>
        <location filename="../src/peoplewidget.cpp" line="40"/>
        <source>&amp;Search:</source>
        <translation>&amp;Buscar:</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="43"/>
        <source>Enter a name here to search for it</source>
        <translation>Escribe aquí un nombre para buscarlo</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="95"/>
        <source>Add a contact to a list</source>
        <translation>Añadir un contacto a una lista</translation>
    </message>
    <message>
        <location filename="../src/peoplewidget.cpp" line="105"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>Post</name>
    <message>
        <location filename="../src/post.cpp" line="1670"/>
        <source>Like this post</source>
        <translation>Decir que te gusta este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1671"/>
        <source>Like</source>
        <translation>Me gusta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="233"/>
        <source>Shared on %1</source>
        <translation>Compartido el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="322"/>
        <location filename="../src/post.cpp" line="505"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="937"/>
        <source>In</source>
        <translation>En</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="196"/>
        <location filename="../src/post.cpp" line="400"/>
        <source>To</source>
        <translation>Para</translation>
    </message>
    <message>
        <source>CC</source>
        <translation type="obsolete">CC</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="237"/>
        <location filename="../src/post.cpp" line="889"/>
        <source>Using %1</source>
        <comment>1=Program used for posting or sharing</comment>
        <translation>Usando %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="484"/>
        <source>Parent</source>
        <comment>As in &apos;Open the parent post&apos;. Try to use the shortest word!</comment>
        <translation>Padre</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="492"/>
        <source>Open the parent post, to which this one replies</source>
        <translation>Abrir el mensaje padre, al que éste responde</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="654"/>
        <source>Modify this post</source>
        <translation>Modificar este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="728"/>
        <source>Join Group</source>
        <translation>Unirse al grupo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="733"/>
        <source>%1 members in the group</source>
        <translation>%1 miembros en el grupo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1304"/>
        <source>1 like</source>
        <translation>Le gusta a 1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1373"/>
        <source>1 comment</source>
        <translation>1 comentario</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1457"/>
        <source>Shared %1 times</source>
        <translation>Compartido %1 veces</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="628"/>
        <source>Share</source>
        <translation>Compartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="37"/>
        <source>Click to download the attachment</source>
        <translation>Haz clic para descargar el archivo adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="130"/>
        <source>Post</source>
        <comment>Noun, not verb</comment>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="882"/>
        <source>Type</source>
        <comment>As in: type of object</comment>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="896"/>
        <source>Modified on %1</source>
        <translation>Modificado el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="652"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1047"/>
        <source>Image is animated. Click on it to play.</source>
        <translation>La imagen es animada. Haz clic para reproducirla.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1071"/>
        <source>Loading image...</source>
        <translation>Cargando imagen...</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1110"/>
        <source>Attached file</source>
        <translation>Archivo adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1309"/>
        <source>%1 likes</source>
        <translation>Les gusta a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1378"/>
        <source>%1 comments</source>
        <translation>%1 comentarios</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="663"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="187"/>
        <source>Via %1</source>
        <translatorcomment>Meh...</translatorcomment>
        <translation>A través de %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1585"/>
        <source>Edited: %1</source>
        <translation>Editado: %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="880"/>
        <source>Posted on %1</source>
        <comment>1=Date</comment>
        <translation>Publicado el %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="609"/>
        <source>If you select some text, it will be quoted.</source>
        <translation>Si seleccionas parte del texto, será citado.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="636"/>
        <source>Unshare</source>
        <translation>Dejar de compartir</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="638"/>
        <source>Unshare this post</source>
        <translation>Dejar de compartir este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="282"/>
        <source>Open post in web browser</source>
        <translation>Abrir el mensaje en el navegador web</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="202"/>
        <location filename="../src/post.cpp" line="416"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="290"/>
        <source>Copy post link to clipboard</source>
        <translation>Copiar el enlace del mensaje al portapapeles</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="309"/>
        <source>Normalize text colors</source>
        <translation>Normalizar colores del texto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="605"/>
        <source>Comment</source>
        <comment>verb, for the comment button</comment>
        <translation>Comentar</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="607"/>
        <source>Reply to this post.</source>
        <translation>Responder a este mensaje.</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="630"/>
        <source>Share this post with your contacts</source>
        <translation>Compartir este mensaje con tus contactos</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="665"/>
        <source>Erase this post</source>
        <translation>Borrar este mensaje</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1066"/>
        <source>Couldn&apos;t load image!</source>
        <translation>¡No se ha podido cargar la imagen!</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1090"/>
        <source>Attached Audio</source>
        <translation>Audio adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1100"/>
        <source>Attached Video</source>
        <translation>Vídeo adjunto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1278"/>
        <source>%1 likes this</source>
        <comment>One person</comment>
        <translation>Le gusta a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1283"/>
        <source>%1 like this</source>
        <comment>More than one person</comment>
        <translation>Les gusta a %1</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1418"/>
        <source>%1 shared this</source>
        <comment>%1 = One person name</comment>
        <translation>%1 ha compartido esto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1423"/>
        <source>%1 shared this</source>
        <comment>%1 = Names for more than one person</comment>
        <translation>%1 han compartido esto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1452"/>
        <source>Shared once</source>
        <translation>Compartido una vez</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1663"/>
        <source>You like this</source>
        <translation>Te gusta esto</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1664"/>
        <source>Unlike</source>
        <translation>Ya no me gusta</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1777"/>
        <source>Share post?</source>
        <translation>¿Compartir mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1778"/>
        <source>Do you want to share %1&apos;s post?</source>
        <translation>¿Quieres compartir el mensaje de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1780"/>
        <source>&amp;Yes, share it</source>
        <translation>&amp;Sí, compartirlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1780"/>
        <location filename="../src/post.cpp" line="1799"/>
        <location filename="../src/post.cpp" line="1843"/>
        <source>&amp;No</source>
        <translation>&amp;No</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1796"/>
        <source>Unshare post?</source>
        <translation>¿Dejar de compartir el mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1797"/>
        <source>Do you want to unshare %1&apos;s post?</source>
        <translation>¿Quieres dejar de compartir el mensaje de %1?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1799"/>
        <source>&amp;Yes, unshare it</source>
        <translation>&amp;Sí,dejar de compartirlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1840"/>
        <source>WARNING: Delete post?</source>
        <translation>ADVERTENCIA: ¿Eliminar mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1841"/>
        <source>Are you sure you want to delete this post?</source>
        <translation>¿Estás seguro de que quieres eliminar este mensaje?</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="1843"/>
        <source>&amp;Yes, delete it</source>
        <translation>&amp;Sí, eliminarlo</translation>
    </message>
    <message>
        <location filename="../src/post.cpp" line="36"/>
        <source>Click the image to see it in full size</source>
        <translation>Haz clic en la imagen para verla en tamaño completo</translation>
    </message>
</context>
<context>
    <name>ProfileEditor</name>
    <message>
        <location filename="../src/profileeditor.cpp" line="27"/>
        <source>Profile Editor</source>
        <translation>Editor de perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="43"/>
        <source>This is your Pump address</source>
        <translation>Esta es tu dirección Pump</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="49"/>
        <source>This is the e-mail address associated with your account, for things such as notifications and password recovery</source>
        <translation>Esta es la dirección de e-mail asociada con tu cuenta, para cosas como notificaciones y recuperación de la contraseña</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="60"/>
        <source>Change &amp;Avatar...</source>
        <translation>Cambiar &amp;avatar...</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="68"/>
        <source>This is your visible name</source>
        <translation>Este es tu nombre visible</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="81"/>
        <source>&amp;Save Profile</source>
        <translation>&amp;Guardar perfil</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="86"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="106"/>
        <source>Webfinger ID</source>
        <translation>ID Webfinger</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="107"/>
        <source>E-mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="109"/>
        <source>Avatar</source>
        <translation>Avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="111"/>
        <source>Full &amp;Name</source>
        <translation>&amp;Nombre completo</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="112"/>
        <source>&amp;Hometown</source>
        <translation>Ciuda&amp;d</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="113"/>
        <source>&amp;Bio</source>
        <translation>&amp;Bio</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="155"/>
        <source>Not set</source>
        <comment>In reference to the e-mail not being set for the account</comment>
        <translation>Sin definir</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="212"/>
        <source>Select avatar image</source>
        <translation>Selecciona imagen de avatar</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="214"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="216"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="238"/>
        <source>Invalid image</source>
        <translation>Imagen no válida</translation>
    </message>
    <message>
        <location filename="../src/profileeditor.cpp" line="239"/>
        <source>The selected image is not valid.</source>
        <translation>La imagen seleccionada no es válida.</translation>
    </message>
</context>
<context>
    <name>ProxyDialog</name>
    <message>
        <location filename="../src/proxydialog.cpp" line="30"/>
        <source>Proxy Configuration</source>
        <translation>Configuración de proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="38"/>
        <source>Do not use a proxy</source>
        <translation>No utilizar un proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="55"/>
        <source>Your proxy username</source>
        <translation>Tu nombre de usuario para el proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="60"/>
        <source>Note: Password is not stored in a secure manner. If you wish, you can leave the field empty, and you&apos;ll be prompted for the password on startup.</source>
        <translation>Nota: La contraseña no se guarda de forma segura. Si lo deseas, puedes dejar el campo vacío, y se te pedirá la contraseña al iniciar.</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="74"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="80"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="95"/>
        <source>Proxy &amp;Type</source>
        <translation>&amp;Tipo de proxy</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="97"/>
        <source>&amp;Hostname</source>
        <translation>&amp;Servidor</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="99"/>
        <source>&amp;Port</source>
        <translation>&amp;Puerto</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="101"/>
        <source>Use &amp;Authentication</source>
        <translation>Usar &amp;autenticación</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="103"/>
        <source>&amp;User</source>
        <translation>&amp;Usuario</translation>
    </message>
    <message>
        <location filename="../src/proxydialog.cpp" line="105"/>
        <source>Pass&amp;word</source>
        <translation>Con&amp;traseña</translation>
    </message>
</context>
<context>
    <name>Publisher</name>
    <message>
        <location filename="../src/publisher.cpp" line="150"/>
        <location filename="../src/publisher.cpp" line="183"/>
        <location filename="../src/publisher.cpp" line="430"/>
        <location filename="../src/publisher.cpp" line="447"/>
        <source>Public</source>
        <translation>Público</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="155"/>
        <location filename="../src/publisher.cpp" line="188"/>
        <location filename="../src/publisher.cpp" line="434"/>
        <location filename="../src/publisher.cpp" line="451"/>
        <source>Followers</source>
        <translation>Seguidores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="251"/>
        <source>Picture</source>
        <translation>Foto</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="257"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="263"/>
        <source>Video</source>
        <translation>Vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="285"/>
        <source>Ad&amp;d...</source>
        <translation>&amp;Añadir...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="287"/>
        <source>Upload media, like pictures or videos</source>
        <translation>Subir multimedia, como fotos o vídeos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="800"/>
        <source>Select Picture...</source>
        <translation>Seleccionar foto...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="69"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="802"/>
        <source>Find the picture in your folders</source>
        <translation>Encontrar la foto en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="170"/>
        <location filename="../src/publisher.cpp" line="203"/>
        <source>People...</source>
        <translation>Personas...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="177"/>
        <source>Select who will see this post</source>
        <translation>Selecciona quien verá este mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="175"/>
        <source>To...</source>
        <translation>Para...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="160"/>
        <location filename="../src/publisher.cpp" line="193"/>
        <source>Lists</source>
        <translation>Listas</translation>
    </message>
    <message>
        <source>CC...</source>
        <translation type="obsolete">CC...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="210"/>
        <source>Select who will get a copy of this post</source>
        <translation>Selecciona quien recibirá una copia de este mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="269"/>
        <source>Other</source>
        <comment>as in other kinds of files</comment>
        <translation>Otros</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="319"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="321"/>
        <source>Cancel the post</source>
        <translation>Cancelar el mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="804"/>
        <source>Picture not set</source>
        <translation>Foto no seleccionada</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="823"/>
        <source>Select Audio File...</source>
        <translation>Seleccionar archivo de audio...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="825"/>
        <source>Find the audio file in your folders</source>
        <translation>Encontrar el archivo de audio en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="827"/>
        <source>Audio file not set</source>
        <translation>Archivo de audio no seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="847"/>
        <source>Select Video...</source>
        <translation>Seleccionar video...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="849"/>
        <source>Find the video in your folders</source>
        <translation>Encontrar el vídeo en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="851"/>
        <source>Video not set</source>
        <translation>Vídeo no seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="870"/>
        <source>Select File...</source>
        <translation>Seleccionar archivo...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="872"/>
        <source>Find the file in your folders</source>
        <translation>Encontrar el archivo en tus carpetas</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="874"/>
        <source>File not set</source>
        <translation>Archivo no seleccionado</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="911"/>
        <location filename="../src/publisher.cpp" line="954"/>
        <source>Error: Already composing</source>
        <translation>Error: Ya se está redactando</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="912"/>
        <source>You can&apos;t edit a post at this time, because a post is already being composed.</source>
        <translation>No puedes editar un mensaje en este momento, porque ya se está redactando un mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="928"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="941"/>
        <source>Editing post</source>
        <translation>Editando mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="955"/>
        <source>You can&apos;t create a message for %1 at this time, because a post is already being composed.</source>
        <translation>No puedes crear un mensaje para %1 en este momento, porque ya se está redactando un mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1021"/>
        <source>Posting failed.

Try again.</source>
        <translation>Ha fallado la publicación.

Prueba de nuevo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1310"/>
        <source>Warning: You have no followers yet</source>
        <translation>Advertencia: Aún no tienes seguidores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1311"/>
        <source>You&apos;re trying to post to your followers only, but you don&apos;t have any followers yet.</source>
        <translation>Estás intentando publicar sólo para tus seguidores, pero no tienes ningún seguidor todavía.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1315"/>
        <source>If you post like this, no one will be able to see your message.</source>
        <translation>Si publicas de esta manera, nadie podrá ver tu mensaje.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1323"/>
        <source>&amp;Cancel, go back to the post</source>
        <translation>&amp;Cancelar, volver al mensaje</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1373"/>
        <source>Updating...</source>
        <translation>Actualizando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1384"/>
        <source>Post is empty.</source>
        <translation>El mensaje está vacío.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1388"/>
        <source>File not selected.</source>
        <translation>No se ha seleccionado un archivo.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1409"/>
        <source>Select one image</source>
        <translation>Selecciona una imagen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1410"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1439"/>
        <source>Select one file</source>
        <translation>Selecciona un archivo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1442"/>
        <source>Invalid file</source>
        <translation>Archivo no válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1443"/>
        <source>The file type cannot be detected.</source>
        <translation>El tipo de archivo no se puede detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1450"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1465"/>
        <source>Since you&apos;re uploading an image, you could scale it down a little or save it in a more compressed format, like JPG.</source>
        <translation>Como lo que estás subiendo es una imagen, podrías reducirla un poco o guardarla en un formato más comprimido, como JPG.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1471"/>
        <source>Dianara currently limits file uploads to 10 MiB per post, to prevent possible storage or network problems in the servers.</source>
        <translation>Actualmente, Dianara limita las subidas de archivos a 10 MiB por mensaje, para evitar posibles problemas de almacenamiento, o de red, en los servidores.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1476"/>
        <source>This is a temporary measure, since the servers cannot set their own limits yet.</source>
        <translation>Esto es una medida temporal, ya que los servidores no pueden establecer sus propios límites todavía.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1470"/>
        <source>File is too big</source>
        <translation>El archivo es demasiado grande</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1318"/>
        <source>Do you want to make the post public instead of followers-only?</source>
        <translation>¿Quieres hacer el mensaje público en lugar de sólo para seguidores?</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1321"/>
        <source>&amp;Yes, make it public</source>
        <translation>&amp;Sí, hacerlo público</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1481"/>
        <source>Sorry for the inconvenience.</source>
        <translation>Lamentamos las molestias.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1501"/>
        <source>Resolution</source>
        <translation>Resolución</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1512"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1514"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1554"/>
        <source>%1 KiB of %2 KiB uploaded</source>
        <translation>%1 KiB de %2 KiB subidos</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1413"/>
        <source>Invalid image</source>
        <translation>Imagen no válida</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="64"/>
        <source>Setting a title helps make the Meanwhile feed more informative</source>
        <translation>Añadir un título ayuda a hacer el contenido del &quot;Mientras tanto&quot; más informativo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="78"/>
        <source>Add a brief title for the post (recommended)</source>
        <translation>Añade un título breve para el mensaje (recomendado)</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="105"/>
        <source>Remove</source>
        <translatorcomment>Quitar? Eliminar?</translatorcomment>
        <translation>Quitar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="107"/>
        <source>Cancel the attachment, and go back to a regular note</source>
        <translation>Cancelar el adjunto y volver a una nota normal</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="208"/>
        <source>Cc...</source>
        <translation>Cc...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="311"/>
        <location filename="../src/publisher.cpp" line="712"/>
        <source>Post</source>
        <comment>verb</comment>
        <translation>Publicar</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1322"/>
        <source>&amp;No, post to my followers only</source>
        <translation>&amp;No, publicar sólo para mis seguidores</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1414"/>
        <source>The image format cannot be detected.
The extension might be wrong, like a GIF image renamed to image.jpg or similar.</source>
        <translation>No se puede detectar el formato de la imagen.
La extensión podría estar equivocada, como una imagen GIF renombrada a imagen.jpg o similar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1420"/>
        <source>Select one audio file</source>
        <translation>Selecciona un archivo de audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1421"/>
        <source>Audio files</source>
        <translation>Archivos de audio</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1424"/>
        <source>Invalid audio file</source>
        <translation>Archivo de audio no válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1425"/>
        <source>The audio format cannot be detected.</source>
        <translation>El formato de audio no se puede detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1429"/>
        <source>Select one video file</source>
        <translation>Selecciona un archivo de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1430"/>
        <source>Video files</source>
        <translation>Archivos de vídeo</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1434"/>
        <source>Invalid video file</source>
        <translation>Archivo de vídeo no válido</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1435"/>
        <source>The video format cannot be detected.</source>
        <translation>El formato de vídeo no se puede detectar.</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="1343"/>
        <source>Posting...</source>
        <translation>Publicando...</translation>
    </message>
    <message>
        <location filename="../src/publisher.cpp" line="313"/>
        <source>Hit Control+Enter to post with the keyboard</source>
        <translation>Pulsa Control+Enter para publicar con el teclado</translation>
    </message>
</context>
<context>
    <name>PumpController</name>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="786"/>
        <source>Getting likes...</source>
        <translatorcomment>meh...</translatorcomment>
        <translation>Recibiendo &quot;likes&quot;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="818"/>
        <source>Getting comments...</source>
        <translation>Recibiendo comentarios...</translation>
    </message>
    <message>
        <source>Getting minor feed...</source>
        <translatorcomment>meh...</translatorcomment>
        <translation type="obsolete">Recibiendo línea temporal menor...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1374"/>
        <source>Error connecting to %1</source>
        <translation>Error conectando a %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1383"/>
        <source>Unhandled HTTP error code %1</source>
        <translation>Código de error HTTP no gestionado: %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1521"/>
        <source>Post published successfully.</source>
        <translation>Mensaje publicado correctamente.</translation>
    </message>
    <message>
        <source>Comment posted successfully.</source>
        <translation type="obsolete">Comentario publicado correctamente.</translation>
    </message>
    <message>
        <source>Minor feed received.</source>
        <translatorcomment>meh</translatorcomment>
        <translation type="obsolete">Línea temporal menor recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1850"/>
        <source>Following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Siguiendo a %1 (%2) correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1860"/>
        <source>Stopped following %1 (%2) successfully.</source>
        <comment>%1 is a person&apos;s name, %2 is the ID</comment>
        <translation>Se ha dejado de seguir a %1 (%2) correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1903"/>
        <source>List of &apos;following&apos; completely received.</source>
        <translation>Lista de &apos;siguiendo&apos; completamente recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1911"/>
        <source>Partial list of &apos;following&apos; received.</source>
        <translation>Parte de la lista de &apos;siguiendo&apos; recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1929"/>
        <source>List of &apos;followers&apos; completely received.</source>
        <translation>Lista de &apos;seguidores&apos; completamente recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1936"/>
        <source>Partial list of &apos;followers&apos; received.</source>
        <translation>Parte de la lista de &apos;seguidores&apos; recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1994"/>
        <source>Person list &apos;%1&apos; created successfully.</source>
        <translation>Lista de personas &apos;%1&apos; creada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2025"/>
        <source>Person list received.</source>
        <translation>Lista de personas recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2191"/>
        <source>File uploaded successfully. Posting message...</source>
        <translation>Archivo subido correctamente. Publicando mensaje...</translation>
    </message>
    <message>
        <source>Timeline received. Updating post list...</source>
        <translation type="obsolete">Línea temporal recibida. Actualizando lista de mensajes...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="225"/>
        <source>Authorized to use account %1. Getting initial data.</source>
        <translation>Autorizado para usar la cuenta %1. Recibiendo datos iniciales.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="230"/>
        <source>There is no authorized account.</source>
        <translation>No hay ninguna cuenta autorizada.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="466"/>
        <source>Getting list of &apos;Following&apos;...</source>
        <translation>Recibiendo lista de &apos;Siguiendo&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="478"/>
        <source>Getting list of &apos;Followers&apos;...</source>
        <translation>Recibiendo lista de &apos;Seguidores&apos;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="531"/>
        <source>Getting list of person lists...</source>
        <translation>Recibiendo lista de listas de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="567"/>
        <source>Creating person list...</source>
        <translation>Creando lista de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="579"/>
        <source>Deleting person list...</source>
        <translation>Borrando lista de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="594"/>
        <source>Getting a person list...</source>
        <translation>Recibiendo una lista de personas...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="632"/>
        <source>Adding person to list...</source>
        <translation>Añadiendo una persona a una lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="668"/>
        <source>Removing person from list...</source>
        <translation>Quitando a una persona de una lista...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="716"/>
        <source>Creating group...</source>
        <translation>Creando grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="746"/>
        <source>Joining group...</source>
        <translation>Uniéndose al grupo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="771"/>
        <source>Leaving group...</source>
        <translation>Saliendo del grupo...</translation>
    </message>
    <message>
        <source>Main timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualización de línea temporal principal solicitada, pero las actualizaciones están bloqueadas.</translation>
    </message>
    <message>
        <source>Getting main timeline...</source>
        <translation type="obsolete">Recibiendo línea temporal principal...</translation>
    </message>
    <message>
        <source>Direct timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualización de línea temporal directa solicitada, pero las actualizaciones están bloqueadas.</translation>
    </message>
    <message>
        <source>Getting direct messages timeline...</source>
        <translation type="obsolete">Recibiendo línea temporal de mensajes directos...</translation>
    </message>
    <message>
        <source>Activity timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualización de línea temporal de actividad solicitada, pero las actualizaciones están bloqueadas.</translation>
    </message>
    <message>
        <source>Getting activity timeline...</source>
        <translation type="obsolete">Recibiendo línea temporal de actividad...</translation>
    </message>
    <message>
        <source>Favorites timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualización de línea temporal de favoritos solicitada, pero las actualizaciones están bloqueadas.</translation>
    </message>
    <message>
        <source>Getting favorites timeline...</source>
        <translation type="obsolete">Recibiendo línea temporal de favoritos...</translation>
    </message>
    <message>
        <source>Timeline update requested, but updates are blocked.</source>
        <translation type="obsolete">Actualización de línea temporal solicitada, pero las actualizaciones están bloqueadas.</translation>
    </message>
    <message>
        <source>Getting timeline...</source>
        <translation type="obsolete">Recibiendo línea temporal...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="914"/>
        <source>Timeline</source>
        <translation>Línea temporal</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="917"/>
        <source>Messages</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1044"/>
        <source>Uploading %1</source>
        <comment>1=filename</comment>
        <translation>Subiendo %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1225"/>
        <source>HTTP error</source>
        <comment>For the following HTTP error codesyou can check http://en.wikipedia.org/wiki/List_of_HTTP_status_codes in your language</comment>
        <translation>Error HTTP</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1237"/>
        <source>Gateway Timeout</source>
        <comment>HTTP 504 error string</comment>
        <translation>Tiempo de espera de la pasarela agotado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1247"/>
        <source>Service Unavailable</source>
        <comment>HTTP 503 error string</comment>
        <translation>Servicio no disponible</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1265"/>
        <source>Not Implemented</source>
        <comment>HTTP 501 error string</comment>
        <translation>No implementado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1275"/>
        <source>Internal Server Error</source>
        <comment>HTTP 500 error string</comment>
        <translation>Error interno</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1295"/>
        <source>Gone</source>
        <comment>HTTP 410 error string</comment>
        <translation>Ya no disponible</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1305"/>
        <source>Not Found</source>
        <comment>HTTP 404 error string</comment>
        <translation>No encontrado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1315"/>
        <source>Forbidden</source>
        <comment>HTTP 403 error string</comment>
        <translation>Prohibido</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1325"/>
        <source>Unauthorized</source>
        <comment>HTTP 401 error string</comment>
        <translation>No autorizado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1336"/>
        <source>Bad Request</source>
        <comment>HTTP 400 error string</comment>
        <translation>Solicitud incorrecta</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1355"/>
        <source>Moved Temporarily</source>
        <comment>HTTP 302 error string</comment>
        <translation>Movido temporalmente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1365"/>
        <source>Moved Permanently</source>
        <comment>HTTP 301 error string</comment>
        <translation>Movido permanentemente</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1491"/>
        <source>Profile received.</source>
        <translation>Perfil recibido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1493"/>
        <source>Followers</source>
        <translation>Seguidores</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1496"/>
        <source>Following</source>
        <translation>Siguiendo</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1510"/>
        <source>Profile updated.</source>
        <translation>Perfil actualizado.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1560"/>
        <source>Avatar published successfully.</source>
        <translation>Avatar publicado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1589"/>
        <source>Post updated successfully.</source>
        <translation>Mensaje actualizado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1595"/>
        <source>Comment updated successfully.</source>
        <translation>Comentario actualizado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1606"/>
        <source>Message liked or unliked successfully.</source>
        <translation>Mensaje marcado o desmarcado &quot;Me gusta&quot; correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1620"/>
        <source>Likes received.</source>
        <translatorcomment>meh...</translatorcomment>
        <translation>&quot;Me gusta&quot; recibidos.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1646"/>
        <source>Comment &apos;%1&apos; posted successfully.</source>
        <comment>%1 is a piece of the comment</comment>
        <translation>Comentario &apos;%1&apos; publicado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1671"/>
        <source>1 comment received.</source>
        <translation>1 comentario recibido.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1676"/>
        <source>%1 comments received.</source>
        <translation>%1 comentarios recibidos.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1703"/>
        <source>Post by %1 shared successfully.</source>
        <comment>1=author of the post we are sharing</comment>
        <translation>Mensaje de %1 compartido correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1732"/>
        <source>Received &apos;%1&apos;.</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Se ha recibido &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1745"/>
        <source>Adding items...</source>
        <translation>Añadiendo elementos...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2244"/>
        <source>SSL errors in connection to %1!</source>
        <translation>¡Errores de SSL en la conexión a %1!</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2256"/>
        <source>Loading external image from %1 regardless of SSL errors, as configured...</source>
        <comment>%1 is a hostname</comment>
        <translation>Cargando imagen externa de %1 a pesar de los errores SSL, como se ha configurado...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2455"/>
        <source>OAuth error while authorizing application.</source>
        <translation>Error de OAuth mientras se autorizaba a la aplicación.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1834"/>
        <source>Message deleted successfully.</source>
        <translation>Mensaje eliminado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="813"/>
        <source>The comments for this post cannot be loaded due to missing data on the server.</source>
        <translation>Los comentarios de este mensaje no se pueden cargar debido a que faltan datos en el servidor.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="870"/>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is the name of a feed</comment>
        <translation>Recibiendo &apos;%1&apos;...</translation>
    </message>
    <message>
        <source>Main timeline</source>
        <translation type="obsolete">Línea temporal principal</translation>
    </message>
    <message>
        <source>Direct messages</source>
        <translation type="obsolete">Mensajes directos</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="920"/>
        <source>Activity</source>
        <translation>Actividad</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="923"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="927"/>
        <source>Meanwhile</source>
        <translation>Mientras tanto</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="930"/>
        <source>Mentions</source>
        <translation>Menciones</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="933"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <source>Getting &apos;%1&apos;...</source>
        <comment>%1 is a feed&apos;s name</comment>
        <translation type="obsolete">Recibiendo &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="1973"/>
        <source>List of &apos;lists&apos; received.</source>
        <translation>Lista de &apos;listas&apos; recibida.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2005"/>
        <source>Person list deleted successfully.</source>
        <translation>Lista de personas borrada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2058"/>
        <source>%1 (%2) added to list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>Se ha añadido a %1 (%2) a la lista correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2078"/>
        <source>%1 (%2) removed from list successfully.</source>
        <comment>1=contact name, 2=contact ID</comment>
        <translation>Se ha eliminado a %1 (%2) de la lista correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2089"/>
        <source>Group %1 created successfully.</source>
        <translation>Grupo %1 creado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2100"/>
        <source>Group %1 joined successfully.</source>
        <translation>Se ha entrado al grupo %1 correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2113"/>
        <source>Left the %1 group successfully.</source>
        <translation>Se ha salido del grupo %1 correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2154"/>
        <source>File downloaded successfully.</source>
        <translation>Archivo descargado correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2200"/>
        <source>Avatar uploaded.</source>
        <translation>Avatar subido.</translation>
    </message>
    <message>
        <source>Loading embedded image from %1 regardless of SSL errors, as configured</source>
        <comment>%1 is a hostname</comment>
        <translation type="obsolete">Cargado imagen incrustada desde %1 a pesar de los errores SSL, como se ha configurado</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2285"/>
        <source>The application is not registered with your server yet. Registering...</source>
        <translation>La aplicación no está registrada con tu servidor todavía. Registrando...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2322"/>
        <source>Getting OAuth token...</source>
        <translation>Recibiendo identificador de autorización (token) de OAuth...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2343"/>
        <source>OAuth support error</source>
        <translation>Error de soporte de OAuth</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2344"/>
        <source>Your installation of QOAuth, a library used by Dianara, doesn&apos;t seem to have HMAC-SHA1 support.</source>
        <translation>Tu instalación de QOAuth, una biblioteca usada por Dianara, no parece tener soporte de HMAC-SHA1.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2348"/>
        <source>You probably need to install the OpenSSL plugin for QCA: %1, %2 or similar.</source>
        <translation>Probablemente necesites instalar el conector de OpenSSL para QCA: %1, %2 o similar.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2405"/>
        <source>Authorization error</source>
        <translation>Error de autorización</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2406"/>
        <source>There was an OAuth error while trying to get the authorization token.</source>
        <translation>Ha habido un error de OAuth mientras se intentaba obtener un identificador de autorización (token).</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2409"/>
        <source>QOAuth error %1</source>
        <translation>Error de QOAuth %1</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2441"/>
        <source>Application authorized successfully.</source>
        <translation>Aplicación autorizada correctamente.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2478"/>
        <source>Waiting for proxy password...</source>
        <translation>Esperando contraseña del proxy...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2507"/>
        <source>Still waiting for profile. Trying again...</source>
        <translation>Aún se está esperando el perfil. Intentándolo de nuevo...</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2616"/>
        <source>Some initial data was not received. Restarting initialization.</source>
        <translation>Algunos datos iniciales no se han recibido. Reiniciando la inicialización.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2624"/>
        <source>Some initial data was not received after several attempts. Something might be wrong with your server. You might still be able to use the service normally.</source>
        <translation>Algunos datos iniciales no se han recibido tras varios intentos. Algo puede estar fallando en tu servidor. Es posible que puedas usar el servicio con normalidad.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2634"/>
        <source>All initial data received. Initialization complete.</source>
        <translation>Se han recibido todos los datos iniciales. Inicialización completada.</translation>
    </message>
    <message>
        <location filename="../src/pumpcontroller.cpp" line="2645"/>
        <source>Ready.</source>
        <translation>Preparado.</translation>
    </message>
</context>
<context>
    <name>TimeLine</name>
    <message>
        <location filename="../src/timeline.cpp" line="66"/>
        <source>Welcome to Dianara</source>
        <translation>Bienvenido a Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="68"/>
        <source>Dianara is a &lt;b&gt;pump.io&lt;/b&gt; client.</source>
        <translation>Dianara es un cliente &lt;b&gt;pump.io&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="77"/>
        <source>Press &lt;b&gt;F1&lt;/b&gt; if you want to open the Help window.</source>
        <translation>Pulsa &lt;b&gt;F1&lt;/b&gt; si quieres abrir la ventana de ayuda.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="80"/>
        <source>First, configure your account from the &lt;b&gt;Settings - Account&lt;/b&gt; menu.</source>
        <translation>En primer lugar, configura tu cuenta desde el menú &lt;b&gt;Configuración - Cuenta&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="83"/>
        <source>After the process is done, your profile and timelines should update automatically.</source>
        <translation>Cuando el proceso esté listo, tu perfil y líneas temporales deberían de actualizarse automáticamente.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="87"/>
        <source>Take a moment to look around the menus and the Configuration window.</source>
        <translation>Tómate un momento para echar un vistazo por los menús y la ventana de Configuración.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="91"/>
        <source>You can also set your profile data and picture from the &lt;b&gt;Settings - Edit Profile&lt;/b&gt; menu.</source>
        <translation>También puedes rellenar tu información de perfil y foto desde el menú &lt;b&gt;Configuración - Editar perfil&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="102"/>
        <source>Dianara&apos;s blog</source>
        <translation>Blog de Dianara</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="179"/>
        <source>Newest</source>
        <translation>Lo más nuevo</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="199"/>
        <source>Newer</source>
        <translation>Más nuevos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="203"/>
        <source>Older</source>
        <translation>Más antiguos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="480"/>
        <source>Page %1 of %2.</source>
        <translation>Página %1 de %2.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="484"/>
        <source>Showing %1 posts per page.</source>
        <translation>Mostrando %1 mensajes por página.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="487"/>
        <source>%1 posts in total.</source>
        <translation>%1 mensajes en total.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="491"/>
        <source>Click here or press Control+G to jump to a specific page</source>
        <translation>Haz clic aquí o pulsa Control+G para saltar a una página específica</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="572"/>
        <source>&apos;%1&apos; cannot be updated because a comment is currently being composed.</source>
        <comment>%1 = feed&apos;s name</comment>
        <translation>No se puede actualizar &apos;%1&apos; porque se está editando un comentario.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="661"/>
        <source>Get %1 pending posts</source>
        <translation>Recibir %1 mensajes pendientes</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="71"/>
        <source>If you don&apos;t have a Pump account yet, you can get one at the following address, for instance:</source>
        <translation>Si aún no tienes una cuenta Pump, puedes conseguir una en la siguiente dirección, por ejemplo:</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="95"/>
        <source>There are tooltips everywhere, so if you hover over a button or a text field with your mouse, you&apos;ll probably see some extra information.</source>
        <translation>Hay descripciones emergentes (tooltips) por todas partes, por lo que si mantienes el ratón sobre un botón o un campo de texto, es probable que veas alguna información adicional.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="104"/>
        <source>Pump.io User Guide</source>
        <translation>Guía de usuario de Pump.io</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="114"/>
        <source>Direct Messages Timeline</source>
        <translation>Línea temporal de mensajes directos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="115"/>
        <source>Here, you&apos;ll see posts specifically directed to you.</source>
        <translation>Aquí verás los mensajes dirigidos específicamente a ti.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="124"/>
        <source>Activity Timeline</source>
        <translation>Línea temporal de actividad</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="125"/>
        <source>You&apos;ll see your own posts here.</source>
        <translation>Aquí verás tus propios mensajes.</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="133"/>
        <source>Favorites Timeline</source>
        <translation>Línea temporal de favoritos</translation>
    </message>
    <message>
        <location filename="../src/timeline.cpp" line="134"/>
        <source>Posts and comments you&apos;ve liked.</source>
        <translation>Mensajes y comentarios que te han gustado.</translation>
    </message>
</context>
<context>
    <name>Timestamp</name>
    <message>
        <location filename="../src/timestamp.cpp" line="61"/>
        <source>Invalid timestamp!</source>
        <translation>¡Hora/fecha no válida!</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="93"/>
        <source>A minute ago</source>
        <translation>Hace un minuto</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="97"/>
        <source>%1 minutes ago</source>
        <translation>Hace %1 minutos</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="108"/>
        <source>An hour ago</source>
        <translation>Hace una hora</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="112"/>
        <source>%1 hours ago</source>
        <translation>Hace %1 horas</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="79"/>
        <source>Just now</source>
        <translation>Ahora mismo</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="83"/>
        <source>In the future</source>
        <translation>En el futuro</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="123"/>
        <source>Yesterday</source>
        <translation>Ayer</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="127"/>
        <source>%1 days ago</source>
        <translation>Hace %1 días</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="138"/>
        <source>A month ago</source>
        <translation>Hace un mes</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="142"/>
        <source>%1 months ago</source>
        <translation>Hace %1 meses</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="151"/>
        <source>A year ago</source>
        <translation>Hace un año</translation>
    </message>
    <message>
        <location filename="../src/timestamp.cpp" line="155"/>
        <source>%1 years ago</source>
        <translation>Hace %1 años</translation>
    </message>
</context>
</TS>
